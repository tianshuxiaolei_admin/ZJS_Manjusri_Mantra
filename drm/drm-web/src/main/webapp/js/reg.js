$(document).ready(function(){
	$("#logsubmit").removeAttr('disabled');
	$("#logsubmit").click(function(){
		/*$('#loginForm').ajaxForm({
			beforeSubmit:  checkForm, // 表单提交执行前检测
			success:       complete,  // 表单提交后执行函数
			dataType: 'json' 
		});*/

		if(checkForm()){
			var formdata = $("#loginForm").serialize(); //序列化表单数据
			var url = GAODUNER.path+"/Member/checklogin?callback=?";
			$.getJSON(url,formdata,function(data){
				complete(data);
			});
			return false;
		}else{
			return false;
		}
		
		function checkForm(){
			//return $.formValidator.pageIsValid('3');
			if(!$("#user_account").val().replace(/^\s+|\s+$/g,'')){
				info = '请输入邮箱或者手机号';
				$("#failTips").html(info).show();
				return false;
			}
			if(!$("#password").val()){
				info = '请输入密码';
				$("#failTips").html(info).show();
				return false;
			}else{
				return true;
			}
		}
		function complete(data){
			if(data.status==1){
				$("#successTips").html(data.info);
				//$("#successTips").html(data.msg);
				//setTimeout(function(){window.location.href='/index.html';}, 1000);
				setTimeout(function(){window.location.href=path+"/Class";}, 1000);
			}else{
				$("#failTips").html(data.info).show();
				//$("#failTips").html(data.msg).show();
			}
		}
	});
	
	/*$.formValidator.initConfig({
		validatorgroup:"3",
		onerror:function(msg){
			//$("#hfailTips").html(msg).show();
			return false;
		}
	});*/
	/*------------------------------------------------------------------*/
	/*$("#user_account").formValidator({validatorgroup:"3",onfocus:"请输入邮箱或者手机号",oncorrect:"账号正确"}).regexValidator({regexp:["email","mobile"],datatype:"enum",onerror:"你输入的手机或邮箱格式不正确"});
	
	$("#nickname").formValidator({validatorgroup:"3",onfocus:"4-12位字符,可由中文、英文、数字、'-'、'_'组成",oncorrect:"该用户名可以注册"}).inputValidator({min:4,max:12,onerror:"用户名非法"}).regexValidator({regexp:["username","chinese"],datatype:"enum",onerror:"用户名格式不正确"});
	
	$("#phone_reg").formValidator({validatorgroup:"3",onfocus:"请输入11位手机号",oncorrect:"谢谢您的合作"}).inputValidator({min:11,max:11,onerror:"11位数字，非中国大陆手机号，请<a href='http://wpa.qq.com/msgrd?v=3&uin=4006008011&site=qq&menu=yes' target='_balnk'>联系学服</a>"}).regexValidator({regexp:"mobile",datatype:"enum",onerror:"你输入的手机号码格式不正确"});

	$("#yzm").formValidator({validatorgroup:"3",onfocus:"请输入验证码",oncorrect:"谢谢您的合作"}).inputValidator({min:6,max:6,onerror:"请填写短信中的6位数字"});

	$("#password").formValidator({validatorgroup:"3",onshow:"请输入密码",onfocus:"密码为6-20位!",oncorrect:"密码合法"}).inputValidator({min:6,max:20,empty:{leftempty:false,rightempty:false,emptyerror:"密码两边不能有空符号"},onerror:"密码格式不正确,请确认"});

	$("#repassword").formValidator({validatorgroup:"3",onshow:"请再次输入密码",onfocus:"密码为6-20位!",oncorrect:"密码一致"}).inputValidator({min:6,max:20,empty:{leftempty:false,rightempty:false,emptyerror:"重复密码两边不能有空符号"},onerror:"重复密码格式不正确,请确认"}).compareValidator({desid:"password",operateor:"=",onerror:"两次密码不一致,请确认"});*/

})
function re(){
	window.location.href='/index.html';
}