$(function(){
	 var current = 0;
            var width = 700;
            var imgobj = $(".imagebox img");
            var time;
            var total = imgobj.length;

            if (total > 1) {
                var clone = imgobj.eq(0).clone();
                $(".imagebox").append(clone).css("width", width * (total + 1) + "px");
                time = setTimeout(callSlide, 3000);

                imgobj.hover(function () {
                    clearTimeout(time);
                }, function () {
                    time = setTimeout(callSlide, 3000);
                });
            }
            function callSlide() {
                clearTimeout(time);
                var imageBox = $("#Box");
                current++;
                imageBox.stop(true, true).animate({ "left": -width * current + "px" }, 500, function () {
                    if (current >= total) {
                        imageBox.css("left", 0);
                        current = 0;
                    }
                    $(".dotlist li").eq(current).addClass("active").siblings().removeClass("active");
                    time = setTimeout(callSlide, 3000);
                });

            }

            function changeImage(index) {
                clearTimeout(time);
                var imageBox = $("#Box");
                if (!imageBox.is(":animated")) {
                    $(".dotlist li").eq(index).addClass("active").siblings().removeClass("active");
                    var offsetWidth = (current - index) * width;
                    imageBox.stop(true, true).animate({ "left": "+=" + offsetWidth + "px" }, 500);
                    current = index;
                    time = setTimeout(callSlide, 3000);
                }
            }

            $(".dotlist li").each(function (i) {
                $(this).click(function () {
                    changeImage(i);
                });
            });
			
			$(".headeritem").mouseenter(function(){
				var index = $(".headeritem").index(this);
				$(this).addClass("active").siblings().removeClass("active");
				$(".newcourses").eq(index).show().siblings().hide();
			});
			
			$(".item-list li").hover(function(){
				var parent = $(this).parents(".course-list>.item-list");
				var offsetTop = parent.offset().top;
				var liTop = $(this).offset().top;
				var posTop = liTop - offsetTop;
				var subMenu = $(this).find(".sub-menu");
				var outerHeight = subMenu.outerHeight();
				var top = outerHeight/2 > posTop ? 0 : -outerHeight/2;
				subMenu.css("top",top+"px");
				$(".course-list li").removeClass("active");
				$(this).addClass("active");
				
			},function(){
				var subMenu = $(this).find(".sub-menu");
				$(this).removeClass("active");
			});
			
/*
			$(".course-item img").hover(function(){
				$(this).stop(true,true).animate({"width":"+=32px","height":"+=18px","left":"-16px","top":"-9px"},300);
			},function(){
				$(this).stop(true,true).animate({"width":"-=32px","height":"-=18px","left":"0","top":"0"},300);
			});
*/

})

