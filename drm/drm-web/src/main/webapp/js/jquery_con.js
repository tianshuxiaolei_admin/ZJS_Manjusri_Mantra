function DrawImage(e, t, n) {
    var r = new Image,
    i = t,
    s = n;
    r.src = e.src,
    r.width > 0 && r.height > 0 && (flag = !0, r.width / r.height >= i / s ? (r.width > i ? (e.width = i, e.height = r.height * i / r.width) : (e.width = r.width, e.height = r.height), e.alt = r.width + "\u00d7" + r.height) : (r.height > s ? (e.height = s, e.width = r.width * s / r.height) : (e.width = r.width, e.height = r.height), e.alt = r.width + "\u00d7" + r.height))
}

$(function(){
	
	$('.tabTitle a').mouseover(function(){
		$(this).addClass("hover").siblings().removeClass();
		//$(this).parents(".tabTitle").next(".tabContent").find('.column_1').eq($(this).index()).show().siblings().hide();
		$(this).parents(".tabTitle").next("div").children("div").eq($(this).index()).show().siblings().hide();
		//$(".tabContent .column_1").eq($(this).index()).css("display","block").siblings().css("display","none");
	});
	
	$("#KinSlideshow").KinSlideshow({
        moveStyle: "down",
        mouseEvent: "mouseover",
        titleBar: {
            titleBar_height: 30,
            titleBar_bgColor: "#ffffff",
            titleBar_alpha: .7
        },
        titleFont: {
            TitleFont_size: 12,
            TitleFont_color: "#000000",
            TitleFont_weight: "normal"
        },
        btn: {
            btn_bgColor: "#FFFFFF",
            btn_bgHoverColor: "#89ab5d",
            btn_fontColor: "#000000",
            btn_fontHoverColor: "#FFFFFF",
            btn_borderColor: "#829964",
            btn_borderHoverColor: "#1188c0",
            btn_borderWidth: 1
        }
    })
	
	
	$("#xiala .left_list").hide();//隐藏下拉菜单
	$("#xiala").hover(function () {
		$(this).children(".left_list").show();
	  },function(){
		$(this).children(".left_list").hide();
	  },300,0
	);
	//下拉菜单鼠标移过效果,鼠标移除效果只有首页保留,其它页面全部取消
	$(".h2_cat").mouseover(function(){
		$(this).addClass('h2_cat active_cat').siblings().removeClass('active_cat');
	});
	//下拉菜单点击效果
	$(".my_left_cat_list h3").click(function(){
		location.href=$(this).children("a").attr("href");
	});
	//$('<div class="right_css"><img src="/images/weixin.jpg"></div>').appendTo("body")
	
});
