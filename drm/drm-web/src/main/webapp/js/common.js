/* 公用脚本函数  */


/**
 * 过滤字符
 * @param   string  str ( 字符串 )
 */
function xss_clean(str)
{
    if(str)
    {
        str = str.replace(/</g, "&lt;");
        str = str.replace(/>/g, "&gt;");
        str = str.replace(/"/g, "&quot;");
        str = str.replace(/&/g, "&quot;");
    }
    return str;
}

/**
 * 获取当前域名
 */
function getCurrentDomain()
{
    var domain = window.location.href.match(/^http:\/\/([^\/]+)\/{1}/);
        domain = domain[1];
    return domain;
}

/**
 * 获取 home domain
 */
function getHomeDomain()
{
    var domain = getCurrentDomain();
    var domainArr = domain.split(".");
    
    if(domainArr.length > 2)
    {
        domainArr[0] = "home";
        domain = domainArr.join(".");
    }
    else
    {
        domain = "home." + domain;
    }
    return domain;
}

/**
 * 获取 domain
 */
function getDomain()
{
    var domain = getCurrentDomain();
    var domainArr = domain.split(".");
    
    if(domainArr.length > 2)
    {
        delete domainArr[0];
        domain = domainArr.join(".");
    }
    else
    {
        domain = '.'.domain;
    }
    return domain;
}

/**
 * 登陆判断
 * @param   string  callback    ( 回调函数 )
 */
function isLogins(callback)
{
    $.getJSON
    (
        "http://" + getHomeDomain() + "/login/ajaxcheck?callback=?", 
        function(result)
        {
            callback(result);
        }
    );
    return;
}

/**
 * 登陆提交
 * @param   string  member      ( 会员名 )
 * @param   string  password    ( 会员密码 )
 * @param   boolean isKeep      ( 保存登陆 )
 * @param   string  callback    ( 回调函数 )
 */
function ajaxLogin(member, password, isKeep, callback)
{
    $.getJSON
    (
        "http://" + getHomeDomain() + "/login/ajaxin?callback=?", 
        {"data[member]": member, "data[password]": password, "data[keep_in]": isKeep}, 
        function(result)
        {
            if(result.sync_code)
            {
                $("body").append(result.sync_code);
            }
            if(callback)
            {
                callback(result.member);
            }
        }
    );
    return;
}

/**
 * 登出提交
 * @param   string  callback    ( 回调函数 )
 */
function ajaxLogout(callback)
{
    $.getJSON
    (
        "http://" + getHomeDomain() + "/login/ajaxout?callback=?", 
        function(result)
        {
            if(result.sync_code)
            {
                $("body").append(result.sync_code);
            }
            if(callback)
            {
                callback();
            }
        }
    );
    return;
}

/**
 * 显示登陆窗
 * @param   string  callback    ( 回调函数名. 关闭登陆窗口: $("#floatBoxshowLogin").remove(); )
 */
function showLogin(callback)
{
    $.getJSON
    (
        "http://" + getHomeDomain() + "/login/group?callback=?", 
        {"call": callback}, 
        function (result)
        {
            floatBox("showLogin", "登录 / 注册", result);
        }
    );
    return;
}

/**
 * 显示协议
 */
function showProtocol()
{
    $.getJSON
    (
        "http://" + getHomeDomain() + "/register/protocol?callback=?", 
        function (result)
        {
            floatBox('protocol', '会计网使用服务协议', '<div style="height:500px; padding:15px; overflow-y:scroll">' + result + '</div>');
        }
    );
    return;
}

/**
 * 判断关注
 * @param   int id          ( 关联ID )
 * @param   int type        ( 关注类型 1为分类栏目, 2为主题 )
 * @param   int callback    ( 回调函数 )
 * @return  int result      ( 返回结果 -1为未登陆, 0为未关注, 1以上为已关注数 )
 */
function isAttention(id, type, callback)
{
    $.getJSON
    (
        "http://" + getHomeDomain() + "/member/is_attention?callback=?", 
        {"id": parseInt(id), "type": parseInt(type)}, 
        function(result)
        {
            callback(result);
        }
    );
    return;
}

/**
 * 关注操作
 * @param   int id          ( 关联ID )
 * @param   int type        ( 关注类型 1为分类栏目, 2为主题 )
 * @param   int option      ( 相关操作 1为添加, 0为取消 )
 * @param   int callback    ( 回调函数 )
 */
function setAttention(id, type, option, callback)
{
    $.getJSON
    (
        "http://" + getHomeDomain() + "/member/addattention?callback=?", 
        {"id": parseInt(id), "type": parseInt(type), "option": parseInt(option)}, 
        function(result)
        {
            callback(result);
        }
    );
    return;
}

/**
 * 设置回到顶部
 */
function setGoTop(site)
{
    switch(site)
    {
        case "home":
            var backToTop = $('<div class="backToTop"></div>');
            break;
        default:
            var backToTop = $('<div class="backToTop"><a title="返回顶部" class="backToTop1" href="javascript:void(0)"></a><a title="帮助" target="_blank" class="backToTop2" href="http://bbs.kuaiji.com/forum-64-1.html"></a></div>');
            break;
    }
    backToTop.appendTo($("body"));
    backToTop.click(function ()
    {
        $("html, body").animate({scrollTop: 0}, 120);
    });
    backToTop.toggle($(document).scrollTop() > 0);
    
    $(window).scroll(function () 
    {
        backToTop.toggle($(document).scrollTop() > 0);
        
        // IE6下的定位
        if(navigator.userAgent.indexOf("MSIE 6") != -1) 
        {
            backToTop.css("top", $(document).scrollTop() + $(window).height() - backToTop.innerHeight() - 108);
        }
    });
}

/**
 * 浮动窗口
 * @param   int     id      ( 唯一ID )
 * @param   string  title   ( 窗口标题 )
 * @param   string  content ( 内容代码 )
 */
function floatBox(id, title, content)
{
    if(!$("#floatBox" + id).length)
    {
        var boxID = "floatBox" + id;
        var boxHtml  = '<table id="' + boxID + '" class="pactT1" border="0" cellspacing="0" cellpadding="0" floatBox="">';
            boxHtml += '<tr>';
            boxHtml += '<td class="pactA-po" width="9" height="31">&nbsp;</td>';
            boxHtml += '<td class="pactB-po">';
            boxHtml += '<div class="pactT-po">';
            boxHtml += '<a id="' + boxID + 'Close" href="javascript:void(0)" onclick="' + "$('#" + boxID + ", #" + boxID + "Black').remove()" + '"></a>';
            boxHtml += '<strong>' + title + '</strong>';
            boxHtml += '</div>';
            boxHtml += '</td>';
            boxHtml += '<td class="pactC-po" width="9">&nbsp;</td>';
            boxHtml += '</tr>';
            boxHtml += '<tr>';
            boxHtml += '<td class="pactD-po" width="9">&nbsp;</td>';
            boxHtml += '<td id="' + boxID + 'Content" style="background-color:#FFFFFF;">';
            boxHtml += content;
            boxHtml += '</td>';
            boxHtml += '<td class="pactF-po" width="9">&nbsp;</td>';
            boxHtml += '</tr>';
            boxHtml += '<tr>';
            boxHtml += '<td class="pactG-po" width="9" height="9">&nbsp;</td>';
            boxHtml += '<td class="pactH-po">&nbsp;</td>';
            boxHtml += '<td class="pactI-po">&nbsp;</td>';
            boxHtml += '</tr>';
            boxHtml += '</table>';
            boxHtml += '<div id="' + boxID + 'Black" class="mapA">';
            boxHtml += '<div id="' + boxID + 'Map" class="mapC"></div>';
            boxHtml += '<script type="text/javascript">';
            boxHtml += 'var container = document.getElementById("' + boxID + 'Map");';
            boxHtml += 'container.style.filter = "progid:DXImageTransform.Microsoft.Alpha(style=3,opacity=50,finishOpacity=50)";';
            boxHtml += 'container.style.opacity = "0.5";';
            boxHtml += 'container.style.filter = "Alpha(Opacity=50, FinishOpacity=50, Style=1, StartX=0, StartY=0, FinishX=50, FinishY=50)";';
            boxHtml += '</script>';
            boxHtml += '</div>';
        $("body").append(boxHtml);
        
        // IE6下的定位
        if(navigator.userAgent.indexOf("MSIE 6") != -1) 
        {
            $("#" + boxID).css(
            {
                "top": $(document).scrollTop() + ($(window).height() - $("#" + boxID).height()) / 2, 
                "marginLeft": -($("#" + boxID).width() / 2)
            });
            
            $(window).scroll(function ()
            {
                $("#" + boxID).css(
                {
                    "top": $(document).scrollTop() + ($(window).height() - $("#" + boxID).height()) / 2,  
                    "marginTop": 0
                });
            });
        }
        else
        {
            $("#" + boxID).css(
            {
                "top": "50%", 
                "marginTop": -($("#" + boxID).height() / 2), 
                "marginLeft": -($("#" + boxID).width() / 2)
            });
        }
        
    }
    return;
}

/**
 * 设置主页
 * @param   string  url ( 地址 )
 */
function sethomepage(url)
{
    if(document.all)
    {
        document.body.style.behavior='url(#default#homepage)';
        document.body.setHomePage(url);
    }
    else if(window.sidebar)
    {
        if(window.netscape)
        {
            try
            {
                netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect");
            }
            catch (e)
            {
                alert("亲爱的用户你好：\n你使用的不是IE浏览器，此操作被浏览器阻挡了，你可以选择手动设置为首页！\n给你带来的不便，本站深表歉意。");
            }
        }
    }
}

/**
 * 收藏页面
 * @param   string  url     ( 地址 )
 * @param   string  title   ( 标题命名 )
 */
function addbookmark(url, title) 
{
    title = title ? title : document.title;
    url = url ? url : window.location.href;
    
    if(window.sidebar) 
    {
        window.sidebar.addPanel(title, url , "");
    }
    else if(document.all) 
    {
        window.external.AddFavorite(url, title);
    }
    else // if( window.opera && window.print ) 
    {
        alert("浏览器收藏没有成功，请使用Ctrl+D进行添加!");
        return true;
    }
}

$(window).load(function(){
	var t = $('.fixMenu').offset().top;
	var mh = $('body').height();
	var fh = $('#fixMenu').height();
	
	$(window).scroll(function(e){
		s = $(document).scrollTop();
		if(s > t - 100){
		$('#fixMenu').addClass("scrollCls");
		if(s + fh > mh){
		}
		}else{
		$('#fixMenu').removeClass("scrollCls");
		}
	})
})