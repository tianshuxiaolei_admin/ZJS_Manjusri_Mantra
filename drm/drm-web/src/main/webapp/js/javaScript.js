// JavaScript Document
function setTab(name, cursel, n, currentClass, otherClass)
{
    for(i = 1; i <= n; i++)
    {
        var menu = document.getElementById(name + i);
        var con = document.getElementById("con_" + name + "_" + i);
        menu.className = i == cursel ? currentClass : otherClass;
        con.style.display = i == cursel ? "block" : "none";
    }
}

function set(obj, cla1, cla2)
{
	var c = $(obj).attr('class');
	c = c == cla1 ? cla2 : cla1; 
	$(obj).attr('class', c);
	return false;
}

/**
 * 方法：callposter
 * 功能：jsonp调用广告
 */
function callposter(sign)
{
    var www_url = 'http://www'+getDomain();
    
     $.ajax
     ({
        type: "POST",
        url: www_url+"/main/getposter/"+sign,
        async: false,
        dataType: "jsonp",
        data: "",
        success: function(obj)
        {
            if(obj.type == 'imagelist')
            {
                var theclass = '';
                var html = '<a href="../images/' + obj.setting.image.url + '" class="' + theclass + '" target="_blank" title="' + obj.description + '"><img width="' + obj.width + '" height="' + obj.height + '" src="../images/' + obj.setting.image.linkurl + '" alt="' + obj.setting.image.alt + '" ></a>';
            }
            else if(obj.type == 'code')
            {
                var html = obj.setting.code.codecontent;
            }
            else if(obj.type == 'imagechange')
            {
                var html = '<div id="basicFeatures" style="height:164px;">';
                for (var i in obj.data) 
                {
                    var j = parseInt(i)+1;
                    html += '<div title="'+j+'"><a href="../images/' + obj.data[i].setting.image.url + '" target="_blank" title="' + obj.data[i].setting.image.alt + '"><img width="' + obj.cat.width + '" height="' + obj.cat.height + '" alt="' + obj.data[i].setting.image.alt + '" src="../images/' + obj.data[i].setting.image.linkurl + '" /></a></div>';
                }
                html += '</div>';                
                $(function()
                {
                    $("#basicFeatures").jshowoff({
                            controls: false,
                            controlText :{ play:'Play', pause:'Pause', previous:'Previous', next:'Next' },
                            links: true,
                            speed: 5000,
                            changeSpeed: 100,
                            cssClass: 'mystyle'
                    }); 
                });
            }
            $('#'+ sign +'js').after(html);
            $('#'+ sign +'js').remove();
        }
    })
}