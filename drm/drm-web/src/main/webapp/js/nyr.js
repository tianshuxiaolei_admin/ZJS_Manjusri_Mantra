
   function initSelect(obj,start,end){
       obj.length = 1;
       for(var i = start;i<=end;i++){
           try{
             obj.add(new Option(i,i),null);
           }catch(ex){
             obj.add(new Option(i,i));
           }
       }
   }
   var year = document.getElementById("year");
   var month = document.getElementById("month");
   var day = document.getElementById("day");
   initSelect(year,1970,2012);
   year.onchange = function(){
       if(this.value != 0){
           initSelect(month,1,12);
       }else{
           month.length = 1;
           day.length = 1;
       }
   }
   month.onchange = function(){
       if(this.value != 0){
           var m30 = {2:1,4:1,6:1,9:1,11:1};
           if(this.value == 2){
              if(isLeapYear(year.value)){
                  initSelect(day,1,29);
              }else{
                  initSelect(day,1,28);
              }
           }else if(this.value in m30){
              initSelect(day,1,30);
           }else{
              initSelect(day,1,31);
           }
       }else{
           day.length = 1;
       }
   }
   //判断闰年的条件：能被4整除且不能被100整除 或 能被100整除且能被400整除
   function isLeapYear(y){
       return (y%4==0 && y%100!=0)||(y%100==0 && y%400==0);
   }
