﻿



                               //查看视频预览
                                function ViewVideoPreview(obj, itemType, isAllowPreview, isAllowAnonymousPreview, id) {


                                    if (itemType == "Video" && isAllowPreview == "True") {
                                        //去看视频
                                        if (id != "") {
                                            if (isAllowAnonymousPreview == "True") {
                                                showPopupVideo(680, 510, "", "/dialog/experiencevideo.htm?id=" + id);
                                            } else {
                                                //未允许匿名访问
                                                checkLogin(function () {
                                                    showPopupVideo(680, 510, "", "/dialog/experiencevideo.htm?id=" + id);
                                                });
                                            }
                                        } else {
                                            showWarningMsg("当前课程暂不支持预览。");
                                        }
                                    } else {
                                        var isCanPreview = 'False';
                                        var previewURL = '/course/study/index/19d0032d38324ac288eba452a906d31a.html?cid=1ff41025-6ed3-495e-8512-16484d21537f';

                                        //如果是模拟学习，则进入模拟学习页面
                                        if (isCanPreview == "True") {
                                            window.location.href = previewURL;
                                        }
                                        else {
                                            //去看购买信息
                                            showRemind(obj);
                                        }
                                    }
                                }

                                //行移入
                                function MouseoverRow(obj) {
                                    if ($(obj).find("input")) {
                                        $(obj).find("input").attr("class", "btntrysee20");
                                    }
                                }

                                //行移出
                                function MouseoutRow(obj) {
                                    if ($(obj).find("input")) {
                                        $(obj).find("input").attr("class", "btntrysee21");
                                    }
                                }
                                function showRemind(obj) {
                                    var dvRemind = $("#dvRemind");
                                    var left = ($(document).width() - 450) / 2;
                                    var top = ($(window).height() - 175) / 2 + $(document).scrollTop();
                                    dvRemind.css("left", left + "px").css("top", top + "px").css("z-index", "901");
                                    dvRemind.show();
                                    var w = $(document).width();
                                    var h = $(document).height();
                                    var w1 = $(window).width();
                                    var w2 = $(document).width();
                                    var h1 = $(window).height();
                                    var h2 = $(document).height();
                                    // 表示滚动条出现类型。none:无,h:水平,v:垂直,a:两者
                                    var s = "";
                                    if (w1 == w2 && h1 == h2) {
                                        s = "none";
                                    }
                                    else if (w1 < w2 && h1 + 16 == h2) {
                                        s = "h";
                                        h = h - 16;
                                    }
                                    else if (w1 + 16 == w2 && h1 < h2) {
                                        s = "v";
                                        w = w - 16;
                                    }
                                    else if (w1 < w2 && h1 < h2) {
                                        s = "a";
                                    }
                                    var s = '<div id="_dvShadePanel" style="width:' + w + 'px; height:' + h + 'px; position:absolute; left:0px; top: 0px; background-color:#000000;opacity:0.4; filter:alpha(opacity=40); z-index:900;"></div>';
                                    $("div:eq(0)").before(s);
                                }

                                function closeRemind() {
                                    $("#dvRemind").hide();
                                    $("#_dvShadePanel").remove();
                                }

                                //控制Scorm课程显示
                                function ControlScormDisplay(id) {
                                    if ($("#" + id).attr("class") == "iconexpansion") {
                                        $("#" + id).attr("class", "iconshrink");
                                    } else {
                                        $("#" + id).attr("class", "iconexpansion");
                                    }

                                    $('div.classhourlist div').each(function (i) {
                                        if (typeof ($(this).attr("myvalue")) != "undefined") {
                                            var value = $(this).attr("myvalue");
                                            if (value == id) {
                                                $(this).toggle(200);
                                            }
                                        }
                                    });
                                    return;
                                }

                                //获取参考资料
                                function GetCoursewareReference(courseid, wareid,tipsObj) {
                                    if (!courseid) {
                                        showWarningMsg("当前课程暂不支持预览。");
                                    }
                                    if (!wareid) {
                                        showWarningMsg("当前课件不存在。");
                                    }
//                                    var tipsLength = $(tipsObj).find("ul").length;
//                                    if (tipsLength > 0) {
//                                        return false;
//                                    }

                                    var methodName = "GetCoursewareReferenceByCourseIDANDCourseWareID";
                                    var arr = '{"courseID":"' + courseid + '","courseWareID": "' + wareid + '"}';
                                    jQuery.ajax({
                                        type: "POST",
                                        contentType: "text/json",
                                        url: "/Services/CommonService.svc/GetCoursewareReferenceByCourseIDANDCourseWareID",
                                        data: arr,
                                        dataType: 'json',
                                        cache: false,
                                        success: function (obj) {
                                            var jsonObj = "";
                                            if (obj) {
                                                jsonObj = eval("(" + obj + ")");
                                                loadCoursewareReference(jsonObj,tipsObj);
                                            }
                                        },
                                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                                            showWarningMsg("服务器忙...请稍等");
                                        }
                                    });
                                }

                                function loadCoursewareReference(jsonObj,tipsObj) {
                                    var html = '';
                                    $(tipsObj).find("ul").html("");
                                    if (jsonObj.length == 0) {
                                        $(tipsObj).find("ul").html("");
                                        $(tipsObj).hide();
                                    }
                                    else {
                                        for (var i = 0; i < jsonObj.length; i++) {
                                            var item = jsonObj[i];
                                            var timeStr = "";
                                            html += '<li>';
                                            html += '<span class="Knowledge_' + item.FileType + '">&nbsp;</span>&nbsp;';
                                            //var onclickJs = "openCourseWareView('" + item.ViewUrl + "','" + item.KnowledgeType + "','" + item.FileType + "','" + item.KnowledgeFileUrl + "','" + item.ID + "','" + item.Title + "')";
                                            //html += '<a onclick="' + onclickJs + '" href="javascript:;">' + LeftString(item.Title, 12) + '</a>';
                                            html += '<span title = ' + item.Title + '>' + LeftString(item.Title, 12) + '</span>';
                                            html += '</li>';
                                        }
                                    }
                                    $(tipsObj).find("ul").html(html);
                                } 


                                $(function () {
                                    //初始化样式
                                    $('div.classhourlist div').each(function (i) {
                                        var value = $(this).attr("myvalue");
                                        if (typeof (value) != "undefined") {
                                            if (value != "") {
                                                $(this).show();
                                            }
                                        }
                                    });
                                });
                            