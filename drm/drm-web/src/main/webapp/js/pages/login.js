var LOGIN = {
    init : function() {
        LOGIN.initUI();
        LOGIN.initEvent();
        LOGIN.uiState();
    },

    initUI : function() {
        
    },

    initEvent : function() {
        
    },

    uiState : function() {
        
    },

    onLogin : function() {
    	
      $('#loginForm').submit();
    },

    goToRegisterPage : function() {
        window.location = "/member/register.htm";
    }
};
jQuery(document).ready(LOGIN.init);