var REGISTER = {
    provinceData: [],
    cityData: [],
    
    init : function() {
        //REGISTER.initUI();
        REGISTER.initEvent();
        REGISTER.uiState();
    },

    initUI : function() {
        REGISTER.getProvinceCity();
    },

    initEvent : function() {
        //$('#sel_province').change(REGISTER.onProvinceChange);
        //$('#sel_city').change(REGISTER.onCityChange);
        //$('#submitImg').click(REGISTER.register);
        $('#agreement').click(REGISTER.uiState);
    },

    uiState : function() {
        // “我已阅读并同意<用户服务协议>”是否选中
        if ($('#agreement').attr("checked")) {
            $('#submit_img').show();
            $('#disable_img').hide();
        } else {
            $('#submit_img').hide();
            $('#disable_img').show();
        }
    },

    getProvinceCity : function() {
        $.get("register/chinacity", REGISTER.callbackDosomething1, "json");
    },

    getRegisterCodeImg : function() {
        var img = "rancode/img2?"+Math.random();
        $("#register_code").attr("src", img);
    },

    register : function() {
        if($("#register").validationEngine('validate')){
//            $('#provinceCode').val($('#sel_province').val());
            $('#provinceName').val($('#sel_province option:selected').text());
//            $('#cityCode').val($('#sel_city').val());
            $('#cityName').val($('#sel_city option:selected').text());
            // 如果注册用户未选择“省”
            if ($('#sel_province').val() == 0) {
                $('#provinceCode').val("");
                $('#provinceName').val("");
            }
            // 如果注册用户未选择“市”
            if ($('#sel_city').val() == 0) {
                $('#cityCode').val("");
                $('#cityName').val("");
            }
            
            $('#register').submit();
            /*$("#register").ajaxSubmit(function(responseResult){
                var obj = $.parseJSON(responseResult);
                if (obj['code'] == 0) {
                    $.Zebra_Dialog(obj['msg'], {
                        'type':     'error',
                        'title':    '错误'
                    });
                } else {
                    // 取得用户输入的邮箱地址和用户id
                    email_addr = obj['mail_addr'];
                    user_id = obj['id'];
                    randomcode = obj['randomcode'];
                    window.open("/email_act.htm");
                }
            });*/
        }
    },

    onProvinceChange : function() {
       // 清空select
       var citySelObj = $('#sel_city');
       citySelObj.empty();
       citySelObj.append('<option value="0">市</option>');
       var provinceCode = $('#sel_province').val();
       // 过滤出所选省下的城市
       if (provinceCode == 0) {
           for (var i=0; i < REGISTER.cityData.length; i++) {
               var option = "<option value='" + REGISTER.cityData[i]['code'] + "'>" + REGISTER.cityData[i]['name'] + "</option>";
               citySelObj.append(option);
           }
       } else {
           for (var i=0; i < REGISTER.cityData.length; i++) {
               if (provinceCode == REGISTER.cityData[i]['parent_code']) {
                   var option = "<option value='" + REGISTER.cityData[i]['code'] + "'>" + REGISTER.cityData[i]['name'] + "</option>";
                   citySelObj.append(option);
               }
           }
       }
    },

    onCityChange : function() {
        var cityCode = $('#sel_city').val();
        for (var i = 0; i < REGISTER.cityData.length; i++) {
            if (cityCode == REGISTER.cityData[i]['code']) {
                $('#sel_province').val(REGISTER.cityData[i]['parent_code']);
                break;
            }
        }
    },

    callbackDosomething1 : function(data) {
        var provinceSelObj = $('#sel_province');
        REGISTER.provinceData = data['province'];
        for (var i=0; i < REGISTER.provinceData.length; i++) {
            var option = "<option value='" + REGISTER.provinceData[i]['code'] + "'>" + REGISTER.provinceData[i]['name'] + "</option>";
            provinceSelObj.append(option);
        }

        var citySelObj = $('#sel_city');
        REGISTER.cityData = data['city'];
        for (var j=0; j < REGISTER.cityData.length; j++) {
            var option = "<option value='" + REGISTER.cityData[j]['code'] + "'>" + REGISTER.cityData[j]['name'] + "</option>";
            citySelObj.append(option);
        }
    },

    goToLoginPage : function() {
        window.location = '/login.htm';
    }
};
jQuery(document).ready(REGISTER.init);