//document.write("<script src='js/pages/articles.js' type='text/javascript'></script>")

/**
 *    换一批分页功能：
 *   一页的数据总数目前假设是5 
 *   在页面设置个隐藏值，每点一次累加1，当做页号传给后台分页
 *   每次重新加载页面时，隐藏值重置为1，且传递当前需要分页的总数，以便分页时判断当前页号是否到最后一页
 *   
 *   
 */



jQuery(document).ready(function() {
	//此处将该函数放到了function中，为了在html全部加载完成后，再发起ajax请求
	Article.InitPage(1);
	
	 pageNumber=1;
	 
	  var menus = $('.zyzx_div_left01nav .zyzx_div_left01navl01, .zyzx_div_left01nav .zyzx_div_left01navl02');
	  menus.find('a').click(function(e) {
		  //alert('test');
		  menus.removeClass().addClass('zyzx_div_left01navl02');
		  var current = $(e.target).parent();
		  current.removeClass().addClass('zyzx_div_left01navl01');
		  
		  var var_type=[32,33,34,35,36,37,38,39];
		  var i_type = menus.index(current);
		  console.log(i_type);
		  Article.tabswitch(var_type[i_type],Article.pageSize , pageNumber);
	  });
	
	$("#imgSwitch").click(function(e) {
		artcletype=1;
		//alert('tet');
		Article.pageSize=5;
		//pageNumber=1;
		
		Article.imgswitch(artcletype,Article.pageSize , Article.pageNumber++);//Article.pageNumber++是为了在下次点击时保证当前页号加1
	   });
	
});


Article = {
		
		baseUrl:"http://www.zhuclass.com/case",
		
		pageNumber:1,
		
		pageSize:5,
		
		InitPage : function(artcletype){
			  Article.imgswitch(artcletype,Article.pageSize,1);
			  
			
		},
		
		//rest方式 ，传入 类型，每页显示的大小数量，第几个页数
		imgswitch:function(artcletype,pageSize,pageNo){
			var restUrl = ( Article.baseUrl +"/"+artcletype+"/"+pageSize+"/"+pageNo ) ;
			console.log("restUrl="+restUrl);
			//获取分页内容
			$.ajax({
				  beforeSend:Article.loading,
				  complete:Article.loaded,
				  type: "GET",
				  url: restUrl,
				  dataType: "json",
				  success: function(data){
					  /** 成功构造内容**/
					  
					  Article.createImgContent(pageNo,data);
					} 
				});
			return null;
		},
		/* 根据ID 查看详情   */
		detail:function (articleId){

		},
		//正在加载中
		loading:function(showyn){
			//console.log(jQuery('#loading-one').html());
			jQuery('#loading-one').empty().append('载入中。。。。.').parent().fadeIn('slow') ;  
		},
		loaded:function(showyn){			
			jQuery('#loading-one').empty().append('加载完毕').parent().fadeOut('slow');  
		},
		//构造最新案例 和 热门案例结果集，且在点击左侧菜单时构造  
		createContent:function( curPage,pageData ){
	        // alert("creattontent");
			//1构造topNew数据 
			var html  = $("#topNewTemplate").render(pageData);
			//清空数据
			$("#contents_topNew").html("");
			//填充内容 			
			$("#contents_topNew").append(html);
			/*//2构造图片分页的数据 
			var html  = $("#imageTemplate").render(pageData);
			//清空内容；
			$("#contents_image").html("");
			//填充内容 			
			$("#contents_image").append(html);
			//3构造热点文章分页的数据 
			var html  = $("#caseTemplate").render(pageData);
			//清空内容；
			$("#casediv").html("");
			//填充内容 			
			$("#casediv").append(html);
          
			if(!curPage || curPage<=1){
				//显示分页的栏目pagingdiv  : 是显示分页栏目的div
				$("#pagingdiv").pagination({
				        items:       pageData.totalCount? pageData.totalCount:0,
				        itemsOnPage: pageData.pageSize?pageData.pageSize:10,
				        onPageClick: Article.onPage
				 });
			 }*/
		},
		//为图片选择一批，分页查询的形式
		tabswitch:function(artcletype){
			var restUrl = "http://www.zhuclass.com/case/jsonTopCase?type="+artcletype ;
			console.log("restUrl="+restUrl);
			//获取分页内容
			$.ajax({
				  beforeSend:Article.loading,
				  complete:Article.loaded,
				  type: "GET",
				  url: restUrl,
				  dataType: "json",
				  success: function(data){
					  
					  Article.createContent(1,data);
				      
					} 
				});
			return null;
		},
		//构造案例分享结果集
		createImgContent:function( curPage,pageData ){
	         
			//构造图片分页的数据 
			var html  = $("#imageTemplate").render(pageData);
			
			//清空内容；
			$("#contents_image").html("");
			//填充内容 			
			$("#contents_image").append(html);
			
		},
		//整除
		divExp:function(exp1, exp2)
		{
		    var n1 = Math.round(exp1); //四舍五入
		    var n2 = Math.round(exp2); //四舍五入
		    
		    var rslt = n1 / n2; //除
		    
		    if (rslt >= 0)
		    {
		        rslt = Math.floor(rslt); //返回值为小于等于其数值参数的最大整数值。
		    }
		    else
		    {
		        rslt = Math.ceil(rslt); //返回值为大于等于其数字参数的最小整数。
		    }
		    
		    return rslt;
		},
		/******
		 * 点击分页按钮的分页事件 
		 * 
		 ***********/
		onPage:function(pageNumber, event){
			/***********分页按钮*************/
			//alert("fsad");
			//console.log(pageNumber);
			//console.log(event);
			//传递 类型   每页多大 ，页数   需要写后台页面
			Article.goPage(1, Article.pageSize , pageNumber) ;
		},
		
		
};

/*****pagingdiv  这个ID 大家可以参考 ，就固定把分页 div 设置为这个ＩＤ *************** 
 * 
 * 下面都是 手动操作分页的 基本很少用到
 * 
 *******http://flaviusmatis.github.io/simplePagination.js/#page-1******/
var paging = {
	/** 是分页的 数字按钮切换到 pageNo 的位置 **/
	selectPage:function(pageNo)	{  		$("#pagingdiv").pagination('selectPage',pageNo );  	},
	/****调到当前页数的前一页****/
    prevPage :function ()		{ 		$("#pagingdiv").pagination('prevPage');				},
    /****调到当前页数的下一页****/
    nextPage: function() 		{    	$("#pagingdiv").pagination('nextPage');    	 		},
    /****获得分页数量的总数****/
    getPagesCount:function () 	{ 		$("#pagingdiv").pagination('getPagesCount');   		},
    
    getCurrentPage:function() 	{  		$("#pagingdiv").pagination('getCurrentPage');   	},
    /*****是分页按钮不可用******/
    disable:function (){	$("#pagingdiv").pagination('disable');    },
    /*****是分页按钮可用******/
    enable:function (){	$("#pagingdiv").pagination('enable');    },
    /*******销毁 分页插件 ********/
    destroy:function (){	$("#pagingdiv").pagination('destroy');    },
    /***重新绘制分页***/ 
    redraw:function (){		$("#pagingdiv").pagination('redraw');     },
    /**  修改分页的总数***/
    updateItems:function (totalNum){  $("#pagingdiv").pagination('updateItemsOnPage', totalNum?totalNum:0);  },
    
    /**  修改分页的总数***/
    updateItems:function (totalNum){  $("#pagingdiv").pagination('updateItemsOnPage', totalNum?totalNum:0);  },
	
    /**********分页中显示 多少个 按钮，  *********/
    drawPage:function(showbNum) {$("#pagingdiv").pagination('drawPage', showbNum?showbNum:8);     }
}

