//document.write("<script src='js/pages/articles.js' type='text/javascript'></script>")



jQuery(document).ready(function() {
	//此处将该函数放到了function中，为了在html全部加载完成后，再发起ajax请求 
	Article.InitPage(32);//32是初始左菜单类型，恋爱婚姻
	
	 pageNumber=1;
	 
	  var menus = $('.zyzx_div_left01nav .zyzx_div_left01navl01, .zyzx_div_left01nav .zyzx_div_left01navl02');
	//  var i_type;
	  menus.find('a').click(function(e) {
		  menus.removeClass().addClass('zyzx_div_left01navl02');
		  var current = $(e.target).parent();
		  current.removeClass().addClass('zyzx_div_left01navl01');
		  
		  var var_type=[32,33,34,35,36,37,38,39];
		  Article.i_type = menus.index(current);
		  //alert(var_type[Article.i_type+1]);
		  Article.caseSwitch(var_type[Article.i_type],Article.pageSize , pageNumber,0);
	  });
	
});


Article = {
		
		baseUrl:"http://www.zhuclass.com/case",
		
		pageNumber:1,
		
		pageSize:5,
		
		i_type:0,
		
		InitPage : function(artcletype){
			  Article.caseSwitch(artcletype,Article.pageSize,1,0);
			  
			
		},
		
		//rest方式 ，传入 类型，每页显示的大小数量，第几个页数
		caseSwitch:function(artcletype,pageSize,pageNo,moreTabCaseTp){
			var restUrl = ( Article.baseUrl +"/"+artcletype+"/"+pageSize+"/"+pageNo +"/"+moreTabCaseTp ) ;
			console.log("restUrl="+restUrl);
			//获取分页内容
			$.ajax({
				  beforeSend:Article.loading,
				  complete:Article.loaded,
				  type: "GET",
				  url: restUrl,
				  dataType: "json",
				  success: function(data){
					  /** 成功构造内容**/
					  
					  Article.createImgContent(pageNo,data);
					} 
				});
			return null;
		},
		
		
		//为图片选择一批，分页查询的形式
		/*tabswitch:function(artcletype){
			var restUrl = "http://www.zhuclass.com/case/listTopCase?type="+"/"+artcletype) ;
			console.log("restUrl="+restUrl);
			//获取分页内容
			$.ajax({
				  beforeSend:Article.loading,
				  complete:Article.loaded,
				  type: "GET",
				  url: restUrl,
				  dataType: "json",
				  success: function(data){
					  
					  Article.createImgContent(1,data);
				      
					} 
				});
			return null;
		},*/
		//构造 分里面的内容 
		createContent:function( curPage,pageData ){
	         
			//1构造分页表的数据 
			var html  = $("#newsTemplate").render(pageData);
			//清空内容；
			$("#contents_article").html("");
			//填充内容 			
			$("#contents_article").append(html);
			//2构造图片分页的数据 
			var html  = $("#imageTemplate").render(pageData);
			//清空内容；
			$("#contents_image").html("");
			//填充内容 			
			$("#contents_image").append(html);
			//3构造热点文章分页的数据 
			var html  = $("#caseTemplate").render(pageData);
			//清空内容；
			$("#casediv").html("");
			//填充内容 			
			$("#casediv").append(html);
          
			if(!curPage || curPage<=1){
				//显示分页的栏目pagingdiv  : 是显示分页栏目的div
				$("#pagingdiv").pagination({
				        items:       pageData.totalCount? pageData.totalCount:0,
				        itemsOnPage: pageData.pageSize?pageData.pageSize:5,
				        onPageClick: Article.onPage
				 });
			 }
			
			
		},
		createImgContent:function( curPage,pageData ){
	         
			//alert(pageData.data[0].user_id);
			//2构造图片分页的数据 
			var html  = $("#moreNewTemplate").render(pageData);
			var content="#contents_image"+Article.i_type;
			console.log(content);
			//清空内容；
			$(content).html("");
			//填充内容 			
			$("#contents_image"+Article.i_type).append(html);
			$("#hotCase").click(function(e) {
				Article.caseSwitch(Article.i_type,Article.pageSize , Article.pageNumber,1);//1表示热门案例
			   });
			$("#tabCase").click(function(e) {
				Article.caseSwitch(Article.i_type,Article.pageSize , Article.pageNumber,0);//0表示新增案例
			   });
			if(!curPage || curPage<=1){
				//显示分页的栏目pagingdiv  : 是显示分页栏目的div
				$("#pagingdiv").pagination({
				        items:       pageData.totalCount? pageData.totalCount:0,
				        itemsOnPage: pageData.pageSize?pageData.pageSize:5,
				        onPageClick: Article.onPage
				 });
			 }
		},
		
		/******
		 * 点击分页按钮的分页事件 
		 * 
		 ***********/
		onPage:function(pageNumber, event){
			/***********分页按钮*************/
			//alert("fsad");
			//console.log(pageNumber);
			//console.log(event);
			//传递 类型   每页多大 ，页数   需要写后台页面
			Article.caseMore(1, Article.pageSize , pageNumber) ;
		},
		/* 根据ID 查看详情   */
		detail:function (articleId){

		},
		//正在加载中
		loading:function(showyn){
			//console.log(jQuery('#loading-one').html());
			jQuery('#loading-one').empty().append('载入中。。。。.').parent().fadeIn('slow') ;  
		},
		loaded:function(showyn){			
			jQuery('#loading-one').empty().append('加载完毕').parent().fadeOut('slow');  
		},
		
};

/*****pagingdiv  这个ID 大家可以参考 ，就固定把分页 div 设置为这个ＩＤ *************** 
 * 
 * 下面都是 手动操作分页的 基本很少用到
 * 
 *******http://flaviusmatis.github.io/simplePagination.js/#page-1******/
var paging = {
	/** 是分页的 数字按钮切换到 pageNo 的位置 **/
	selectPage:function(pageNo)	{  		$("#pagingdiv").pagination('selectPage',pageNo );  	},
	/****调到当前页数的前一页****/
    prevPage :function ()		{ 		$("#pagingdiv").pagination('prevPage');				},
    /****调到当前页数的下一页****/
    nextPage: function() 		{    	$("#pagingdiv").pagination('nextPage');    	 		},
    /****获得分页数量的总数****/
    getPagesCount:function () 	{ 		$("#pagingdiv").pagination('getPagesCount');   		},
    
    getCurrentPage:function() 	{  		$("#pagingdiv").pagination('getCurrentPage');   	},
    /*****是分页按钮不可用******/
    disable:function (){	$("#pagingdiv").pagination('disable');    },
    /*****是分页按钮可用******/
    enable:function (){	$("#pagingdiv").pagination('enable');    },
    /*******销毁 分页插件 ********/
    destroy:function (){	$("#pagingdiv").pagination('destroy');    },
    /***重新绘制分页***/ 
    redraw:function (){		$("#pagingdiv").pagination('redraw');     },
    /**  修改分页的总数***/
    updateItems:function (totalNum){  $("#pagingdiv").pagination('updateItemsOnPage', totalNum?totalNum:0);  },
    
    /**  修改分页的总数***/
    updateItems:function (totalNum){  $("#pagingdiv").pagination('updateItemsOnPage', totalNum?totalNum:0);  },
	
    /**********分页中显示 多少个 按钮，  *********/
    drawPage:function(showbNum) {$("#pagingdiv").pagination('drawPage', showbNum?showbNum:8);     }
}

