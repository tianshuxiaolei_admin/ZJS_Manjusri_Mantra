var EMAIL_ACT = {
        init : function() {
            EMAIL_ACT.initUI();
            EMAIL_ACT.initEvent();
            EMAIL_ACT.uiState();
        },

        initUI : function() {
        },

        initEvent : function() {
        }, 

        uiState : function() {
        },

        goToMailAddr : function() {
            var url = EMAIL_ACT.getMailAddr();
            window.open("http://" + url);
        },

        getMailAddr : function() {
            mail_url = window.opener.email_addr.split('@')[1];
            mail_url = mail_url.toLowerCase();
            if (mail_url == '163.com') {
                return 'mail.163.com';
            } else if (mail_url == 'vip.163.com') {
                return 'vip.163.com';
            } else if (mail_url == '126.com') {
                return 'mail.126.com';
            } else if (mail_url == 'qq.com' || mail_url == 'vip.qq.com' || mail_url == 'foxmail.com') {
                return 'mail.qq.com';
            } else if (mail_url == 'gmail.com') {
                return 'mail.google.com';
            } else if (mail_url == 'sohu.com') {
                return 'mail.sohu.com';
            } else if (mail_url == 'tom.com') {
                return 'mail.tom.com';
            } else if (mail_url == 'vip.sina.com') {
                return 'vip.sina.com';
            } else if (mail_url == 'sina.com.cn' || mail_url == 'sina.com') {
                return 'mail.sina.com.cn';
            } else if (mail_url == 'yahoo.com.cn' || mail_url == 'yahoo.cn') {
                return 'sg.mail.yahoo.com';
            } else if (mail_url == 'yeah.net') {
                return 'www.yeah.net';
            } else if (mail_url == '21cn.com') {
                return 'mail.21cn.com';
            } else if (mail_url == 'hotmail.com') {
                return 'www.hotmail.com';
            } else if (mail_url == 'sogou.com') {
                return 'mail.sogou.com';
            } else if (mail_url == '188.com') {
                return 'www.188.com';
            } else if (mail_url == '139.com') {
                return 'mail.10086.cn';
            } else if (mail_url == '189.cn') {
                return 'webmail15.189.cn/webmail';
            } else if (mail_url == 'wo.com.cn') {
                return 'mail.wo.com.cn/smsmail';
            } else if (mail_url == '139.com') {
                return 'mail.10086.cn';
            } else {
                if (mail_url.substring(0, 4) == 'mail') {
                    return mail_url;
                } else {
                    return "mail" + mail_url
                }
            }
        },

        sendEmailAgain : function() {
            window.location='http://www.zhuclass.com/register/activeadd?checkemail=' + window.opener.randomcode +'&rdid=' + window.opener.user_id + '&repit=Y';
        }
};
jQuery(document).ready(EMAIL_ACT.init);