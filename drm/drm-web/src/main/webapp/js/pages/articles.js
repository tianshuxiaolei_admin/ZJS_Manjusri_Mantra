/**
 * 文章类型 ; 1= 通知公告 2= 学院动态 3= 行业动态 4= 考试指南 5=备考指南 6=考试动态 7=报考指南 8=制定用书 9=证书与就业 10=活动
 */

Article = {
		
		baseUrl:"http://www.zhuclass.com/article",
		
		pageSize:10,
		
		InitPage : function(artcletype, count){
			  Article.goPage(artcletype,Article.pageSize,1);
			  
			  pageNumber=1;
			  /*$("#select1").click(function(e) {
				  Article.goPage(1,Article.pageSize , pageNumber);
			  });
			  $("#select2").click(function(e) {
				  Article.goPage(2,Article.pageSize , pageNumber);
				  $("#select2_span").attr("alxx_text02","alxx_text01") ;                 
			  });
			  $("#select3").click(function(e) {
				  Article.goPage(3,Article.pageSize , pageNumber)
			  });
			  $("#select4").click(function(e) {
				  Article.goPage(4,Article.pageSize , pageNumber)
			   });
			  $("#select5").click(function(e) {
				  Article.goPage(5,Article.pageSize , pageNumber)
			   });
			  $("#select6").click(function(e) {
				  Article.goPage(6,Article.pageSize , pageNumber)
			   });
			  $("#select7").click(function(e) {
				  Article.goPage(7,Article.pageSize , pageNumber)
			   });
			  $("#select8").click(function(e) {
				  Article.goPage(8,Article.pageSize , pageNumber)
			   });
			  $("#select9").click(function(e) {
				  Article.goPage(9,Article.pageSize , pageNumber)
			   });
			  $("#select10").click(function(e) {
				  Article.goPage(10,Article.pageSize , pageNumber)
			   });*/
			  
			  var menus = $('.alxx_divt01 .alxx_text02, .alxx_divt01 .alxx_text01');
			  menus.find('a').click(function(e) {
				  menus.removeClass().addClass('alxx_text02');
				  var current = $(e.target).parent();
				  current.removeClass().addClass('alxx_text01');
				  
				  var i = menus.index(current);
				  count=0;// 需要改
				  Article.goPage(i + count,Article.pageSize , pageNumber)
			  });
			
		},
		
		//rest方式 ，传入 类型，每页显示的大小数量，第几个页数
		goPage:function(artcletype,pageSize,pageNo){
			var restUrl = ( Article.baseUrl +"/"+artcletype+"/"+pageSize+"/"+pageNo ) ;
			//console.log("restUrl="+restUrl);
			//alert("restUrl=");
			//获取分页内容
			$.ajax({
				  beforeSend:Article.loading,
				  complete:Article.loaded,
				  type: "GET",
				  url: restUrl,
				  dataType: "json",
				  success: function(data){
					  /** 成功构造内容**/
					  Article.createContent(pageNo,data);
					} 
				});
			return null;
		},
		
		/* 根据ID 查看详情   */
		detail:function (articleId){

		},
		//正在加载中
		loading:function(showyn){
			//console.log(jQuery('#loading-one').html());
			jQuery('#loading-one').empty().append('载入中。。。。.').parent().fadeIn('slow') ;  
		},
		loaded:function(showyn){			
			jQuery('#loading-one').empty().append('加载完毕').parent().fadeOut('slow');  
		},
		//构造 分里面的内容 
		createContent:function( curPage,pageData ){
			//test data 
			var data = [{ "articleId": "1", "title": "testTitle", "articleCdt": "20100101"}];//[{ "Name": "朋友 1-1", "Age": 18, "Friends": null }, { "Name": "朋友 1-2", "Age": 34, "Friends": null }] }, { "Name": "不二当", "Age": 27, "Friends": [{ "Name": "朋友 2-1", "Age": 19, "Friends": null }, { "Name": "朋友 2-2", "Age": 19, "Friends": null }] }];
		   
			//根据模板构造内容
			var html  = $("#newsTemplate").render(pageData.data);
			//清空内容；
			$("#contents_article").html("");
			//填充内容 			
			$("#contents_article").append(html);
       
			if(!curPage || curPage<=1){
				//显示分页的栏目pagingdiv  : 是显示分页栏目的div
				$("#pagingdiv").pagination({
				        items:       pageData.totalCount? pageData.totalCount:0,
				        itemsOnPage: pageData.pageSize?pageData.pageSize:10,
				        onPageClick: Article.onPage
				 });
			 }
		},
		/******
		 * 点击分页按钮的分页事件 
		 * 
		 ***********/
		onPage:function(pageNumber, event){
			/***********分页按钮*************/
			//alert("fsad");
			//console.log(pageNumber);
			//console.log(event);
			//传递 类型   每页多大 ，页数   需要写后台页面
			Article.goPage(1, Article.pageSize , pageNumber) ;
		},
		
		
};

/*****pagingdiv  这个ID 大家可以参考 ，就固定把分页 div 设置为这个ＩＤ *************** 
 * 
 * 下面都是 手动操作分页的 基本很少用到
 * 
 *******http://flaviusmatis.github.io/simplePagination.js/#page-1******/
var paging = {
	/** 是分页的 数字按钮切换到 pageNo 的位置 **/
	selectPage:function(pageNo)	{  		$("#pagingdiv").pagination('selectPage',pageNo );  	},
	/****调到当前页数的前一页****/
    prevPage :function ()		{ 		$("#pagingdiv").pagination('prevPage');				},
    /****调到当前页数的下一页****/
    nextPage: function() 		{    	$("#pagingdiv").pagination('nextPage');    	 		},
    /****获得分页数量的总数****/
    getPagesCount:function () 	{ 		$("#pagingdiv").pagination('getPagesCount');   		},
    
    getCurrentPage:function() 	{  		$("#pagingdiv").pagination('getCurrentPage');   	},
    /*****是分页按钮不可用******/
    disable:function (){	$("#pagingdiv").pagination('disable');    },
    /*****是分页按钮可用******/
    enable:function (){	$("#pagingdiv").pagination('enable');    },
    /*******销毁 分页插件 ********/
    destroy:function (){	$("#pagingdiv").pagination('destroy');    },
    /***重新绘制分页***/ 
    redraw:function (){		$("#pagingdiv").pagination('redraw');     },
    /**  修改分页的总数***/
    updateItems:function (totalNum){  $("#pagingdiv").pagination('updateItemsOnPage', totalNum?totalNum:0);  },
    
    /**  修改分页的总数***/
    updateItems:function (totalNum){  $("#pagingdiv").pagination('updateItemsOnPage', totalNum?totalNum:0);  },
	
    /**********分页中显示 多少个 按钮，  *********/
    drawPage:function(showbNum) {$("#pagingdiv").pagination('drawPage', showbNum?showbNum:8);     }
}


jQuery(document).ready(function() {
	Article.InitPage(1);//此处将该函数放到了function中，为了在html全部加载完成后，再发起ajax请求
});
