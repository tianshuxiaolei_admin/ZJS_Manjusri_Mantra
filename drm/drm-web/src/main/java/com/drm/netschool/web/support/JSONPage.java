package com.drm.netschool.web.support;

import java.util.List;

public class JSONPage {
	private long total;
    private int page;
    private long records;
    private int pagesize;
    private List<?> rows;

    public JSONPage() {
        super();
    }

    public JSONPage(long total, int page, int records) {
        super();
        this.total = total;
        this.page = page;
        this.records = records;
    }

    public JSONPage(long total, int page, int records, int pagesize) {
        super();
        this.total = total;
        this.page = page;
        this.records = records;
        this.pagesize = pagesize;
    }

    public JSONPage(long total, int page, long records, int pagesize, List<?> rows) {
        super();
        this.total = total;
        this.page = page;
        this.records = records;
        this.rows = rows;
        this.pagesize = pagesize;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public long getRecords() {
        return records;
    }

    public void setRecords(long records) {
        this.records = records;
    }

    public int getPagesize() {
        return pagesize;
    }

    public void setPagesize(int pagesize) {
        this.pagesize = pagesize;
    }

    public List<?> getRows() {
        return rows;
    }

    public void setRows(List<?> rows) {
        this.rows = rows;
    }
}
