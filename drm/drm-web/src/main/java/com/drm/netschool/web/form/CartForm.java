package com.drm.netschool.web.form;

import java.util.List;

import com.drm.common.util.NumberUtil;
import com.drm.netschool.entity.DrmSalesPromotion;
import com.drm.netschool.entity.DrmStudyCoin;
import com.drm.netschool.entity.DrmUserSinfo;
import com.drm.netschool.entity.extend.CartInfo;

public class CartForm extends BaseForm {
	
	private CartInfo cart;
	
	private List<CartInfo> cartList;
	
	private DrmUserSinfo userInfo;
	
	private DrmStudyCoin studyCoin;
	
	private DrmSalesPromotion promotion;
	
	private boolean useState;	//学习币使用状态
	
	private List<Long> idsList;	//购物车ids
	
	//学习币对应的钱数（单位：分）
	public Long getCoin2Fen(){
		if(studyCoin == null || NumberUtil.parseInteger(studyCoin.getScoinCnt()) == 0){
			return 0L;
		}
		if(userInfo == null || NumberUtil.parseInteger(userInfo.getUserLearncredit()) == 0){
			return 0L;
		}
		Integer total = userInfo.getUserLearncredit() * (NumberUtil.parseInteger(studyCoin.getRmbCnt()) * 100)/NumberUtil.parseInteger(studyCoin.getScoinCnt());
		return Long.valueOf(total.toString());
	}
	
	public Long getUseCoinAmount(){
		if(!useState){
			return 0L;
		}
		
		if(getCount() - getDiscountAmount() - getCoin2Fen() > 0){
			return getCoin2Fen();
		}else{
			return getCount() - getDiscountAmount();
		}
	}
	
	public Long getCount() {
		Long count = 0L;
		if(cartList != null){
			for(CartInfo cart : cartList){
				count += NumberUtil.parseLong(cart.getCurPrice());
			}
		}
		return count;
	}
	
	public Long getDiscountAmount(){
		if(promotion == null){
			return 0L;
		}
		
		if(promotion.getPromotionTp() == 1){
			if(getCount() > promotion.getPromotionMoney()){
				return promotion.getReduceMoney();
			}
		}else if(promotion.getPromotionTp() == 2){
			if(getCount() > promotion.getPromotionMoney()){
				return getCount() * promotion.getReduceMoney();
			}
		}
		return 0L;
	}
	
	public Long getAmount() {
		if(userInfo != null && useState){
			if(getCount() - getDiscountAmount() - getCoin2Fen() > 0){
				return getCount() - getDiscountAmount() - getCoin2Fen();
			}else{
				return 0L;
			}
		}
		return getCount() - getDiscountAmount();
	}

	public DrmUserSinfo getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(DrmUserSinfo userInfo) {
		this.userInfo = userInfo;
	}

	public DrmSalesPromotion getPromotion() {
		return promotion;
	}

	public void setPromotion(DrmSalesPromotion promotion) {
		this.promotion = promotion;
	}

	public boolean isUseState() {
		return useState;
	}

	public void setUseState(boolean useState) {
		this.useState = useState;
	}

	public List<Long> getIdsList() {
		return idsList;
	}

	public void setIdsList(List<Long> idsList) {
		this.idsList = idsList;
	}

	public List<CartInfo> getCartList() {
		return cartList;
	}

	public void setCartList(List<CartInfo> cartList) {
		this.cartList = cartList;
	}

	public CartInfo getCart() {
		return cart;
	}

	public void setCart(CartInfo cart) {
		this.cart = cart;
	}

	public DrmStudyCoin getStudyCoin() {
		return studyCoin;
	}

	public void setStudyCoin(DrmStudyCoin studyCoin) {
		this.studyCoin = studyCoin;
	}

}
