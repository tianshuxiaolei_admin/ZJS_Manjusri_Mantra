package com.drm.netschool.web.form;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.drm.netschool.entity.extend.ResourceInfo;
import com.drm.netschool.entity.extend.UserInfo;

public class ResourceForm extends BaseForm{
	
	private int type;	//0:最新上传   1： 热门文件 2：下载排行
	
	private String keyword;
	
	private int tagId;
	
	private ResourceInfo resourceInfo;
	
	private List<ResourceInfo> downUserList;
	
	private List<ResourceInfo> resourceList;
	
	private List<ResourceInfo> latestResourceList;
	
	private List<ResourceInfo> hotestResourceList;
	
	private List<ResourceInfo> downResourceList;
	
	private List<UserInfo> shareExpertList;
	
	private static Map<Integer, String> navTitle = new HashMap<Integer, String>();
	
	static{
		navTitle.put(0, "最新上传");
		navTitle.put(1, "热门文件");
		navTitle.put(2, "下载排行");
	}

	public List<ResourceInfo> getLatestResourceList() {
		return latestResourceList;
	}

	public void setLatestResourceList(List<ResourceInfo> latestResourceList) {
		this.latestResourceList = latestResourceList;
	}

	public List<ResourceInfo> getHotestResourceList() {
		return hotestResourceList;
	}

	public void setHotestResourceList(List<ResourceInfo> hotestResourceList) {
		this.hotestResourceList = hotestResourceList;
	}

	public List<ResourceInfo> getDownResourceList() {
		return downResourceList;
	}

	public void setDownResourceList(List<ResourceInfo> downResourceList) {
		this.downResourceList = downResourceList;
	}

	public List<UserInfo> getShareExpertList() {
		return shareExpertList;
	}

	public void setShareExpertList(List<UserInfo> shareExpertList) {
		this.shareExpertList = shareExpertList;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public List<ResourceInfo> getResourceList() {
		return resourceList;
	}

	public void setResourceList(List<ResourceInfo> resourceList) {
		this.resourceList = resourceList;
	}

	public Map<Integer, String> getNavTitle() {
		return navTitle;
	}

	public List<ResourceInfo> getDownUserList() {
		return downUserList;
	}

	public void setDownUserList(List<ResourceInfo> downUserList) {
		this.downUserList = downUserList;
	}

	public ResourceInfo getResourceInfo() {
		return resourceInfo;
	}

	public void setResourceInfo(ResourceInfo resourceInfo) {
		this.resourceInfo = resourceInfo;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public int getTagId() {
		return tagId;
	}

	public void setTagId(int tagId) {
		this.tagId = tagId;
	}
}
