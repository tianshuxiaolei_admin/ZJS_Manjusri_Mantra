package com.drm.netschool.web.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.drm.netschool.entity.DrmArea;
import com.drm.netschool.service.DrmAreaService;

/**
 * Created by CUIJB on 2015年7月29日
 */
@Controller
@RequestMapping("/area/")
public class AreaController {
	
	private Logger LOGGER = LoggerFactory.getLogger(AreaController.class);
	
	@Autowired
	private DrmAreaService areaService;
	
	@RequestMapping(value = "cities/{pCode}", method = RequestMethod.GET)
	public List<DrmArea> getCityList(@PathVariable String pCode){
		LOGGER.info("查询城市集合 pcode:" + pCode);
		return areaService.getCityListByPcode(pCode);
	}
}
