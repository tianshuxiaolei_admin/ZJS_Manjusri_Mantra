package com.drm.netschool.web.form;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.drm.netschool.entity.extend.GroupInfo;
import com.drm.netschool.entity.extend.UserInfo;

public class UserForm extends BaseForm {

	private int type;

	private List<UserInfo> userInfoList; // 学员空间
	private List<GroupInfo> groupInfos; // 学习群组
	
	private List<UserInfo> rqUserList;	//人气校友.
	
	private static Map<Integer, String> navTitle = new HashMap<Integer, String>();
	private static Map<Integer, String> groupNavTitle = new HashMap<Integer, String>();
	private static Map<Integer, String> universityNavTitle = new HashMap<Integer, String>();
	
	static{
		navTitle.put(0, "人气学员");
		navTitle.put(1, "全部学员");
		
		groupNavTitle.put(0, "火爆群组");
		groupNavTitle.put(1, "全部群组");
		
		universityNavTitle.put(0, "企业大学");
		universityNavTitle.put(1, "最新加入");
		universityNavTitle.put(2, "人气大学");
	}
	
	public Map<Integer, String> getUniversityNavTitle() {
		return universityNavTitle;
	}
	
	public Map<Integer, String> getGroupNavTitle() {
		return groupNavTitle;
	}
	
	public Map<Integer, String> getNavTitle() {
		return navTitle;
	}
	
	public List<UserInfo> getUserInfoList() {
		return userInfoList;
	}

	public void setUserInfoList(List<UserInfo> userInfoList) {
		this.userInfoList = userInfoList;
	}

	public List<GroupInfo> getGroupInfos() {
		return groupInfos;
	}

	public void setGroupInfos(List<GroupInfo> groupInfos) {
		this.groupInfos = groupInfos;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public List<UserInfo> getRqUserList() {
		return rqUserList;
	}

	public void setRqUserList(List<UserInfo> rqUserList) {
		this.rqUserList = rqUserList;
	}

}
