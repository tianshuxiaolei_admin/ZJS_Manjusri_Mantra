package com.drm.netschool.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.drm.common.util.ConfigUtil;
import com.drm.common.util.CookieUtil;
import com.drm.common.util.constants.Constants;
import com.drm.netschool.entity.DrmUser;
import com.drm.netschool.entity.SumerTag;
import com.drm.netschool.entity.extend.ResourceInfo;
import com.drm.netschool.entity.extend.UserInfo;
import com.drm.netschool.service.DrmSystemSettingService;
import com.drm.netschool.service.DrmUserCacheService;
import com.drm.netschool.service.DrmUserService;
import com.drm.netschool.service.ResourceService;
import com.drm.netschool.service.TagService;
import com.drm.netschool.web.annotation.AccessRequired;
import com.drm.netschool.web.form.ResourceForm;


@Controller
@RequestMapping("/resource/")
public class ResourceController {
	
	private Logger LOGGER = LoggerFactory.getLogger(ResourceController.class);
	
	@Autowired
	private ResourceService resourceService;
	
	@Autowired
	private DrmUserService userService;
	
	@Autowired
	private DrmUserCacheService userCacheService;
	
	@Autowired
	private DrmSystemSettingService systemSettingService;
	
	@Autowired
	private TagService tagService;
	
	@RequestMapping(value = "info")
	public String info(@ModelAttribute("form") ResourceForm form, HttpServletRequest request, HttpServletResponse response){
		form.setMetaInfo(systemSettingService.getDrmSystemSetting());
		
		//最新上传
		form.setLatestResourceList(resourceService.getResourceInfoList(new ResourceInfo(), 1, 5));
		//热门文件
		ResourceInfo hotInfo = new ResourceInfo();
		hotInfo.setOrderBy("viewCnt");
		form.setHotestResourceList(resourceService.getResourceInfoList(hotInfo, 1, 5));
		//下载排行
		ResourceInfo downInfo = new ResourceInfo();
		downInfo.setOrderBy("downCnt");
		form.setDownResourceList(resourceService.getResourceInfoList(downInfo, 1, 5));
		//案例分析达人
		form.setShareExpertList(userService.getUserInfoList(new UserInfo(), 1, 5));
		return "resource/document";
	}
	
	@RequestMapping(value = "list")
	public String list(@ModelAttribute("form") ResourceForm form, HttpServletRequest request, HttpServletResponse response){
		form.setMetaInfo(systemSettingService.getDrmSystemSetting());
		
		ResourceInfo resource = new ResourceInfo();
		if(form.getType() == 1)
			resource.setOrderBy("viewCnt");
		else if(form.getType() == 2){
			resource.setOrderBy("downCnt");
		}
		int total = resourceService.getResourceInfoCount(resource);
		form.setTotalRecords(total);
		if(total > 0)
			form.setResourceList(resourceService.getResourceInfoList(resource, form.getPage(), form.getPageSize()));
		return "resource/list";
	}
	
	@RequestMapping(value = "detail/{id}")
	public String detail(@PathVariable Long id, @ModelAttribute("form") ResourceForm form){
		form.setMetaInfo(systemSettingService.getDrmSystemSetting());
		
		LOGGER.info("resourceDetail:::" + id);
		form.setResourceInfo(resourceService.getResourceInfo(id));
		
		ResourceInfo entity = new ResourceInfo();
		entity.setId(id);
		form.setDownUserList(resourceService.getDownUserList(entity, 1, 6));
		return "resource/detail";
	}
	
	@RequestMapping(value = "search")
	public String getByTag(@ModelAttribute("form") ResourceForm form){
		form.setMetaInfo(systemSettingService.getDrmSystemSetting());
		
		ResourceInfo entity = new ResourceInfo();
		entity.setTagId(form.getTagId());
		int total = resourceService.getResourceInfoCount(entity);
		form.setTotalRecords(total);
		if(total > 0)
			form.setResourceList(resourceService.getResourceInfoList(entity, form.getPage(), form.getPageSize()));
		
		SumerTag tag = tagService.getById(form.getTagId());
		form.setKeyword(tag == null ? "" : tag.getName());
		return "resource/document-tag";
	}
	
	@AccessRequired
	@RequestMapping(value = "jump/{resourceId}/{fileId}")
	public String downloadJump(@PathVariable Long resourceId, @PathVariable String fileId, @ModelAttribute("form") ResourceForm form,HttpServletRequest request, HttpServletResponse response) {
		 
		ResourceInfo entity = resourceService.getResourceInfo(resourceId);
		LOGGER.info("resourceId:::" + resourceId +",fileId:::" + fileId);
		CookieUtil cookieUtil = new CookieUtil(request, response, ConfigUtil.getString("cookie.domain"));
    	String key = cookieUtil.getCookie(Constants.COOKIE_USER_ID,false);
    	
    	DrmUser user = userCacheService.getUserCache(key);
    	if(user == null){
    		return "login/login";
    	}
		entity.setAuthorId( user.getUserId());
		entity.setUserLnm( user.getUserLnm());
		
		resourceService.addDownLoadCount( entity );
		resourceService.setDownLoadWeekCount( entity );
		resourceService.insertDownLoadRecord(entity);
		
		String tempDk = resourceService.takedownloadKey();
		return "redirect:" + form.getDrmDownUrl(entity.getFileId(), entity.getName(),tempDk);
	}
}
