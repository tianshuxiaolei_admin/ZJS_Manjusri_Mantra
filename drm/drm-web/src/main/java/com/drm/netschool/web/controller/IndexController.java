package com.drm.netschool.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.drm.common.util.DateUtil;
import com.drm.netschool.entity.DrmArticles;
import com.drm.netschool.entity.DrmCourseType;
import com.drm.netschool.entity.SumerSharedResource;
import com.drm.netschool.entity.extend.GroupInfo;
import com.drm.netschool.entity.extend.RecommendCourse;
import com.drm.netschool.entity.extend.UserInfo;
import com.drm.netschool.service.DrmArticlesService;
import com.drm.netschool.service.DrmCourseService;
import com.drm.netschool.service.DrmCourseTypeService;
import com.drm.netschool.service.DrmSystemSettingService;
import com.drm.netschool.service.DrmUserService;
import com.drm.netschool.service.GroupService;
import com.drm.netschool.service.ResourceService;
import com.drm.netschool.web.form.IndexForm;

/**
 * Created by CUIJB on 2015年7月25日
 */
@Controller
@RequestMapping("/")
public class IndexController {
	
	private Logger LOGGER = LoggerFactory.getLogger(IndexController.class);
	
	@Autowired
	private DrmUserService userService;
	
	@Autowired
	private GroupService groupService;
	
	@Autowired
	private DrmCourseTypeService courseTypeService;
	
	@Autowired
	private DrmCourseService courseService;
	
	@Autowired
	private DrmArticlesService articlesService;
	
	@Autowired
	private ResourceService sumerSharedResourceService;
	
	@Autowired
	private DrmSystemSettingService systemSettingService;

	@RequestMapping("")
    public String index(@ModelAttribute("form") IndexForm form, HttpServletRequest request, HttpServletResponse response) {
		LOGGER.info("index:::" + DateUtil.current());
		form.setMetaInfo(systemSettingService.getDrmSystemSetting());
		//课程前5条数据
		DrmCourseType courseType = new DrmCourseType();
		courseType.setCourseClass(2);
		courseType.setUseYn(0);
		form.setCourseTypeList(courseTypeService.getListByPage(courseType, 1, 5));
		
		//公开课
		RecommendCourse rc = new RecommendCourse();
		rc.setCourseClass("1");
		form.setCourseList(courseService.getRecommendCourses(rc, 1, 4));
		
		//精品课程
		RecommendCourse jpCourse = new RecommendCourse();
		jpCourse.setCourseGoodteacher(1);
		form.setJpCourseList(courseService.getRecommendCourses(jpCourse, 1, 4));
		//轮播图
		DrmArticles tArticlesList = new DrmArticles();
		tArticlesList.setArticleTp(203);
		tArticlesList.setArticlePublishyn(1);
		form.setlArticlesList(articlesService.getListByPage(tArticlesList, 1, 4));
		//专家团队
		DrmArticles zArticles = new DrmArticles();
		zArticles.setArticleTp(8);
		form.setArticlesList(articlesService.getListByPage(zArticles, 1, 5));
		
		//学院动态
		DrmArticles sArticles = new DrmArticles();
		sArticles.setArticleTp(2);
		form.setsArticlesList(articlesService.getListByPage(sArticles, 1, 13));
		
		//行业动态
		DrmArticles iArticles = new DrmArticles();
		iArticles.setArticleTp(3);
		form.setiArticlesList(articlesService.getListByPage(iArticles, 1, 13));
		
		//实务技能
		RecommendCourse swjn = new RecommendCourse();
		swjn.setCourseClass("2");
		swjn.setCourseTp(1L);
		form.setSwjnList(courseService.getRecommendCourses(swjn, 1, 8));
		
		//政策法规
		RecommendCourse zcfg = new RecommendCourse();
		zcfg.setCourseClass("2");
		zcfg.setCourseTp(2L);
		form.setZcfgList(courseService.getRecommendCourses(zcfg, 1, 8));
		//职业认证
		RecommendCourse zyrz = new RecommendCourse();
		zyrz.setCourseClass("2");
		zyrz.setCourseTp(3L);
		form.setZyrzList(courseService.getRecommendCourses(zyrz, 1, 8));
		//投资融资
		RecommendCourse tzrz = new RecommendCourse();
		tzrz.setCourseClass("2");
		tzrz.setCourseTp(4L);
		form.setTzrzList(courseService.getRecommendCourses(tzrz, 1, 8));
		//企业管理
		RecommendCourse qygl = new RecommendCourse();
		qygl.setCourseClass("2");
		qygl.setCourseTp(5L);
		form.setQyglList(courseService.getRecommendCourses(qygl, 1, 8));
		
		//学员空间
		UserInfo user = new UserInfo();
		user.setUserTp(5);
		form.setStudents(userService.getUserInfoList(user, 1, 15));
		//学习群组
		form.setGroupInfos(groupService.getGroupInfoList(new GroupInfo(), 1, 3));
		//资源库
		form.setZykList(sumerSharedResourceService.getListByPage(new SumerSharedResource(), 1, 4));
		return "index";
    }
}
