package com.drm.netschool.web.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.springframework.web.context.ContextLoader;

import com.drm.common.util.ApplicationContextUtil;

/**
 *
 * @author CUIJB
 * @date 2015年3月14日
 */
public class InitApplicationListener extends ContextLoader implements ServletContextListener {

    public InitApplicationListener() {
    }

    /** 
     * @see javax.servlet.ServletContextListener#contextInitialized(javax.servlet.ServletContextEvent)
     */
    @Override
    public void contextInitialized(ServletContextEvent sce) {

        // 初始化Spring容器
        ApplicationContextUtil.applicationContext = initWebApplicationContext(sce.getServletContext());
    }

    /** 
     * @see javax.servlet.ServletContextListener#contextDestroyed(javax.servlet.ServletContextEvent)
     */
    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        closeWebApplicationContext(sce.getServletContext());
    }

}
