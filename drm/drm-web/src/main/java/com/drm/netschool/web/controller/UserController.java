package com.drm.netschool.web.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.drm.common.util.CookieUtil;
import com.drm.common.util.StringUtil;
import com.drm.netschool.entity.DrmUser;
import com.drm.netschool.entity.extend.GroupInfo;
import com.drm.netschool.entity.extend.UserInfo;
import com.drm.netschool.service.DrmSystemSettingService;
import com.drm.netschool.service.DrmUserCacheService;
import com.drm.netschool.service.DrmUserService;
import com.drm.netschool.service.GroupService;
import com.drm.netschool.web.form.UserForm;

@Controller
@RequestMapping("/user/")
public class UserController {
	
	private Logger LOGGER = LoggerFactory.getLogger(UserController.class);
	
	@Autowired
	private DrmUserCacheService userCacheService;

	@Autowired
	private DrmUserService userService;
	
	@Autowired
	private GroupService groupService;
	
	@Autowired
	private DrmSystemSettingService systemSettingService;
	
	@RequestMapping(value = "list")
	public String list(@ModelAttribute("form") UserForm form, HttpServletRequest request, HttpServletResponse response) {
		form.setMetaInfo(systemSettingService.getDrmSystemSetting());
		
		// 学员空间
		UserInfo user = new UserInfo();
		user.setUserTp(5);
		int total = userService.getUserInfoCount(user);
		form.setTotalRecords(total);
		if(total > 0){
			List<UserInfo> list = null;
			if(form.getType() == 0){
				list = userService.getSortUserInfoList(user, form.getPage(), form.getPageSize());
			}else if( form.getType() == 1){
				list = userService.getUserInfoList(user, form.getPage(), form.getPageSize());
			}
			form.setUserInfoList(list);
		}
		return "user/list";
	}
	
	@RequestMapping(value = "group")
	public String group(@ModelAttribute("form") UserForm form, HttpServletRequest request, HttpServletResponse response){
		form.setMetaInfo(systemSettingService.getDrmSystemSetting());
		
		GroupInfo entity = new GroupInfo();
		if(form.getType() == 0)	//火爆群组
			entity.setOrderBy("topicCnt");
		int total = groupService.getGroupInfoCount(entity);
		form.setTotalRecords(total);
		if(total > 0)
			form.setGroupInfos(groupService.getGroupInfoList(entity, form.getPage(), form.getPageSize()));
		return "user/group";
	}
	
	@RequestMapping(value = "cor-university")
	public String getUniversity(@ModelAttribute("form") UserForm form, HttpServletRequest request, HttpServletResponse response) {
		form.setMetaInfo(systemSettingService.getDrmSystemSetting());
		
		// 大学
		UserInfo user = new UserInfo();
		user.setUserTp(4);
		if(form.getType() == 1){
			user.setOrderBy("userCdt");
		}else if(form.getType() == 2){
			user.setOrderBy("shareCnt");
		}
		int total = userService.getSortUserInfoCount(user);
		form.setTotalRecords(total);
		if(total > 0)
			form.setUserInfoList(userService.getSortUserInfoList(user, form.getPage(), form.getPageSize()));
		
		return "user/cor-university";
	}
	
	@RequestMapping(value = "member")
	public String getMember(@ModelAttribute("form") UserForm form, HttpServletRequest request, HttpServletResponse response) {
		form.setMetaInfo(systemSettingService.getDrmSystemSetting());
		
		//人气校友
		UserInfo u = new UserInfo();
		form.setRqUserList(userService.getSortUserInfoList(u, 1, 5));
		
		// 所有会员
		UserInfo user = new UserInfo();
		int total = userService.getUserInfoCount(user);
		form.setTotalRecords(total);
		if(total > 0)
			form.setUserInfoList(userService.getUserInfoList(user, form.getPage(), form.getPageSize()));
		
		//最新推荐会员
		form.setRqUserList(userService.getRecommendUserInfoList());
		return "user/member";
	}
	
	@RequestMapping(value = "detail/{userId}")
	public String detail(@PathVariable Long userId, @ModelAttribute("form") UserForm form){
		form.setMetaInfo(systemSettingService.getDrmSystemSetting());
		
		LOGGER.info("userDetail:::" + userId);
		return "user/detail";
	}
	
	
	
	
	/**
	 * 
	 *  Function:
	 *  功能说明：  通过  cookie里面的值，获取  用户的ID
	 *	 使用说明：space 服务使用接口 补充， 改版做得时候没有加上这个
	 *  @author  wangxiaolei  DateTime 2015年9月12日 下午11:53:15
	 *	返回类型: String    
	 *  @param uid
	 *  @param uidpk
	 *  @return
	 */
	@RequestMapping(value = "getid/{uid}/{uidpk}")
	@ResponseBody
	public String getuserId(@PathVariable String uid , @PathVariable String uidpk ,HttpServletRequest request, HttpServletResponse response){
		//判断这两个ID 是否配对
		boolean  isRightCookie =   CookieUtil.checkCookieIdIsOk(uid,uidpk);
		 //配对后，才进行别操作
		if (!StringUtil.empty(uid))
		{
			DrmUser user = userCacheService.getUserCache(uid);
			 if(isRightCookie && user != null ){
		    	//容器缓存失效
		    	try{
					 return user.getUserId().toString();
				}catch(Exception e){
					LOGGER.error(e.getMessage(),e);
				}
		     }
		}
		response.setStatus(HttpServletResponse.SC_NOT_FOUND);
		return "";
	}
	
	
	
}
