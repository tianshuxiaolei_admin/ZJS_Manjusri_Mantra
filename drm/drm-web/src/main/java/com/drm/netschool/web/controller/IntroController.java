package com.drm.netschool.web.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.drm.common.util.NumberUtil;
import com.drm.common.util.StringUtil;
import com.drm.netschool.entity.DrmCourseComment;
import com.drm.netschool.entity.extend.RecommendCourse;
import com.drm.netschool.service.AgentApplyService;
import com.drm.netschool.service.DrmCourseService;
import com.drm.netschool.service.DrmSystemSettingService;
import com.drm.netschool.web.form.IntroForm;

@Controller
@RequestMapping("/intro/")
public class IntroController {
	
	@Autowired
	private AgentApplyService agentApplyService;
	
	@Autowired
	private DrmCourseService courseService;
	
	@Autowired
	private DrmSystemSettingService systemSettingService;

	@RequestMapping(value = "about.html")
	public String about(@ModelAttribute("form") IntroForm form){
		form.setMetaInfo(systemSettingService.getDrmSystemSetting());
		return "intro/about";
	}
	
	@RequestMapping(value = "aboutUsPro.html")
	public String aboutUsPro(@ModelAttribute("form") IntroForm form){
		form.setMetaInfo(systemSettingService.getDrmSystemSetting());
		return "intro/aboutUsPro";
	}
	
	@RequestMapping(value = "aboutUsvei.html")
	public String getCount(@ModelAttribute("form") IntroForm form){
		form.setMetaInfo(systemSettingService.getDrmSystemSetting());
		return "intro/aboutUsvei";
	}
	
	@RequestMapping(value = "agenJion.html")
	public String agenJion(@ModelAttribute("form") IntroForm form){
		form.setMetaInfo(systemSettingService.getDrmSystemSetting());
		return "intro/agenJion";
	}
	
	@RequestMapping(value = "agent.html")
	public String addAgent(@ModelAttribute("form") IntroForm form){
		form.setMetaInfo(systemSettingService.getDrmSystemSetting());
		return "intro/agent";
	}
	@RequestMapping(value = "agent.html", method = RequestMethod.POST)
	public String doAddAgent(@ModelAttribute("form") IntroForm form, HttpServletRequest request, HttpServletResponse response){
		form.setMetaInfo(systemSettingService.getDrmSystemSetting());
		
		try {
			if(form == null || form.getAgentApply() == null){
				form.putError("cornName", "参数不能为空");
				return "intro/agent";
			}else if(StringUtil.empty(form.getAgentApply().getApplyerName())){
				form.putError("applyerName", "申请企业不能为空");
				return "intro/agent";
			}else if(StringUtil.empty(form.getAgentApply().getCornName())){
				form.putError("cornName", "联系人不能为空");
				return "intro/agent";
			}else if(StringUtil.empty(form.getAgentApply().getApplyerTel())){
				form.putError("applyerTel", "申请电话不能为空");
				return "intro/agent";
			}else if(StringUtil.empty(form.getAgentApply().getApplyerEmail())){
				form.putError("applyerEmail", "申请邮箱不能为空");
				return "intro/agent";
			}else if(StringUtil.empty(form.getAgentApply().getApplyTxt())){
				form.putError("applyTxt", "申请说明不能为空");
				return "intro/agent";
			}
			agentApplyService.add(form.getAgentApply());
			form.putError("cornName", "申请成功");
		} catch (Exception e) {
			e.printStackTrace();
			form.putError("cornName", "申请失败");
		}
		return "intro/agent";
	}
	
	@RequestMapping(value = "contactUs.html")
	public String contactUs(@ModelAttribute("form") IntroForm form){
		form.setMetaInfo(systemSettingService.getDrmSystemSetting());
		return "intro/contactUs";
	}
	
	@RequestMapping(value = "guide.html")
	public String guide(@ModelAttribute("form") IntroForm form){
		form.setMetaInfo(systemSettingService.getDrmSystemSetting());
		return "intro/guide";
	}
	
	@RequestMapping(value = "gdgk.html")
	public String gdgk(@ModelAttribute("form") IntroForm form){
		form.setMetaInfo(systemSettingService.getDrmSystemSetting());
		return "intro/gdgk";
	}
	
	@RequestMapping(value = "yhxy.html")
	public String yhxy(@ModelAttribute("form") IntroForm form){
		form.setMetaInfo(systemSettingService.getDrmSystemSetting());
		return "intro/yhxy";
	}
	
	@RequestMapping(value = "lecturerRecruitment.html")
	public String lecturerRecruitment(@ModelAttribute("form") IntroForm form){
		form.setMetaInfo(systemSettingService.getDrmSystemSetting());
		return "intro/lecturerRecruitment";
	}
	
	@RequestMapping(value = "faceCourse")
	public String faceCourse(@ModelAttribute("form") IntroForm form){
		form.setMetaInfo(systemSettingService.getDrmSystemSetting());
		
		RecommendCourse entity = new RecommendCourse();
		entity.setCourseBoutique(4);
		entity.setCourseOnlinetp(2);
		
		int total = courseService.getRecommendCoursesCount(entity);
		form.setTotalRecords(total);
		if(total > 0){
			List<RecommendCourse> list = courseService.getRecommendCourses(entity, form.getPage(), form.getPageSize());
			form.setRecommendCourseList(list);
		}
		return "intro/faceCourse";
	}
	
	@RequestMapping(value = "one-course")
	public String oneCourse(@ModelAttribute("form") IntroForm form){
		form.setMetaInfo(systemSettingService.getDrmSystemSetting());
		
		RecommendCourse course = new RecommendCourse();
		course.setCurPrice(100L);
		
		int total = courseService.getRecommendCoursesCount(course);
		form.setTotalRecords(total);
		if(total > 0)
			form.setCourseList(courseService.getRecommendCourses(course, form.getPage(), form.getPageSize()));
		return "intro/one-course";
	}
}
