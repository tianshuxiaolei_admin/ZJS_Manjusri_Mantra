package com.drm.netschool.web.form;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.drm.common.util.NumberUtil;
import com.drm.netschool.entity.DrmArticles;
import com.drm.netschool.entity.DrmCodes;

public class ArticlesForm extends BaseForm {
	
	private int type;
	
	private DrmArticles articles;
	
	private String content;
	
	private List<DrmCodes> codesList;
	
	private List<DrmArticles> articlesList;
	
	private List<DrmArticles> tzggList;
	
	private List<DrmArticles> zjtdList;
	
	private Map<DrmCodes, List<DrmArticles>> informationMap = new LinkedHashMap<DrmCodes, List<DrmArticles>>();
	
	public String getCodesName(){
		if(type > 0 && codesList != null && codesList.size() > 0){
			for(DrmCodes c : codesList){
				if(NumberUtil.parseInteger(c.getCd()) == type){
					return c.getNm();
				}
			}
		}
		return "通知公告";
	}
	
	public String getNavUrl(){
		return getDomain() +"/articles/list?type=" + type;
	}

	public List<DrmArticles> getTzggList() {
		return tzggList;
	}

	public void setTzggList(List<DrmArticles> tzggList) {
		this.tzggList = tzggList;
	}

	public List<DrmArticles> getZjtdList() {
		return zjtdList;
	}

	public void setZjtdList(List<DrmArticles> zjtdList) {
		this.zjtdList = zjtdList;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public List<DrmCodes> getCodesList() {
		return codesList;
	}

	public void setCodesList(List<DrmCodes> codesList) {
		this.codesList = codesList;
	}

	public List<DrmArticles> getArticlesList() {
		return articlesList;
	}

	public void setArticlesList(List<DrmArticles> articlesList) {
		this.articlesList = articlesList;
	}

	public DrmArticles getArticles() {
		return articles;
	}

	public void setArticles(DrmArticles articles) {
		this.articles = articles;
	}

	public Map<DrmCodes, List<DrmArticles>> getInformationMap() {
		return informationMap;
	}

	public void setInformationMap(Map<DrmCodes, List<DrmArticles>> informationMap) {
		this.informationMap = informationMap;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}
