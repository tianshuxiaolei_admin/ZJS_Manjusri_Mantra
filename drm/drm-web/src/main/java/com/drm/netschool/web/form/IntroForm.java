package com.drm.netschool.web.form;

import java.util.List;

import com.drm.netschool.entity.DrmAgentApply;
import com.drm.netschool.entity.extend.RecommendCourse;

public class IntroForm extends BaseForm {
	
	private DrmAgentApply agentApply;
	
	private List<RecommendCourse> courseList;	//一元课程列表
	
	private List<RecommendCourse> recommendCourseList; //精品面授

	public DrmAgentApply getAgentApply() {
		return agentApply;
	}

	public void setAgentApply(DrmAgentApply agentApply) {
		this.agentApply = agentApply;
	}

	public List<RecommendCourse> getCourseList() {
		return courseList;
	}

	public void setCourseList(List<RecommendCourse> courseList) {
		this.courseList = courseList;
	}

	public List<RecommendCourse> getRecommendCourseList() {
		return recommendCourseList;
	}

	public void setRecommendCourseList(List<RecommendCourse> recommendCourseList) {
		this.recommendCourseList = recommendCourseList;
	}
}
