package com.drm.netschool.web.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.drm.common.util.ConfigUtil;
import com.drm.common.util.CookieUtil;
import com.drm.common.util.DateUtil;
import com.drm.common.util.PaymentType;
import com.drm.common.util.StringUtil;
import com.drm.common.util.constants.Constants;
import com.drm.netschool.entity.DrmBuyOrder;
import com.drm.netschool.entity.DrmSalesPromotion;
import com.drm.netschool.entity.DrmUser;
import com.drm.netschool.entity.DrmUserSinfo;
import com.drm.netschool.entity.extend.CartInfo;
import com.drm.netschool.redis.RedisUtil;
import com.drm.netschool.service.DrmBuyCartService;
import com.drm.netschool.service.DrmSalesPromotionService;
import com.drm.netschool.service.DrmSystemSettingService;
import com.drm.netschool.service.DrmUserCacheService;
import com.drm.netschool.service.DrmUserSinfoService;
import com.drm.netschool.service.OrderService;
import com.drm.netschool.web.form.ShopForm;

@Controller
@RequestMapping("/shop/")
public class ShopController {

	private Logger LOGGER = LoggerFactory.getLogger(ShopController.class);

	@Autowired
	private DrmUserCacheService userCacheService;

	@Autowired
	private OrderService orderService;

	@Autowired
	private DrmBuyCartService cartService;

	@Autowired
	private DrmSalesPromotionService promotionService;

	@Autowired
	private DrmUserSinfoService userInfoService;
	
	@Autowired
	private DrmSystemSettingService systemSettingService;
	
	@Autowired
	private RedisUtil redisUtil;

	@RequestMapping(value = "/shoppay", method = RequestMethod.POST)
	public String shoppay(@ModelAttribute("form") ShopForm form, HttpServletRequest request, HttpServletResponse response) {
		LOGGER.info("shoppay:::" + DateUtil.current());
		form.setMetaInfo(systemSettingService.getDrmSystemSetting());
		
		DrmUser user = null;
		CookieUtil cookieUtil = new CookieUtil(request, response, ConfigUtil.getString("cookie.domain"));
		String key = cookieUtil.getCookie(Constants.COOKIE_USER_ID,false);
		if (!StringUtil.empty(key))
			user = userCacheService.getUserCache(key);

		if (user == null) {
			return "login/login";
		}
		form.setUser(user);
		CartInfo cart = new CartInfo();
		cart.setUserId(user.getUserId());
		cart.setCartSt(1);
		List<CartInfo> cartList = cartService.getCartList(cart, form.getCartIds());
		DrmUserSinfo us = userInfoService.getByUserId(user.getUserId());
		form.setUserSinfo(us);
		if (cartList != null && cartList.size() > 0) {
			DrmSalesPromotion promotion = promotionService.getPromotion(1);
			DrmBuyOrder order = orderService.addOrder(user.getUserId(), cartList, promotion, us, form.isUseState());
			form.setOrder(order);
		}
		//只支付学习币的情况
		if (form.getOrder() != null && form.getOrder().getOrderSt() == 4) {
			return "shop/shopsucces";
		}
		
		return "/shop/shoppay";
	}
	
	@RequestMapping(value = "/gopay")
	public String goPay(@ModelAttribute("form") ShopForm form){
		form.setMetaInfo(systemSettingService.getDrmSystemSetting());
		
		DrmBuyOrder order = orderService.getById(form.getOrder().getOrderId());
		PaymentType ptype = PaymentType.valueOf(form.getPayType());
		return "redirect:" + orderService.comfirmPay(order, ptype);
	}
	
	@RequestMapping(value = "/sure")
	public String pay(@ModelAttribute("form") ShopForm form){
		return "shop/shopsucces";
	}
}
