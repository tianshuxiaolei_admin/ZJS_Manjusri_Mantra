package com.drm.netschool.web.form;

/**
 * Created by CUIJB on 2015年7月25日
 */
public class LoginForm extends BaseForm {

	private String userName;

	private String password;

	private int remember;
	
	private String checkCode;
	
	/**
	 * 登录后重定向链接
	 */
	private String redirectURL;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getRemember() {
		return remember;
	}

	public void setRemember(int remember) {
		this.remember = remember;
	}

	public String getRedirectURL() {
		return redirectURL;
	}

	public void setRedirectURL(String redirectURL) {
		this.redirectURL = redirectURL;
	}

	public String getCheckCode() {
		return checkCode;
	}

	public void setCheckCode(String checkCode) {
		this.checkCode = checkCode;
	}
}
