package com.drm.netschool.web.controller;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.drm.common.util.NumberUtil;
import com.drm.common.util.StringUtil;
import com.drm.netschool.entity.DrmArticles;
import com.drm.netschool.entity.DrmCodes;
import com.drm.netschool.mongo.MongoDao;
import com.drm.netschool.service.DrmArticlesService;
import com.drm.netschool.service.DrmCodesService;
import com.drm.netschool.service.DrmSystemSettingService;
import com.drm.netschool.web.form.ArticlesForm;


@Controller
@RequestMapping("/articles/")
public class ArticlesController {

	private Logger LOGGER = LoggerFactory.getLogger(ArticlesController.class);
	
	@Autowired
	private DrmArticlesService articlesService;
	
	@Autowired
	private DrmCodesService codesService;
	
	@Autowired
	private DrmSystemSettingService systemSettingService;
	

	@Autowired
	@Qualifier("mongoDao")
	private MongoDao mongoDao;
	
	@RequestMapping(value = "{type}", method = RequestMethod.GET)
	@ResponseBody
	public List<DrmArticles> getArticleList(@PathVariable Integer type, HttpServletRequest request, HttpServletResponse response){
		LOGGER.info("getArticleList  type:" + type);
		int page = NumberUtil.parseInteger(request.getParameter("page"), 1);
		int pageSize = NumberUtil.parseInteger(request.getParameter("pageSize"), 5);
		DrmArticles articles = new DrmArticles();
		articles.setArticleTp(type);
		articles.setArticlePublishyn(1);	//已发布
		return articlesService.getListByPage(articles, page, pageSize);
	}
	
	@RequestMapping(value = "list")
	public String list(@ModelAttribute("form") ArticlesForm form, HttpServletRequest request, HttpServletResponse response){
		form.setType(form.getType() == 0 ? 1 : form.getType());
		form.setMetaInfo(systemSettingService.getDrmSystemSetting());
		DrmCodes codes = new DrmCodes();
		codes.setCodeGp("article_tp");
		codes.setUseYn(1);
		form.setCodesList(codesService.getListByPage(codes, 1, null));
		
		DrmArticles zArticles = new DrmArticles();
		zArticles.setArticleTp(form.getType());
		zArticles.setArticlePublishyn(1);	//已发布
		int total = articlesService.getCount(zArticles);
		form.setTotalRecords(total);
		if(total > 0)
			form.setArticlesList(articlesService.getListByPage(zArticles, form.getPage(), form.getPageSize()));
		return "articles/list";
	}
	
	@RequestMapping(value = "information")
	public String getInformation(@ModelAttribute("form") ArticlesForm form, HttpServletRequest request, HttpServletResponse response){
		form.setMetaInfo(systemSettingService.getDrmSystemSetting());
		
		DrmCodes codes = new DrmCodes();
		codes.setCodeGp("article_tp");
		codes.setUseYn(1);
		List<DrmCodes> codesList = codesService.getListByPage(codes, 1, null);
		
		if(codesList != null && codesList.size() > 0){
			Map<DrmCodes, List<DrmArticles>> informationMap = new LinkedHashMap<DrmCodes, List<DrmArticles>>();
			for(DrmCodes c : codesList){
				DrmArticles tArticles = new DrmArticles();
				tArticles.setArticleTp(NumberUtil.parseInteger(c.getCd()));
				tArticles.setArticlePublishyn(1);	//已发布
				List<DrmArticles> list = articlesService.getListByPage(tArticles, 1, 3);
				if(list != null && list.size() > 0)
					informationMap.put(c, list);
			}
			form.setInformationMap(informationMap);
		}
		
		/*DrmArticles tArticles = new DrmArticles();
		tArticles.setArticleTp(1);
		tArticles.setArticlePublishyn(1);	//已发布
		form.setTzggList(articlesService.getListByPage(tArticles, 1, 3));
		
		DrmArticles zArticles = new DrmArticles();
		zArticles.setArticleTp(8);
		zArticles.setArticlePublishyn(1);	//已发布
		form.setZjtdList(articlesService.getListByPage(zArticles, 1, 3));*/
		return "articles/information";
	}
	
	@RequestMapping(value = "detail/{type}/{id}")
	public String detail(@PathVariable Integer type, @PathVariable Long id, @ModelAttribute("form") ArticlesForm form){
		form.setMetaInfo(systemSettingService.getDrmSystemSetting());
		form.setType((type == null || type == 0) ? 1 : type);
		
		DrmCodes codes = new DrmCodes();
		codes.setCodeGp("article_tp");
		codes.setUseYn(1);
		form.setCodesList(codesService.getListByPage(codes, 1, null));
		DrmArticles entity = articlesService.getById(id);
		form.setArticles(entity);
		if(entity != null && !StringUtil.empty(entity.getArticleContent())){
			form.setContent(mongoDao.getContentById(entity.getArticleContent()));
		}
		//更新浏览次数
		if(entity != null){
			entity.setBrowerCnt(NumberUtil.parseInteger(entity.getBrowerCnt()) + 1);
			articlesService.update(entity);
		}
		return "articles/detail";
	}
}
