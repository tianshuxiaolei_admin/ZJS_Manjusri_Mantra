package com.drm.netschool.web.form;

import java.util.List;

import com.drm.common.util.StringUtil;
import com.drm.netschool.entity.DrmArea;
import com.drm.netschool.entity.DrmUser;

/**
 * Created by CUIJB on 2015年7月25日
 */
public class RegisterForm extends BaseForm {

	private DrmUser user;
	private List<DrmArea> areas;
    private String confirmPassword;
    private String checkCode;
    //邮箱校验的token
    private String token;
    //登录邮箱的地址
    public String getEmailAddress() {
    	if(user != null && !StringUtil.empty(user.getUserEmail())){
    		int index = user.getUserEmail().indexOf("@");
    		if(index != -1){
    			return "http://mail." +  user.getUserEmail().substring(index + 1);
    		}
    	}
		return "#";
	}
    
	public DrmUser getUser() {
		return user;
	}
	public void setUser(DrmUser user) {
		this.user = user;
	}
	public String getConfirmPassword() {
		return confirmPassword;
	}
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}
	public String getCheckCode() {
		return checkCode;
	}
	public void setCheckCode(String checkCode) {
		this.checkCode = checkCode;
	}
	public List<DrmArea> getAreas() {
		return areas;
	}
	public void setAreas(List<DrmArea> areas) {
		this.areas = areas;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
}
