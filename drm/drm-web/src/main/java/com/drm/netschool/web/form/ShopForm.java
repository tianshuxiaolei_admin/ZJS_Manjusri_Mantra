package com.drm.netschool.web.form;

import java.util.List;

import com.drm.netschool.entity.DrmBuyOrder;
import com.drm.netschool.entity.DrmUser;
import com.drm.netschool.entity.DrmUserSinfo;
import com.drm.netschool.entity.extend.CartInfo;

public class ShopForm extends BaseForm {

	private DrmUser user;

	private DrmUserSinfo userSinfo;

	private DrmBuyOrder order;
	
	private String payType;

	private List<CartInfo> cartList;

	private List<Long> cartIds;

	private boolean useState; // 学习币使用状态

	public boolean isUseState() {
		return useState;
	}

	public void setUseState(boolean useState) {
		this.useState = useState;
	}

	public List<Long> getCartIds() {
		return cartIds;
	}

	public void setCartIds(List<Long> cartIds) {
		this.cartIds = cartIds;
	}

	public List<CartInfo> getCartList() {
		return cartList;
	}

	public void setCartList(List<CartInfo> cartList) {
		this.cartList = cartList;
	}

	public DrmUser getUser() {
		return user;
	}

	public void setUser(DrmUser user) {
		this.user = user;
	}

	public DrmUserSinfo getUserSinfo() {
		return userSinfo;
	}

	public void setUserSinfo(DrmUserSinfo userSinfo) {
		this.userSinfo = userSinfo;
	}

	public DrmBuyOrder getOrder() {
		return order;
	}

	public void setOrder(DrmBuyOrder order) {
		this.order = order;
	}

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}
}
