package com.drm.netschool.web.form;

import java.net.URLDecoder;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;

import com.drm.common.util.ConfigUtil;
import com.drm.common.util.NumberUtil;
import com.drm.common.util.PageIndex;
import com.drm.common.util.PriceUtils;
import com.drm.common.util.StringUtil;
import com.drm.common.util.constants.Constants;
import com.drm.common.util.constants.IconConstants;
import com.drm.common.util.web.context.WebContext;
import com.drm.common.util.web.context.WebContextHolder;
import com.drm.netschool.entity.DrmSystemSetting;

/**
 * Created by CUIJB on 2015年7月26日
 */
public class BaseForm {
	
	private PageIndex pageIndex; // 开始索引和结束索引,页码
	private long totalPages = 1; // 总页数
	private int pageSize = 5; // 每页显示记录数
	private int page = 1; // 当前页
	private long totalRecords; //总记录数
	private int pageCode = 5; //每页显示的页码数量，默认10条
	
	private DrmSystemSetting metaInfo;
	
	private Map<String, String> errors = new LinkedHashMap<String, String>();

	public Map<String, String> getErrors() {
		return errors;
	}

	public void setErrors(Map<String, String> errors) {
		this.errors = errors;
	}

	public void putError(String key, String val) {
		errors.put(key, val);
	}

	public void putErrors(Map<String, String> errors) {
		errors.putAll(errors);
	}

	public boolean hasError() {
		return errors.size() > 0;
	}
	
	/**
	 * 获取记录的开始索引
	 * @return
	 */
	public int getStartIndex() {
		return (this.page - 1) * this.pageSize;
	}

	public int getPageCode() {
		return pageCode;
	}

	public void setPageCode(int pageCode) {
		this.pageCode = pageCode;
	}

	public PageIndex getPageIndex() {
		return pageIndex;
	}


	public long getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(long totalPages) {
		this.totalPages = totalPages;
		this.pageIndex = PageIndex.getPageIndex(pageCode, page, totalPages); //计算出页码
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getPageSize() {
		return pageSize;
	}

	public int getPage() {
		return page;
	}
	
	public void setPage(int page) {
		this.page = page;
	}

	public long getTotalRecords() {
		return totalRecords;
	}

	public void setTotalRecords(long totalRecords) {
		this.totalRecords = totalRecords;
		setTotalPages(this.totalRecords % this.pageSize == 0 ? this.totalRecords / this.pageSize : this.totalRecords / this.pageSize + 1); 
	}
	
	public final WebContext getWebContext() {
		WebContext webContext = WebContextHolder.get();
		return webContext == null ? new WebContext() : webContext;
	}

	public final String getRequestURI() {
		try {
			return URLDecoder.decode(getWebContext().getRequestURI(), "UTF-8");
		} catch (Exception e) {
			return getWebContext().getRequestURI();
		}
	}
	
	public String getDomain() {
        return Constants.USER_DOMAIN;
    }
	
	public String getResourceDomain() {
		return ConfigUtil.getString("drmFile.server");
    }
	
	public String getImageUrl(String key){
		if(StringUtil.empty(key))
			return "#";
		return getResourceDomain() + "/drmimg/" + key;
	}
	
	public String getDrmDownUrl(String key, String fileName,String temkey){
		if(StringUtil.empty(key) || StringUtil.empty(fileName))
			return "#";
		return getResourceDomain() + "/drmdown/" + key + "?fnm=" + fileName +"&dk="+temkey;
	}
	public String getDrmDownUrl(String key, String fileName){
		if(StringUtil.empty(key) || StringUtil.empty(fileName))
			return "#";
		return getResourceDomain() + "/drmdown/" + key + "?fnm=" + fileName ;
	}
	public String getCollegeUrl(Long userId) {
		return "http://college.zhuclass.com/home/" + userId;
	}
	
	public String getUserUrl(Long userId) {
		return "http://space.zhuclass.com/#/" + userId + "/home";
	}
	
	public String getGroupUrl(Long userId) {
		return "http://space.zhuclass.com/teamgroup/query_group_main/" + userId;
	}
	
	public String getCssDomain() {
		return ConfigUtil.getString("css.server");
    }
	
	public String getJsDomain() {
		return ConfigUtil.getString("js.server");
    }

    public String getStaticResourceDomain() {
        return Constants.BACKEND_STATIC_RESOURCE_DOMAIN;
    }
    
	public String getPrice(Object priceInt) {
		return PriceUtils.intToStr(NumberUtil.parseInteger(priceInt));
	}
	
	//过滤html标签  并截取长度
	public String subAndFilerHtml(String content, Integer max){
		String s = StringUtil.filerHtml(content);
		if(max == null || max == 0){
			return s;
		}
		if(s.length() > max){
			return s.substring(0, max) + "...";
		}
		return s;
	}
	
	public String subContent(String content, Integer max){
		if(max == null || max == 0){
			return content;
		}
		if(content.length() > max){
			return content.substring(0, max) + "...";
		}
		return content;
	}
	
	public static String selIcon(String fileType){
		if(StringUtil.empty(fileType))
			return IconConstants.other;
		
		for(String key : IconConstants.icons.keySet()){
			String[] values = IconConstants.icons.get(key).split(",");
			if(Arrays.asList(values).contains(fileType.toLowerCase())){
				return key;
			}
		}
		return IconConstants.other;
	}

	public DrmSystemSetting getMetaInfo() {
		return metaInfo;
	}

	public void setMetaInfo(DrmSystemSetting metaInfo) {
		this.metaInfo = metaInfo;
	}
}
