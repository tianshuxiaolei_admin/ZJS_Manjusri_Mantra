package com.drm.netschool.web.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.drm.common.util.DateUtil;
import com.drm.netschool.entity.SumerTag;
import com.drm.netschool.service.DrmSystemSettingService;
import com.drm.netschool.service.TagService;
import com.drm.netschool.web.form.TagForm;

/**
 * Created by CUIJB on 2015年8月13日
 */
@Controller
@RequestMapping("/tag/")
public class TagController {
	
	private Logger LOGGER = LoggerFactory.getLogger(TagController.class);
	
	@Autowired
	private TagService tagService;
	
	@Autowired
	private DrmSystemSettingService systemSettingService;
	
	@RequestMapping(value = "hot")
	@ResponseBody
	public List<SumerTag> getHotList(){
		return tagService.getHostTagList(new SumerTag(), 1, 10);
	}
	
	@RequestMapping(value = "list")
	public String list(@ModelAttribute("form") TagForm form){
		LOGGER.info("tagList:::" + DateUtil.current());
		form.setMetaInfo(systemSettingService.getDrmSystemSetting());
		form.setTagList(tagService.getListByPage(new SumerTag(), 1, null));
		return "tag/tags";
	}
}
