package com.drm.netschool.web.form;

import java.util.List;

import com.drm.netschool.entity.DrmArticles;
import com.drm.netschool.entity.DrmCourse;
import com.drm.netschool.entity.DrmCourseType;
import com.drm.netschool.entity.SumerSharedResource;
import com.drm.netschool.entity.extend.GroupInfo;
import com.drm.netschool.entity.extend.RecommendCourse;
import com.drm.netschool.entity.extend.UserInfo;

/**
 * Created by CUIJB on 2015年7月25日
 */
public class IndexForm extends BaseForm {
	
	private DrmCourseType courseType = new DrmCourseType();
	
	private DrmArticles articles = new DrmArticles();
	
	private DrmCourse drmCourse = new DrmCourse();
	
	private List<DrmCourseType> courseTypeList;
	
	private List<DrmArticles> articlesList;	//轮播图
	private List<DrmArticles> lArticlesList;	//专家团队
	private List<DrmArticles> sArticlesList;//学院动态
	private List<DrmArticles> iArticlesList;//行业动态
	private List<RecommendCourse> courseList;	//公开课
	private List<RecommendCourse> jpCourseList;	//精品课
	private List<RecommendCourse> swjnList;	//实务技能
	private List<RecommendCourse> zcfgList;	//政策法规
	private List<RecommendCourse> zyrzList;	//职业认证
	private List<RecommendCourse> tzrzList;	//投资融资
	private List<RecommendCourse> qyglList;	//企业管理
	
	private List<UserInfo> students;	//学员空间
	private List<GroupInfo> groupInfos;	//学习群组
	private List<SumerSharedResource> zykList;	//资源库
	
	public DrmCourseType getCourseType() {
		return courseType;
	}
	public void setCourseType(DrmCourseType courseType) {
		this.courseType = courseType;
	}
	public DrmArticles getArticles() {
		return articles;
	}
	public void setArticles(DrmArticles articles) {
		this.articles = articles;
	}
	public DrmCourse getDrmCourse() {
		return drmCourse;
	}
	public void setDrmCourse(DrmCourse drmCourse) {
		this.drmCourse = drmCourse;
	}
	public List<DrmCourseType> getCourseTypeList() {
		return courseTypeList;
	}
	public void setCourseTypeList(List<DrmCourseType> courseTypeList) {
		this.courseTypeList = courseTypeList;
	}
	public List<DrmArticles> getArticlesList() {
		return articlesList;
	}
	public void setArticlesList(List<DrmArticles> articlesList) {
		this.articlesList = articlesList;
	}
	public List<DrmArticles> getsArticlesList() {
		return sArticlesList;
	}
	public void setsArticlesList(List<DrmArticles> sArticlesList) {
		this.sArticlesList = sArticlesList;
	}
	public List<DrmArticles> getiArticlesList() {
		return iArticlesList;
	}
	public void setiArticlesList(List<DrmArticles> iArticlesList) {
		this.iArticlesList = iArticlesList;
	}
	public List<RecommendCourse> getCourseList() {
		return courseList;
	}
	public void setCourseList(List<RecommendCourse> courseList) {
		this.courseList = courseList;
	}
	public List<RecommendCourse> getSwjnList() {
		return swjnList;
	}
	public void setSwjnList(List<RecommendCourse> swjnList) {
		this.swjnList = swjnList;
	}
	public List<RecommendCourse> getZcfgList() {
		return zcfgList;
	}
	public void setZcfgList(List<RecommendCourse> zcfgList) {
		this.zcfgList = zcfgList;
	}
	public List<RecommendCourse> getZyrzList() {
		return zyrzList;
	}
	public void setZyrzList(List<RecommendCourse> zyrzList) {
		this.zyrzList = zyrzList;
	}
	public List<RecommendCourse> getTzrzList() {
		return tzrzList;
	}
	public void setTzrzList(List<RecommendCourse> tzrzList) {
		this.tzrzList = tzrzList;
	}
	public List<RecommendCourse> getQyglList() {
		return qyglList;
	}
	public void setQyglList(List<RecommendCourse> qyglList) {
		this.qyglList = qyglList;
	}
	public List<SumerSharedResource> getZykList() {
		return zykList;
	}
	public void setZykList(List<SumerSharedResource> zykList) {
		this.zykList = zykList;
	}
	public List<GroupInfo> getGroupInfos() {
		return groupInfos;
	}
	public void setGroupInfos(List<GroupInfo> groupInfos) {
		this.groupInfos = groupInfos;
	}
	public List<UserInfo> getStudents() {
		return students;
	}
	public void setStudents(List<UserInfo> students) {
		this.students = students;
	}
	public List<DrmArticles> getlArticlesList() {
		return lArticlesList;
	}
	public void setlArticlesList(List<DrmArticles> lArticlesList) {
		this.lArticlesList = lArticlesList;
	}
	public List<RecommendCourse> getJpCourseList() {
		return jpCourseList;
	}
	public void setJpCourseList(List<RecommendCourse> jpCourseList) {
		this.jpCourseList = jpCourseList;
	}
}
