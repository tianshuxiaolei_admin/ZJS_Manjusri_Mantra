package com.drm.netschool.web.form;

import com.drm.common.util.StringUtil;


public class FindPasswordForm extends BaseForm {
	
	private Long userId;
	
	private String userName;
	
	private String email;
	
	private String checkCode;
	
	private String newPwd;
	
	private String confirmPwd;
	
	private String token;
	
	//登录邮箱的地址
    public String getEmailAddress() {
    	if(!StringUtil.empty(email)){
    		int index = email.indexOf("@");
    		if(index != -1){
    			return "http://mail." +  email.substring(index + 1);
    		}
    	}
		return "#";
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCheckCode() {
		return checkCode;
	}

	public void setCheckCode(String checkCode) {
		this.checkCode = checkCode;
	}

	public String getNewPwd() {
		return newPwd;
	}

	public void setNewPwd(String newPwd) {
		this.newPwd = newPwd;
	}

	public String getConfirmPwd() {
		return confirmPwd;
	}

	public void setConfirmPwd(String confirmPwd) {
		this.confirmPwd = confirmPwd;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
}
