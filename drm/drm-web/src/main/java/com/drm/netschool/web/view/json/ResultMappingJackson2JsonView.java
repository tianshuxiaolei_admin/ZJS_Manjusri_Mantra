package com.drm.netschool.web.view.json;

import java.util.HashMap;
import java.util.Map;

import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import com.drm.netschool.web.form.BaseForm;

/**
 *
 * @author CUIJB
 * @date 2015年3月18日
 */
public class ResultMappingJackson2JsonView extends MappingJackson2JsonView {

    /** 
     * @see org.springframework.web.servlet.view.json.MappingJackson2JsonView#filterModel(java.util.Map)
     */
    @Override
    protected Object filterModel(Map<String, Object> model) {
        Map<String, Object> result = new HashMap<String, Object>(model.size());

        for (Map.Entry<String, Object> entry : model.entrySet()) {
            if (entry.getValue() instanceof BaseForm || entry.getValue() instanceof BindingResult) {
                continue;
            }

            result.put(entry.getKey(), entry.getValue());
        }
        return result;
    }

}
