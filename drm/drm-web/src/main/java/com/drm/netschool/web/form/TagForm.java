package com.drm.netschool.web.form;

import java.util.List;

import com.drm.netschool.entity.SumerTag;

public class TagForm extends BaseForm {
	
	private List<SumerTag> tagList;

	public List<SumerTag> getTagList() {
		return tagList;
	}

	public void setTagList(List<SumerTag> tagList) {
		this.tagList = tagList;
	}
	
}
