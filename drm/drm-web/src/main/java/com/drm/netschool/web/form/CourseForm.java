package com.drm.netschool.web.form;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.drm.netschool.entity.CourseCategoryBean;
import com.drm.netschool.entity.DrmArticles;
import com.drm.netschool.entity.DrmCourseCount;
import com.drm.netschool.entity.DrmCourseOffline;
import com.drm.netschool.entity.DrmCourseType;
import com.drm.netschool.entity.extend.BuyCourseRecord;
import com.drm.netschool.entity.extend.RecommendCourse;
import com.drm.netschool.entity.extend.UserInfo;

public class CourseForm extends BaseForm {

	private int type;// 分类
	private int sort;// 排序信息
	
	private String keyword;	//主页关键词搜索课程

	private RecommendCourse course;
	// 推荐课程
	private List<RecommendCourse> courseList;
	// 课程分类
	private List<DrmCourseType> courseTypeList;
	// 资讯最新动态
	private List<DrmArticles> articlesList;
	// 课件信息
	private List<CourseCategoryBean> courseCategoryList;
	// 课程列表
	private List<RecommendCourse> cList;
	// 购买记录
	private List<BuyCourseRecord> buyrecord;
	
	private UserInfo teacher;
	//购买数量  粉丝数量  浏览数量
	private DrmCourseCount count;
	//面授课程
	private DrmCourseOffline offlineInfo;
	//用户购买过此课程的标识   true:已购买
	private boolean buyFlag;

	private static Map<Integer, String> navTitle = new HashMap<Integer, String>();

	static {
		navTitle.put(0, "课程列表");
		navTitle.put(1, "最新");
		navTitle.put(2, "推荐");
		navTitle.put(3, "热门");
		navTitle.put(4, "专题");
		navTitle.put(5, "面授");
		navTitle.put(6, "公开课");
	}

	public Map<Integer, String> getNavTitle() {
		return navTitle;
	}

	public List<DrmCourseType> getCourseTypeList() {
		return courseTypeList;
	}

	public void setCourseTypeList(List<DrmCourseType> courseTypeList) {
		this.courseTypeList = courseTypeList;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getSort() {
		return sort;
	}

	public void setSort(int sort) {
		this.sort = sort;
	}

	public List<RecommendCourse> getCourseList() {
		return courseList;
	}

	public void setCourseList(List<RecommendCourse> courseList) {
		this.courseList = courseList;
	}

	public List<DrmArticles> getArticlesList() {
		return articlesList;
	}

	public void setArticlesList(List<DrmArticles> articlesList) {
		this.articlesList = articlesList;
	}

	public RecommendCourse getCourse() {
		return course;
	}

	public void setCourse(RecommendCourse course) {
		this.course = course;
	}

	public List<CourseCategoryBean> getCourseCategoryList() {
		return courseCategoryList;
	}

	public void setCourseCategoryList(List<CourseCategoryBean> courseCategoryList) {
		this.courseCategoryList = courseCategoryList;
	}

	public List<RecommendCourse> getcList() {
		return cList;
	}

	public void setcList(List<RecommendCourse> cList) {
		this.cList = cList;
	}

	public List<BuyCourseRecord> getBuyrecord() {
		return buyrecord;
	}

	public void setBuyrecord(List<BuyCourseRecord> buyrecord) {
		this.buyrecord = buyrecord;
	}

	public UserInfo getTeacher() {
		return teacher;
	}

	public void setTeacher(UserInfo teacher) {
		this.teacher = teacher;
	}

	public DrmCourseCount getCount() {
		return count;
	}

	public void setCount(DrmCourseCount count) {
		this.count = count;
	}

	public DrmCourseOffline getOfflineInfo() {
		return offlineInfo;
	}

	public void setOfflineInfo(DrmCourseOffline offlineInfo) {
		this.offlineInfo = offlineInfo;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public boolean isBuyFlag() {
		return buyFlag;
	}

	public void setBuyFlag(boolean buyFlag) {
		this.buyFlag = buyFlag;
	}

}
