package com.drm.common.util.constants;

/**
 * 
 * @author CUIJB
 * @date 2015年3月14日
 */
public class Constants {

	public static final String APPLICATION_NAME = "http://www.zhuclass.com";
	public static final String DOMAIN = "http://www.zhuclass.com";
	public static final String STATIC_RESOURCE_DOMAIN = "http://resource.zhuclass.com";

	public static final String USER_DOMAIN = "http://www.zhuclass.com";
	public static final String BACKEND_STATIC_RESOURCE_DOMAIN = "http://resource.zhuclass.com";

	public static final String COOKIE_CONTEXT_ID = "c_id";
	public static final String COOKIE_USER_NAME = "un";
	public static final String COOKIE_USER_CHECKCODE = "uc";
	public static final String COOKIE_USER_ID = "uid";
	public static final String COOKIE_USER_ID_PK = "uidpk";
	public static final String COOKIE_SESSION_ID = "s_id";

	public static final String ENCODING_UTF_8 = "UTF-8";

	public static final String DEFAULT_MAIL_ENCODING = ENCODING_UTF_8;
	public static final boolean DEFAULT_MAIL_HTML = true;
	public static final int MAIL_SEND_SUCC = 1;
	public static final int MAIL_SEND_FAIL = 0;

	public static final String STATUS_VALID = "1";
	public static final String STATUS_INVALID = "0";

	public static final String RESULT_KEY_STATUS = "status";
	public static final String RESULT_VAL_STATUS_200 = "200";
	public static final String RESULT_VAL_STATUS_403 = "403";
	public static final String RESULT_VAL_STATUS_500 = "500";
	public static final String RESULT_KEY_DATA = "data";

	public static final String SEGMENTATION = ";";
	public static final String SEPARATOR = ":";

	public static final String LOGIN_TYPE_EMAIL = "E";
	public static final String LOGIN_TYPE_MOBILE_PHONE = "M";

	public static final String MOBILE_PHONE_CHECK_CODE = "m_code";

	public static final String MOBILE_PHONE_CHECK_CODE_KEY_PREFIX = "mc";

	public static final int MOBILE_PHONE_CHECK_CODE_EXPIRE = 20 * 60;

	public static final String SMS_USER_NAME = "netschool";
	public static final String SMS_PASSWORD = "09258833133";
}
