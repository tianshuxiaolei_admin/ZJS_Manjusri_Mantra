package com.drm.common.util;

import org.springframework.context.ApplicationContext;

/**
 *
 * @author CUIJB
 * @date 2015年3月19日
 */
public class ApplicationContextUtil {

    public static ApplicationContext applicationContext;

    public static Object getBean(String beanName) {
        return applicationContext.getBean(beanName);
    }

    public static <T> T getBean(String name, Class<T> requiredType) {
        return applicationContext.getBean(name, requiredType);
    }

    public static <T> T getBean(Class<T> requiredType) {
        return applicationContext.getBean(requiredType);
    }

}
