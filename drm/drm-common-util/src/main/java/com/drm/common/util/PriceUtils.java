package com.drm.common.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created with IntelliJ IDEA. User: xujunchi Date: 14-3-1 Time: 下午12:59 To
 * change this template use File | Settings | File Templates.
 */
public class PriceUtils {
	private static Logger logger = LoggerFactory.getLogger(PriceUtils.class);

	public static long YuanToCoin(String priceStr) {
		logger.info("元：" + priceStr);
		boolean isPositivenumber = true;
		if (priceStr == null || priceStr.trim().equals("")) {
			return 0;
		}
		if (priceStr.trim().substring(0, 1).equals("-")) {
			priceStr = priceStr.substring(1, priceStr.length());
			isPositivenumber = false;
		}
		if (priceStr.trim().equals("0") || priceStr.trim().equals("0.0") || priceStr.trim().equals("0.00")) {
			return 0;
		}
		String[] data = priceStr.split("\\.");
		String data1 = data[0];
		String data2 = "";
		if (data.length > 1) {
			data2 = data[1];
			if (data2.length() >= 2) {
				data2 = data2.substring(0, 2);
			} else {
				data2 = data2 + "0";
			}
		} else {
			data2 = "00";
		}
		if (isPositivenumber) {
			logger.info("分：" + data1 + data2);
			return Integer.parseInt(data1 + data2);
		} else {
			logger.info("分：-" + data1 + data2);
			return Integer.parseInt("-" + data1 + data2);
		}

	}

	public static String CoinToYuan(long priceInt) {
		logger.info("分：" + priceInt);
		boolean isPositivenumber = true;
		if (priceInt <= 0) {
			priceInt = Math.abs(priceInt);
			isPositivenumber = false;
		}
		if (priceInt == 0) {
			return "0.00";
		}
		String priceStr = priceInt + "";
		String data1 = "";
		String data2 = "";
		int length = priceStr.length();
		if (length >= 3) {
			data1 = priceStr.substring(0, length - 2);
			data2 = priceStr.substring(length - 2, length);
		}
		if (length == 2) {
			data1 = "0";
			data2 = priceStr;
		}
		if (length == 1) {
			data1 = "0";
			data2 = "0" + priceStr;
		}
		priceStr = data1 + "." + data2;
		if (!isPositivenumber) {
			priceStr = "-" + priceStr;
		}

		logger.info("元：" + priceStr);
		return priceStr;
	}

	public static int YuanToCoinInt(String priceStr) {
		return Long.valueOf(YuanToCoin(priceStr)).intValue();
	}

	public static int strToInt(String priceStr) {
		if (priceStr == null || priceStr.isEmpty())
			return 0;

		if (priceStr.trim().equals("0")) {
			return Integer.parseInt(priceStr);
		}
		String[] data = priceStr.split("\\.");
		String data1 = data[0];
		String data2 = "";
		if (data.length > 1) {
			data2 = data[1];
			if (data2.length() >= 2) {
				data2 = data2.substring(0, 2);
			} else {
				data2 = data2 + "0";
			}
		} else {
			data2 = "00";
		}
		logger.info("字符串 到数字转换 。。。。。。。。。。。。" + priceStr + "to" + data1 + data2);
		return Integer.parseInt(data1 + data2);
	}

	public static String intToStr(int priceInt) {
		if (priceInt == 0) {
			return priceInt + "";
		}
		String priceStr = priceInt + "";
		String data1 = "";
		String data2 = "";
		int length = priceStr.length();
		if (length >= 3) {
			data1 = priceStr.substring(0, length - 2);
			data2 = priceStr.substring(length - 2, length);
			if (data2.equals("00")) {
				data2 = "";
			}
			if (data2.matches("\\d0")) {
				data2 = data2.substring(0, 1);
			}
		}
		if (length == 2) {
			data1 = "0";
			data2 = priceStr;
		}
		if (length == 1) {
			data1 = "0";
			data2 = "0" + priceStr;
		}
		if (data2.equals("")) {
			priceStr = data1;
		} else {
			priceStr = data1 + "." + data2;
		}
		return priceStr;
	}
}
