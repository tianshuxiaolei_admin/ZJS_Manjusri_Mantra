package com.drm.common.util;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

/**
 * context.properties
 *
 */
public abstract class ConfigUtil {
    private static Configuration config            = null;
    private static final String  CONTEXT_FILE_NAME = "context.properties";

    private static final String  RUN_MODE          = "runMode";
    private static final String  RUN_MODE_DEV      = "dev";
    private static final String  RUN_MODE_PROD     = "prod";
    private static final String  RUN_MODE_TEST     = "test";

    private static final String  DB_TYPE           = "dbType";
    private static final String  DB_TYPE_ORACLE    = "oracle";
    private static final String  DB_TYPE_MYSQL     = "mysql";

    private static final String  ATTACH_FILE_DIR   = "attachFileDir";

    static {
        try {
            config = new PropertiesConfiguration("/config/" + CONTEXT_FILE_NAME);
        } catch (ConfigurationException e) {
            e.printStackTrace();
        }
    }

    public static final Configuration getConfig() {
        return config;
    }

    public static final String getString(String key) {
        return getConfig().getString(key);
    }

    //================下面的方法跟配置内容相关=====================//
    public static final String getRunMode() {
        return getString(RUN_MODE);
    }

    public static final String getDbType() {
        return getString(DB_TYPE);
    }

    public static final boolean isDev() {
        return RUN_MODE_DEV.equals(getRunMode());
    }

    public static final boolean isProd() {
        return RUN_MODE_PROD.equals(getRunMode());
    }

    public static final boolean isTest() {
        return RUN_MODE_TEST.equals(getRunMode());
    }

    public static final boolean isOracle() {
        return DB_TYPE_ORACLE.equals(getDbType());
    }

    public static final boolean isMysql() {
        return DB_TYPE_MYSQL.equals(getDbType());
    }

    public static final String getAttachFileDir() {
        return getString(ATTACH_FILE_DIR);
    }

    public static void main(String[] args) {
        System.out.println(getRunMode());
    }
}
