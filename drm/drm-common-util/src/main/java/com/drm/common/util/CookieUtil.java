package com.drm.common.util;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author CUIJB
 * @date 2015年3月14日
 */
public class CookieUtil {
    private HttpServletRequest  request;

    private HttpServletResponse response;

    private String              domain;

    private static final String PATH = "/";

    public CookieUtil(final HttpServletRequest request, final HttpServletResponse response) {
        this.request = request;
        this.response = response;
    }

    public CookieUtil(final HttpServletRequest request, final HttpServletResponse response, final String domain) {
        this.request = request;
        this.response = response;
        this.domain = domain;
    }

    /**
     * 默认不base64转码
     * @param name
     * @return
     */
    public String getCookie(String name) {
        return getCookie(name, true);
    }

    /*
     * decode  true   base64 编码，  false: 原始内容
     */
    public String getCookie(String name, boolean decode) {
        Cookie[] cookies = request.getCookies();
        if (cookies == null || cookies.length == 0) {
            return null;
        }

        for (Cookie cookie : cookies) {
            if (cookie.getName().equals(name)) {
                String value = cookie.getValue();
                return decode ? decode(value) : value;
            }
        }
        return null;
    }

    public void setCookie(String name, String value) {
        setCookie(name, value, PATH);
    }

    public void setCookie(String name, String value, String path) {
        setCookie(name, value, path, true);
    }

    public void setCookie(String name, String value, int expiry) {
        setCookie(name, value, PATH, expiry, true);
    }

    public void setCookie(String name, String value, String path, int expiry) {
        setCookie(name, value, path, expiry, true);
    }

    public void setCookie(String name, String value, String path, boolean encode) {
        setCookie(name, value, path, -1, encode);
    }

    public void setCookie(String name, String value, String path, int expiry, boolean encode) {
        Cookie cookie = new Cookie(name, encode ? encode(value) : value);
        cookie.setPath(path);
        cookie.setMaxAge(expiry);
        if (StringUtils.isNotBlank(domain)) {
            cookie.setDomain(domain);
        }
        response.addCookie(cookie);
    }

    public void removeCokie(String name) {
        setCookie(name, null, 0);
    }

    private String encode(String value) {
        return value == null ? null : new String(Base64.encodeBase64(value.getBytes()));
    }

    private String decode(String value) {
        return value == null ? null : new String(Base64.decodeBase64(value.getBytes()));
    }
    
    
    /**
     * 
     *  Function:
     *  功能说明： 验证    用户的cookie是否正确
     *	 使用说明：
     *  @author  wangxiaolei  DateTime 2014年12月17日 下午12:16:02
     *	返回类型: boolean    
     *  @param uid
     *  @param uidKey
     *  @return
     */
    public static boolean checkCookieIdIsOk(String uid,String uidKey){
    	 if(uidKey!=null &&  uidKey.equals(  MD5Util.digest(MD5Util.digest(uid))   )){
    		 return true;
    	 }else{
    		 return false;
    	 }
    	
    }
    
    
    public static String getUidpk(String uid){
    	return  MD5Util.digest(MD5Util.digest(uid)) ;
    }
   
}
