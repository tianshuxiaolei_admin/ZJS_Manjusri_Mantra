package com.drm.common.util;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;

import org.apache.log4j.Logger;

public class CopyUtil {

	private static final Logger logger = Logger.getLogger(CopyUtil.class);

	public static void copy(Object source, Object dest) {
		try {
			// 获取属性
			BeanInfo sourceBean = Introspector.getBeanInfo(source.getClass(), java.lang.Object.class);
			PropertyDescriptor[] sourceProperty = sourceBean.getPropertyDescriptors();

			BeanInfo destBean = Introspector.getBeanInfo(dest.getClass(), java.lang.Object.class);
			PropertyDescriptor[] destProperty = destBean.getPropertyDescriptors();

			for (int i = 0; i < sourceProperty.length; i++) {
				for (int j = 0; j < destProperty.length; j++) {
					if (sourceProperty[i].getName().equals(destProperty[j].getName())) {
						// 调用source的getter方法和dest的setter方法
						Object obj = sourceProperty[i].getReadMethod().invoke(source);
						if(obj != null){
							destProperty[j].getWriteMethod().invoke(dest, obj);
							break;
						}else{
							System.out.println(":::::;;;;;");
						}
					}
				}
			}
		} catch (Exception e) {
			logger.error("属性cope异常!", e);
		}
	}

}
