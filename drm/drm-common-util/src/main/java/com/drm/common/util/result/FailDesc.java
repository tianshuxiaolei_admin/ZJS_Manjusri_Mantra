package com.drm.common.util.result;

public class FailDesc {
	
	public String name;
	
	public String failCode;
	
	public String desc;

	public FailDesc(String name, String failCode, String desc) {
		this.name = name;
		this.failCode = failCode;
		this.desc = desc;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFailCode() {
		return failCode;
	}

	public void setFailCode(String failCode) {
		this.failCode = failCode;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
}
