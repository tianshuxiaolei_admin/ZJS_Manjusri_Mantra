package com.drm.common.util;

import static java.lang.String.format;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author xiaolei1.wang
 * @date 2011-11-22 21:28
 */
public abstract class EncryptTool {

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		// System.out.println( Sha1.sha1Base64("123456") );
		// System.out.println( sha1HEX("123456") );
		System.out.println(sha1HEX("123456"));
		System.out.println(md5Hex("123456"));
	}

	public static String md5Hex(String text) throws NoSuchAlgorithmException {
		MessageDigest md5 = MessageDigest.getInstance("MD5");
		md5.update(text.getBytes());
		byte[] k = md5.digest();
		return convertToHex(k);
	}

	public static String sha1HEX(String text) throws Exception {
		try {
			MessageDigest md = MessageDigest.getInstance("SHA");

			md.update(text.getBytes(), 0, text.length());

			return convertToHex((md.digest()));
		} catch (Exception e) {
			throw new Exception(format("Problem hashing %s", text), e);
		}
	}

	static char[] carr = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
			'a', 'b', 'c', 'd', 'e', 'f' };

	@SuppressWarnings("unused")
	private static int intFromChar(char c) {
		char clower = Character.toLowerCase(c);
		for (int i = 0; i < carr.length; i++) {
			if (clower == carr[i]) {
				return i;
			}
		}
		return 0;
	}

	private static String convertToHex(byte[] data) {
		StringBuilder buf = new StringBuilder();
		for (int i = 0; i < data.length; i++) {
			int halfbyte = (data[i] >>> 4) & 0x0F;
			int two_halfs = 0;
			do {
				if ((0 <= halfbyte) && (halfbyte <= 9))
					buf.append((char) ('0' + halfbyte));
				else
					buf.append((char) ('a' + (halfbyte - 10)));
				halfbyte = data[i] & 0x0F;
			} while (two_halfs++ < 1);
		}
		return buf.toString();
	}

	public static String Str2Base64(String str, String charset) {
		if (str == null)
			return null;
		try {
			byte[] temp = str.getBytes(charset);
			return convertToHex(temp);
		} catch (UnsupportedEncodingException e) {
			return null;
		}
	}

	public static String Str2Base64(String str) {
		if (str == null) {
			return null;
		}
		byte[] temp = str.getBytes();
		return convertToHex(temp);
	}
}
