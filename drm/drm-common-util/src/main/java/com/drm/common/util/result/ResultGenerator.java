package com.drm.common.util.result;

public class ResultGenerator {

	/**
	 * 错误代码定义
	 */

	public static final FailDesc SYSTEM_ERROR = new FailDesc("SYSTEM_ERROR", "100000", "system error");

	public static final FailDesc PARAM_ERROR = new FailDesc("PARAM_ERROR", "100001", "param is error");

	public static final FailDesc USER_NAME_ALREADY_EXISTS = new FailDesc("USER_ERROR", "200000", "The user already exists");

	public static final FailDesc CONFIRM_PASSWORD_NOT_SAME = new FailDesc("USER_ERROR", "200001", "The password and confirmation is not consistent");

	public static final FailDesc USERNAME_OR_PASSWORD_ERROR = new FailDesc("USER_ERROR", "200002", "用户名或密码错误");
	
	public static final FailDesc MOBILE_PHONE_CHECK_CODE_ERROR = new FailDesc("USER_ERROR", "200003", "Mobile phone checkcode error");
	
	public static final FailDesc CREATE_MOBILE_PHONE_CHECK_CODE_ERROR = new FailDesc("USER_ERROR", "200004", "Create mobile phone checkcode error");
	
	public static final FailDesc LOGIN_CAPTCHA_ERROR = new FailDesc("USER_ERROR", "200004", "login captcha is error");
	
	
	public static final FailDesc FLIGHT_ORDER_NOT_FOND_ERROR = new FailDesc("FLIGHT_ORDER_ERROR", "300000", "flight order not fund error");
	
	public static final FailDesc FLIGHT_NOT_FOND_ERROR = new FailDesc("Flight_ERROR", "400000", "flight not fund error");
	public static final FailDesc FLIGHT_UPDATE_ERROR = new FailDesc("Flight_ERROR", "400001", "flight update error");
	
	public static final FailDesc FLIGHT_RATES_NOT_FOND_ERROR = new FailDesc("Flight_RATES_ERROR", "500000", "flight rates not fund error");
	public static final FailDesc FLIGHT_RATES_UPDATE_ERROR = new FailDesc("Flight_RATES_ERROR", "500001", "flight rates update error");
	
	public static final FailDesc UPLOAD_EXCEL_ERROR = new FailDesc("UPLOAD_EXCEL_ERROR", "600000", "upload excel error");
	public static final FailDesc EXCEL_DATA_ERROR = new FailDesc("UPLOAD_EXCEL_ERROR", "600001", "excel data error");
	public static final FailDesc EXCEL_FORMAT_ERROR = new FailDesc("UPLOAD_EXCEL_ERROR", "600002", "excel format error");
	
	

}
