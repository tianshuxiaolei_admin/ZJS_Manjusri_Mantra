package com.drm.common.util;

/**
 * *************************************************************************
 * 
 * @文件名称: AliPayPramVo.java
 * 
 * @包路径 : com.iman.manjusri.vo
 * 
 * @版权所有: TSW 科技有限公司 (C) 2014
 * 
 * @类描述:
 * 
 * @创建人: wangxiaolei
 * 
 * @创建时间: 2015年3月30日 - 下午10:46:20
 * 
 * @修改记录: 
 *        ------------------------------------------------------------------------
 *        ----------------------- 时间 | 修改人 | 修改的方法 | 修改描述
 *        ------------------------
 *        ------------------------------------------------
 *        ----------------------- | | |
 *        ------------------------------------------
 *        -----------------------------------------------------
 * 
 ************************************************************************** 
 */
public class AliPayPram {

	public String service;
	public String partner;
	public String _input_charset;
	// 支付类型
	public String payment_type;
	// 必填，不能修改
	// 服务器异步通知页面路径
	public String notify_url;
	// 需http://格式的完整路径，不能加?id=123这类自定义参数
	// 页面跳转同步通知页面路径//需http://格式的完整路径，不能加?id=123这类自定义参数，不能写成http://localhost/
	public String return_url;
	// 卖家支付宝帐户 必填
	public String seller_email;
	// 必填
	// 商户订单号//商户网站订单系统中唯一订单号，必填
	public String out_trade_no;

	// 订单名称
	public String subject;

	// 付款金额 元
	public String total_fee;

	// 订单描述
	public String body;

	// 商品展示地址
	public String show_url;

	// 防钓鱼时间戳
	// 若要使用请调用类文件submit中的query_timestamp函数
	public String anti_phishing_key;

	// 客户端的IP地址
	// 非局域网的外网IP地址，如：221.0.0.1
	public String exter_invoke_ip;
	// 支付平台key
	public String paymentKey = "";

}
