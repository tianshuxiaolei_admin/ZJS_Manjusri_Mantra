package com.drm.common.util.constants;

public enum UserGender {

	MALE(1, "男"),

	FEMALE(2, "女");

	private Integer code;

	private String name;

	private UserGender(Integer code, String name) {
		this.code = code;
		this.name = name;
	}

	public Integer getCode() {
		return code;
	}

	public static String getName(Integer code) {
		for (UserGender userGender : values()) {
			if (userGender.code.equals(code)) {
				return userGender.name;
			}
		}
		return "";
	}
	
	public static void main(String[] args) {
		System.out.println(getName(UserGender.FEMALE.getCode()));
	}
}