package com.drm.common.util.constants;

/**
 * 
 * @author CUIJB
 * @date 2015年3月14日
 */
public class UserConstants {

	public static final String USER_LEVEL_V1 = "v1";
	public static final String USER_LEVEL_V2 = "v2";
	public static final String USER_LEVEL_V3 = "v3";
	public static final String USER_LEVEL_V4 = "v4";

	public static final String USER_SOURCE_MOBILE_PHONE = "M";
	public static final String USER_SOURCE_PC = "P";

	public static final String USER_STATUS_NORMAL = "N";
	public static final String USER_STATUS_CLOSE = "C";
	public static final String USER_STATUS_FROZEN = "F";
	public static final String USER_STATUS_DELETE = "D";

	public static final String USER_COUNTRY_CHINA = "中国";

}
