package com.drm.common.util;

import org.apache.commons.codec.digest.DigestUtils;


 
 /***************************************************************************
 * 
 * @文件名称:   SaltCheckUtil.java
 * @包   路   径：  @Package com.zhe800.seller.utils 
 				 
 * @版权所有：   团博百众（北京）科技有限公司 (C) 2014
 *
 * @类描述:   用于验证传入的用户提款密码签名的验证
 * @版本:    V2.0
 * @创建人：        wangxiaolei  
 * @创建时间：   2014年3月19日 - 下午7:47:02 
 *
 *
 * @
 * @修改记录：
   -----------------------------------------------------------------------------------------------
             时间                      |       修改人            |         修改的方法                       |         修改描述                                                                
   -----------------------------------------------------------------------------------------------
                 |                 |                           |                                       
   ----------------------------------------------------------------------------------------------- 	
 
 ***************************************************************************/

public class Saltcheckutil {

    /**
     *  @param param 设置 bare_field_name
     *  描述:
     *	内部特殊实现:
     */
    public Saltcheckutil() {
        // TODO Auto-generated constructor stub
    }

    
    
    /**
     * 
     *  Function:
     *  功能说明： 根据用户传入的参数 计算签名 ，
     *  使用说明： 提供给提款申请时 用户输入的参数验证
     *  @author  Thinkpad  DateTime 2014年3月19日 下午7:51:56
     *  @param userInput
     *  @return
     */
    public static  String  saltArithmeticOld(String userInput,String salt){
        if(userInput==null ||salt==null ){
            return "";
        }
        String pw =  BCrypt.hashpw(userInput, salt);
        
        return  pw/*DigestUtils.sha512Hex(userInput+salt)*/;
    }
    
  
    /**
     * 
     *  Function:
     *  功能说明： 根据用户传入的参数 计算签名 ，
     *  使用说明： 提供给提款申请时 用户输入的参数验证
     *  @author  Thinkpad  DateTime 2014年3月19日 下午7:51:56
     *  @param userInput
     *  @return
     */
    @SuppressWarnings("deprecation")
	public static  String  saltArithmetic(String userInput,String salt){
        // TODO  需要进进提供算法 
        String temp = DigestUtils.shaHex(userInput+salt);
        //总共计算20次
        for(int i = 0 ; i < 19 ;i++){
            temp=   DigestUtils.shaHex( temp);
        }
        return  temp/*DigestUtils.sha512Hex(userInput+salt)*/;
    }
}
