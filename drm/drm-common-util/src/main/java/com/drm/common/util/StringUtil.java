package com.drm.common.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author CUIJB
 * @date 2015年3月14日
 */
@SuppressWarnings({ "unchecked", "rawtypes" })
public class StringUtil {
	
	static Pattern p=Pattern.compile("^\\s*$",Pattern.CASE_INSENSITIVE);
	
	public static boolean empty(String str){
		if(null == str) return true;
        //Pattern.CASE_INSENSITIVE 忽略大小  
        Matcher m=p.matcher(str);
        return m.matches();
	}
	
	public static String getLastWord(String str, String regex){
		String[] strs = str.split(regex);
		if(strs.length <= 0) return "";
		return strs[strs.length - 1];
	}
	
	public static String join(String str1, String str2, String split){
		StringBuilder buf = new StringBuilder();
		if(str1 == null || str2 == null || split == null)
			return "";
		buf.append(str1).append(split).append(str2);
		return buf.toString();
	}
	
	public static String join(String str1, String str2, String str3, String split){
		StringBuilder buf = new StringBuilder();
		if(str1 == null || str2 == null || str3 == null || split == null)
			return "";
		buf.append(str1).append(split).append(str2).append(split).append(str3);
		return buf.toString();
	}
	
	public static String join(String str1, String str2, String str3, String str4, String split){
		StringBuilder buf = new StringBuilder();
		if(str1 == null || str2 == null || str3 == null || str4 == null || split == null)
			return "";
		buf.append(str1).append(split).append(str2).append(split).append(str3).append(split).append(str4);
		return buf.toString();
	}
	
	public static String removeEndLetter(String str){
		if(null == str || str.length() <= 0) return "";
		return str.substring(0, str.length() - 1);
	}
	
	public static String join(List list, String split){
		StringBuilder buf = new StringBuilder();
		for(Object str : list){
			if(null != str && !("").equals(str)){
				buf.append(str).append(split);
			}
		}
		return StringUtil.removeEndLetter(buf.toString());
	}
	
	public static String join(Object[] ary, String split){
		if(null == ary) return "";
		StringBuilder buf = new StringBuilder();
		for(int i = 0; i < ary.length; i++){
			if(null != ary[i] && !("").equals(ary[i])){
				buf.append(ary[i]).append(split);
			}
			
		}
		
		return StringUtil.removeEndLetter(buf.toString());
	}
	
	/**
	 * 首字母转换为大写
	 * @param str
	 * @return
	 */
	public static String firstLetterUpcase(String str){
		return str.substring(0, 1).toUpperCase() + str.substring(1);
	}
	
	/**
	 * 首字母转换为小写
	 * @param str
	 * @return
	 */
	public static String firstLetterLowerCase(String str){
		return str.substring(0, 1).toLowerCase() + str.substring(1);
	}
	
	
	public static String listToStr(List list){
		StringBuilder buffer = new StringBuilder("[");
		for(int i = 0; i < list.size(); i++){
			Object sub = list.get(i);
			if(sub instanceof Map)
				buffer.append(mapToStr((Map)sub));
			
			if(i < list.size() - 1)
				buffer.append(",");
		}
		
		buffer.append("]");
		return buffer.toString();
	}
	
	/**
	 * map to string
	 * @param map
	 * @return
	 */
	public static String mapToStr(Map<String, Object> map){
		StringBuilder buffer = new StringBuilder("{");
		int i = 0;
		for(String key : map.keySet()){
			buffer.append("\"").append(key).append("\":").append(wrapStr(map.get(key)));
			if(i++ < map.keySet().size() - 1)
				buffer.append(",");
		}
		buffer.append("}");
		//System.out.println("json:::" + buffer.toString());
		return buffer.toString();
	}
	
	
	
	/**
	 * 对基础数据类型进行字符串包装
	 * @param obj
	 * @return
	 */
	public static String wrapStr(Object obj){
		if(obj instanceof Integer)
			return obj + "";
		else if(obj instanceof Double)
			return obj + "";
		else if(obj instanceof String)
			return "\"" + obj + "\"";
		else if(obj instanceof Date)
			return "\"" + DateUtil.dateToStr((Date)obj, DateUtil.FORMAT_DEFAULT) + "\"";
		else
			return obj + "";
	}
	
	public static String filerHtml(String content){
		if(!StringUtil.empty(content)){
			content = content.replaceAll("<[.[^<]]*>", " ");
			content = content.replaceAll("&\\w{4};", " ");
			content = content.replaceAll("\\\\w", " ");
		}
		return content;
	}
	
	public static String htmlEscape(String content){
		if(!StringUtil.empty(content)){
			content = content.replaceAll("<", "&lt;");
			content = content.replaceAll(">", "&gt;");
			content = content.replaceAll("\"", "&quot;");
		}
		return content;
	}
	
	
	public static String convertStreamToString(InputStream is) {
		BufferedReader reader;
		StringBuilder sb = new StringBuilder();
		try {
			reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
			String line = null;
			try {
				while ((line = reader.readLine()) != null) {
					sb.append(line + "\n");
				}
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					is.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
		
		return sb.toString();
	}
	
	/**
     * 全角转半角
     * @param input String.
     * @return 半角字符串
     */
	public static String ToDBC(String input) {
		char c[] = input.toCharArray();
		for (int i = 0; i < c.length; i++) {
			if (c[i] == '\u3000') {
				c[i] = ' ';
			} else if (c[i] > '\uFF00' && c[i] < '\uFF5F') {
				c[i] = (char) (c[i] - 65248);

			}
		}
		String returnString = new String(c);
		return returnString;
	}
	
	/**
	 * 半角转全角
	 * 
	 * @param input
	 *            String.
	 * @return 全角字符串.
	 */
	public static String ToSBC(String input) {
		char c[] = input.toCharArray();
		for (int i = 0; i < c.length; i++) {
			if (c[i] == ' ') {
				c[i] = '\u3000';
			} else if (c[i] < '\177') {
				c[i] = (char) (c[i] + 65248);

			}
		}
		return new String(c);
	}
	
	public static boolean checkPhone(String phone) {
		Pattern pattern = Pattern
				.compile("^13\\d{9}||15[0-9]\\d{8}||14[7]\\d{8}||18[0-9]\\d{8}$");
		Matcher matcher = pattern.matcher(phone);
		if (matcher.matches()) {
			return true;
		}
		return false;
	}
	
	public static boolean isNumber(String str) {
		Pattern pattern = Pattern.compile("[0-9]*");
		Matcher match = pattern.matcher(str);
		if (match.matches()) {
			return true;
		} 
		return false;
	}
	
	public static String subAndFilerHtml(String content, Integer max){
		String s = StringUtil.filerHtml(content);
		if(max == null || max == 0){
			return s;
		}
		return s.substring(0, max);
	}
	
	public static void main(String[] args) {
		String s = "<div class=\"infor_detailtxt\"> <p>北京中建政研信息咨询中心（以下简称中建政研）依托住房和城乡建设部政策研究中心于2004年经相关部门批准正式成立。目前以形成集行业会议、职业教育、赴企内训、咨询服务、高端研修、国际交流、产品推广、组织论坛等业务为一体的专业服务机构，中心致力于为行业搭建立体性、全方位综合服务平台。</p> <p>“追求卓越、开拓创新”是中建政研人执着的信念。“顶级专家、一流服务”是中建政研不变的承诺，而今，中建政研正迈着务实、稳健、快速的步伐，立足眼前、放眼未来，以专业、领先、专注、创新的精神风貌打造中国管理咨询与培训业的领导品牌，助力企业成长，推动行业发展！</p> <p><b>企业愿景：</b></p> <p>成为中国本土一流的咨询培训中心，专业的会展组织！</p> <p><b>企业使命：</b></p> <p>致力于为行业搭建立体型，全方位综合服务平台！</p> <p><b>企业精神：</b></p> <p>追求卓越，开拓创新！</p> <p><b>企业宗旨：</b></p> <p>稳定和高素质的员工，是企业 发展的基础！</p> <p>优质和持续的业务是企业发展的动力！</p> <p>明确和先进的制度是企业发展的保障！</p> </div>"; 
		System.out.println(subAndFilerHtml(s, 10));
	}
	
}
