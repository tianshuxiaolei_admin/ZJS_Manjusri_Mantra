package com.drm.common.util.constants;

import java.util.HashMap;
import java.util.Map;

public class IconConstants {

	public static final String word = "word,doc,docx,txt";
	public static final String excel = "cvs,xls,excel";
	public static final String sound = "cda,wav,wma,ra,midi,ogg,ape,flac,aac,mp3";
	public static final String video = "mp4,avi,dat,mkv,flv,vob,mov,rm,rmvb,wmv,asf,asx";
	public static final String ppt = "ppt,pptx";
	public static final String pdf = "pdf";
	public static final String bt = "torrent";
	public static final String zip = "rar,zip,arj,gz,z";
	public static final String cal = "cal,cals";
	public static final String image = "bmp,gif,jpg,pic,png,tif";
	public static final String android_app = "apk";
	public static final String ios_app = "ipa";
	
	public static final String other = "icon_43.png";
	
	public static final Map<String, String> icons = new HashMap<String, String>();
	
	static {
		icons.put("icon_5.png", zip);
		icons.put("icon_21.png", cal);
		icons.put("icon_27.png", image);
		icons.put("icon_31.png", pdf);
		icons.put("icon_33.png", word);
		icons.put("icon_35.png", excel);
		icons.put("icon_39.png", sound);
		icons.put("icon_41.png", video);
//		icons.put("icon_43.png", "");
		icons.put("icon_45.png", ios_app);
		icons.put("icon_51.png", android_app);
		icons.put("icon_55.png", bt);
		icons.put("icon_87.png", ppt);
	}
}
