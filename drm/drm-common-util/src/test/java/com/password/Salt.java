package com.password;

import java.security.NoSuchAlgorithmException;

import org.junit.Test;

import com.drm.common.util.Saltcheckutil;

public class Salt {

	@Test
	public void test() throws NoSuchAlgorithmException {
		 
		String salt = "$2ebe66dcf1d3aa273585778363f28c2ac";
		String pass = "dcb06d108bc44e3cc90bb0316959ddaa31dfee10";
		 
		String s = Saltcheckutil2.saltArithmetic("111111", salt);
		
		System.out.println(s.equals(pass));
		
		
		//String salt_ = Encrypt.getMD5String("zhongjianzhengyan" + "11111" );
			
		
		
		System.out.println("-----------student------1-------  ");
		String salt_="$2"+Encrypt.getMD5String("tsw123" + "111111");
		String transPass=Saltcheckutil.saltArithmetic("111111",salt_);
		System.out.println("student:  "+salt_);
		System.out.println("studenttransPass:  "+transPass);
		System.out.println("-----------teacher------1-------  ");
		 salt_="$2"+Encrypt.getMD5String("tsw1234" + "111111");
		 transPass=Saltcheckutil.saltArithmetic("111111",salt_);
		System.out.println("teacher:  "+salt_);
		System.out.println("teachertransPass:  "+transPass);
		System.out.println("--------------admin-----1-----  ");
		 salt_="$2"+Encrypt.getMD5String("tsw12345" + "111111");
		 transPass=Saltcheckutil.saltArithmetic("111111",salt_);
		System.out.println("admin:  "+salt_);
		System.out.println("transPass:  "+transPass);
		
		
		System.out.println("-----------student------2-------  ");
		 salt_="$2"+Encrypt.getMD5String("tsw123" + "111111");
		 transPass=Saltcheckutil2.saltArithmetic("111111",salt_);
		System.out.println("student:  "+salt_);
		System.out.println("studenttransPass:  "+transPass);
		System.out.println("-----------teacher------2-------  ");
		 salt_="$2"+Encrypt.getMD5String("tsw1234" + "111111");
		 transPass=Saltcheckutil2.saltArithmetic("111111",salt_);
		System.out.println("teacher:  "+salt_);
		System.out.println("teachertransPass:  "+transPass);
		System.out.println("--------------admin-----2-----  ");
		 salt_="$2"+Encrypt.getMD5String("tsw12345" + "111111");
		 transPass=Saltcheckutil2.saltArithmetic("111111",salt_);
		System.out.println("admin:  "+salt_);
		System.out.println("transPass:  "+transPass);
		
		
		
	}

}
