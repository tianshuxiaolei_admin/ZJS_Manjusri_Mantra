package com.drm.netschool.mapper.manual;

import java.util.List;
import java.util.Map;

import com.drm.netschool.entity.DrmSalesPromotion;

public interface DrmSalesPromotionExtendMapper {
	
	public List<DrmSalesPromotion> getListByPage(Map<String, Object> param);
	
	public int getCount(Map<String, Object> param);
}
