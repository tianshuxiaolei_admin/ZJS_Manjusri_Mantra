package com.drm.netschool.entity;

import java.io.Serializable;
import java.util.Date;
/**
 * @author chenxuezheng
 * 课程基本信息
 * */
public class CourseInfoBean implements Serializable {
	private long courseId;
	private int courseTp;//课程类型的主键
	private String courseNm;//课程名称
	private String courseClass;//是否是公开课   1 公开课  2 非公开课
	private int orgPrice;//课程的原始价格 单位分
	private int curPrice;//课程的当前价格 单位分
	private int courseTimes;//课时表示一共有多少个课件  组成
	private int onlyPack;//是不是只在专题中使用，暂时不用
	private int courseLimitTp;//课程限制类型，1 免费  2，限制观看次数   3，显示学习时间单位天
	private int courseLimit;//对应上面类型的数值，可能是时间单位是天， 限制观看次数，数量，course_limit_tp =2 表示的是 次数   =3  表示的是天数
	private int courseCredit;//该课程的学分
	private String courseSmart;//课程的简介,mongodb  ID
	private String courseDesc;//课程详情，放在mongodb的 id
	private String courseLables;//课程标签
	private Date courseCdt;//创建时间
	private Date coursePdt;//发布时间
	private int courseOnlinetp;//课程的线上线下类型 1：线上课程 2：线下课程
	private long createUid;//创建课程人ID
	private String courseNo;//课程编号
	private long courseTeacherid;//该课程的老师ID
	private String courseTeachernm;//课程老师的名字
	private int courseCancut;//0 不参与课程促销，1参与课程促销
	private int courseFlowyn;//课程是否是流程化学习 1流程化，0 非流程化
	private String coursePimg;//课程封面图片
	private int courseSubject;//1: 课程  2：专题
	public long getCourseId() {
		return courseId;
	}
	public void setCourseId(long courseId) {
		this.courseId = courseId;
	}
	public int getCourseTp() {
		return courseTp;
	}
	public void setCourseTp(int courseTp) {
		this.courseTp = courseTp;
	}
	public String getCourseNm() {
		return courseNm;
	}
	public void setCourseNm(String courseNm) {
		this.courseNm = courseNm;
	}
	public String getCourseClass() {
		return courseClass;
	}
	public void setCourseClass(String courseClass) {
		this.courseClass = courseClass;
	}
	public int getOrgPrice() {
		return orgPrice;
	}
	public void setOrgPrice(int orgPrice) {
		this.orgPrice = orgPrice;
	}
	public int getCurPrice() {
		return curPrice;
	}
	public void setCurPrice(int curPrice) {
		this.curPrice = curPrice;
	}
	public int getCourseTimes() {
		return courseTimes;
	}
	public void setCourseTimes(int courseTimes) {
		this.courseTimes = courseTimes;
	}
	public int getOnlyPack() {
		return onlyPack;
	}
	public void setOnlyPack(int onlyPack) {
		this.onlyPack = onlyPack;
	}
	public int getCourseLimitTp() {
		return courseLimitTp;
	}
	public void setCourseLimitTp(int courseLimitTp) {
		this.courseLimitTp = courseLimitTp;
	}
	public int getCourseLimit() {
		return courseLimit;
	}
	public void setCourseLimit(int courseLimit) {
		this.courseLimit = courseLimit;
	}
	public int getCourseCredit() {
		return courseCredit;
	}
	public void setCourseCredit(int courseCredit) {
		this.courseCredit = courseCredit;
	}
	public String getCourseSmart() {
		return courseSmart;
	}
	public void setCourseSmart(String courseSmart) {
		this.courseSmart = courseSmart;
	}
	public String getCourseDesc() {
		return courseDesc;
	}
	public void setCourseDesc(String courseDesc) {
		this.courseDesc = courseDesc;
	}
	public String getCourseLables() {
		return courseLables;
	}
	public void setCourseLables(String courseLables) {
		this.courseLables = courseLables;
	}
	public Date getCourseCdt() {
		return courseCdt;
	}
	public void setCourseCdt(Date courseCdt) {
		this.courseCdt = courseCdt;
	}
	public Date getCoursePdt() {
		return coursePdt;
	}
	public void setCoursePdt(Date coursePdt) {
		this.coursePdt = coursePdt;
	}
	public int getCourseOnlinetp() {
		return courseOnlinetp;
	}
	public void setCourseOnlinetp(int courseOnlinetp) {
		this.courseOnlinetp = courseOnlinetp;
	}
	public long getCreateUid() {
		return createUid;
	}
	public void setCreateUid(long createUid) {
		this.createUid = createUid;
	}
	public String getCourseNo() {
		return courseNo;
	}
	public void setCourseNo(String courseNo) {
		this.courseNo = courseNo;
	}
	public long getCourseTeacherid() {
		return courseTeacherid;
	}
	public void setCourseTeacherid(long courseTeacherid) {
		this.courseTeacherid = courseTeacherid;
	}
	public String getCourseTeachernm() {
		return courseTeachernm;
	}
	public void setCourseTeachernm(String courseTeachernm) {
		this.courseTeachernm = courseTeachernm;
	}
	public int getCourseCancut() {
		return courseCancut;
	}
	public void setCourseCancut(int courseCancut) {
		this.courseCancut = courseCancut;
	}
	public int getCourseFlowyn() {
		return courseFlowyn;
	}
	public void setCourseFlowyn(int courseFlowyn) {
		this.courseFlowyn = courseFlowyn;
	}
	public String getCoursePimg() {
		return coursePimg;
	}
	public void setCoursePimg(String coursePimg) {
		this.coursePimg = coursePimg;
	}
	public int getCourseSubject() {
		return courseSubject;
	}
	public void setCourseSubject(int courseSubject) {
		this.courseSubject = courseSubject;
	}
	
	
}
