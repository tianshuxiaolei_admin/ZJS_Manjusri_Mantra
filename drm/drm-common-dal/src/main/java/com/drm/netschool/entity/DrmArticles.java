package com.drm.netschool.entity;

import java.util.Date;

public class DrmArticles {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column drm_articles.id
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    private Long id;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column drm_articles.article_title
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    private String articleTitle;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column drm_articles.notice_subtitle
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    private String noticeSubtitle;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column drm_articles.article_tp
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    private Integer articleTp;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column drm_articles.article_keys
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    private String articleKeys;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column drm_articles.article_hotyn
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    private Integer articleHotyn;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column drm_articles.article_desc
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    private String articleDesc;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column drm_articles.article_image
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    private String articleImage;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column drm_articles.article_content
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    private String articleContent;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column drm_articles.article_show_student
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    private Integer articleShowStudent;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column drm_articles.article_attch_yn
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    private String articleAttchYn;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column drm_articles.article_cdt
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    private Date articleCdt;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column drm_articles.article_udt
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    private Date articleUdt;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column drm_articles.brower_cnt
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    private Integer browerCnt;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column drm_articles.article_headyn
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    private Integer articleHeadyn;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column drm_articles.article_cid
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    private Long articleCid;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column drm_articles.article_uid
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    private Long articleUid;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column drm_articles.show_imgicon
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    private Integer showImgicon;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column drm_articles.article_delyn
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    private Integer articleDelyn;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column drm_articles.article_publishyn
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    private Integer articlePublishyn;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column drm_articles.article_publishdt
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    private Date articlePublishdt;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column drm_articles.id
     *
     * @return the value of drm_articles.id
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    public Long getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column drm_articles.id
     *
     * @param id the value for drm_articles.id
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column drm_articles.article_title
     *
     * @return the value of drm_articles.article_title
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    public String getArticleTitle() {
        return articleTitle;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column drm_articles.article_title
     *
     * @param articleTitle the value for drm_articles.article_title
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    public void setArticleTitle(String articleTitle) {
        this.articleTitle = articleTitle == null ? null : articleTitle.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column drm_articles.notice_subtitle
     *
     * @return the value of drm_articles.notice_subtitle
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    public String getNoticeSubtitle() {
        return noticeSubtitle;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column drm_articles.notice_subtitle
     *
     * @param noticeSubtitle the value for drm_articles.notice_subtitle
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    public void setNoticeSubtitle(String noticeSubtitle) {
        this.noticeSubtitle = noticeSubtitle == null ? null : noticeSubtitle.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column drm_articles.article_tp
     *
     * @return the value of drm_articles.article_tp
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    public Integer getArticleTp() {
        return articleTp;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column drm_articles.article_tp
     *
     * @param articleTp the value for drm_articles.article_tp
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    public void setArticleTp(Integer articleTp) {
        this.articleTp = articleTp;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column drm_articles.article_keys
     *
     * @return the value of drm_articles.article_keys
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    public String getArticleKeys() {
        return articleKeys;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column drm_articles.article_keys
     *
     * @param articleKeys the value for drm_articles.article_keys
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    public void setArticleKeys(String articleKeys) {
        this.articleKeys = articleKeys == null ? null : articleKeys.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column drm_articles.article_hotyn
     *
     * @return the value of drm_articles.article_hotyn
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    public Integer getArticleHotyn() {
        return articleHotyn;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column drm_articles.article_hotyn
     *
     * @param articleHotyn the value for drm_articles.article_hotyn
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    public void setArticleHotyn(Integer articleHotyn) {
        this.articleHotyn = articleHotyn;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column drm_articles.article_desc
     *
     * @return the value of drm_articles.article_desc
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    public String getArticleDesc() {
        return articleDesc;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column drm_articles.article_desc
     *
     * @param articleDesc the value for drm_articles.article_desc
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    public void setArticleDesc(String articleDesc) {
        this.articleDesc = articleDesc == null ? null : articleDesc.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column drm_articles.article_image
     *
     * @return the value of drm_articles.article_image
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    public String getArticleImage() {
        return articleImage;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column drm_articles.article_image
     *
     * @param articleImage the value for drm_articles.article_image
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    public void setArticleImage(String articleImage) {
        this.articleImage = articleImage == null ? null : articleImage.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column drm_articles.article_content
     *
     * @return the value of drm_articles.article_content
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    public String getArticleContent() {
        return articleContent;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column drm_articles.article_content
     *
     * @param articleContent the value for drm_articles.article_content
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    public void setArticleContent(String articleContent) {
        this.articleContent = articleContent == null ? null : articleContent.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column drm_articles.article_show_student
     *
     * @return the value of drm_articles.article_show_student
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    public Integer getArticleShowStudent() {
        return articleShowStudent;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column drm_articles.article_show_student
     *
     * @param articleShowStudent the value for drm_articles.article_show_student
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    public void setArticleShowStudent(Integer articleShowStudent) {
        this.articleShowStudent = articleShowStudent;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column drm_articles.article_attch_yn
     *
     * @return the value of drm_articles.article_attch_yn
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    public String getArticleAttchYn() {
        return articleAttchYn;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column drm_articles.article_attch_yn
     *
     * @param articleAttchYn the value for drm_articles.article_attch_yn
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    public void setArticleAttchYn(String articleAttchYn) {
        this.articleAttchYn = articleAttchYn == null ? null : articleAttchYn.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column drm_articles.article_cdt
     *
     * @return the value of drm_articles.article_cdt
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    public Date getArticleCdt() {
        return articleCdt;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column drm_articles.article_cdt
     *
     * @param articleCdt the value for drm_articles.article_cdt
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    public void setArticleCdt(Date articleCdt) {
        this.articleCdt = articleCdt;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column drm_articles.article_udt
     *
     * @return the value of drm_articles.article_udt
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    public Date getArticleUdt() {
        return articleUdt;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column drm_articles.article_udt
     *
     * @param articleUdt the value for drm_articles.article_udt
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    public void setArticleUdt(Date articleUdt) {
        this.articleUdt = articleUdt;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column drm_articles.brower_cnt
     *
     * @return the value of drm_articles.brower_cnt
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    public Integer getBrowerCnt() {
        return browerCnt;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column drm_articles.brower_cnt
     *
     * @param browerCnt the value for drm_articles.brower_cnt
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    public void setBrowerCnt(Integer browerCnt) {
        this.browerCnt = browerCnt;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column drm_articles.article_headyn
     *
     * @return the value of drm_articles.article_headyn
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    public Integer getArticleHeadyn() {
        return articleHeadyn;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column drm_articles.article_headyn
     *
     * @param articleHeadyn the value for drm_articles.article_headyn
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    public void setArticleHeadyn(Integer articleHeadyn) {
        this.articleHeadyn = articleHeadyn;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column drm_articles.article_cid
     *
     * @return the value of drm_articles.article_cid
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    public Long getArticleCid() {
        return articleCid;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column drm_articles.article_cid
     *
     * @param articleCid the value for drm_articles.article_cid
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    public void setArticleCid(Long articleCid) {
        this.articleCid = articleCid;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column drm_articles.article_uid
     *
     * @return the value of drm_articles.article_uid
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    public Long getArticleUid() {
        return articleUid;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column drm_articles.article_uid
     *
     * @param articleUid the value for drm_articles.article_uid
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    public void setArticleUid(Long articleUid) {
        this.articleUid = articleUid;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column drm_articles.show_imgicon
     *
     * @return the value of drm_articles.show_imgicon
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    public Integer getShowImgicon() {
        return showImgicon;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column drm_articles.show_imgicon
     *
     * @param showImgicon the value for drm_articles.show_imgicon
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    public void setShowImgicon(Integer showImgicon) {
        this.showImgicon = showImgicon;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column drm_articles.article_delyn
     *
     * @return the value of drm_articles.article_delyn
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    public Integer getArticleDelyn() {
        return articleDelyn;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column drm_articles.article_delyn
     *
     * @param articleDelyn the value for drm_articles.article_delyn
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    public void setArticleDelyn(Integer articleDelyn) {
        this.articleDelyn = articleDelyn;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column drm_articles.article_publishyn
     *
     * @return the value of drm_articles.article_publishyn
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    public Integer getArticlePublishyn() {
        return articlePublishyn;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column drm_articles.article_publishyn
     *
     * @param articlePublishyn the value for drm_articles.article_publishyn
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    public void setArticlePublishyn(Integer articlePublishyn) {
        this.articlePublishyn = articlePublishyn;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column drm_articles.article_publishdt
     *
     * @return the value of drm_articles.article_publishdt
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    public Date getArticlePublishdt() {
        return articlePublishdt;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column drm_articles.article_publishdt
     *
     * @param articlePublishdt the value for drm_articles.article_publishdt
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    public void setArticlePublishdt(Date articlePublishdt) {
        this.articlePublishdt = articlePublishdt;
    }
}