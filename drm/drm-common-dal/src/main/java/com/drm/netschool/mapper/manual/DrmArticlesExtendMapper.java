package com.drm.netschool.mapper.manual;

import java.util.List;
import java.util.Map;

import com.drm.netschool.entity.DrmArticles;

public interface DrmArticlesExtendMapper {
	
	public List<DrmArticles> getListByPage(Map<String, Object> param);
	
	public int getCount(Map<String, Object> param);
}