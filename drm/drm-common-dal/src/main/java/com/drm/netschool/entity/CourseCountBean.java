package com.drm.netschool.entity;

import java.io.Serializable;
/**
 * @author chenxuezheng
 * 课程数量信息
 * */
public class CourseCountBean implements Serializable {
	private long courseId;
	private int courseBuycnt;// 但却课程被购买数量
	private int courseCommentcnt;// 当前课程评论数量
	private int courseBrowsercnt;// 当前课程浏览次数
	public long getCourseId() {
		return courseId;
	}
	public void setCourseId(long courseId) {
		this.courseId = courseId;
	}
	public int getCourseBuycnt() {
		return courseBuycnt;
	}
	public void setCourseBuycnt(int courseBuycnt) {
		this.courseBuycnt = courseBuycnt;
	}
	public int getCourseCommentcnt() {
		return courseCommentcnt;
	}
	public void setCourseCommentcnt(int courseCommentcnt) {
		this.courseCommentcnt = courseCommentcnt;
	}
	public int getCourseBrowsercnt() {
		return courseBrowsercnt;
	}
	public void setCourseBrowsercnt(int courseBrowsercnt) {
		this.courseBrowsercnt = courseBrowsercnt;
	}
	
	
}
