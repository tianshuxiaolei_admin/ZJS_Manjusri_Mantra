package com.drm.netschool.entity;

import java.io.Serializable;
import java.util.Date;

public class CourseCtgHomework implements Serializable {
	private long id;
	private long categoryId;
	private String homeworkName;
	private int homeworkScore;
	private String homeworkTxt;
	private String homeworkAnswer;
	private Date homeworkDate;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(long categoryId) {
		this.categoryId = categoryId;
	}

	public String getHomeworkName() {
		return homeworkName;
	}

	public void setHomeworkName(String homeworkName) {
		this.homeworkName = homeworkName;
	}

	public int getHomeworkScore() {
		return homeworkScore;
	}

	public void setHomeworkScore(int homeworkScore) {
		this.homeworkScore = homeworkScore;
	}

	public String getHomeworkTxt() {
		return homeworkTxt;
	}

	public void setHomeworkTxt(String homeworkTxt) {
		this.homeworkTxt = homeworkTxt;
	}

	public String getHomeworkAnswer() {
		return homeworkAnswer;
	}

	public void setHomeworkAnswer(String homeworkAnswer) {
		this.homeworkAnswer = homeworkAnswer;
	}

	public Date getHomeworkDate() {
		return homeworkDate;
	}

	public void setHomeworkDate(Date homeworkDate) {
		this.homeworkDate = homeworkDate;
	}
}
