package com.drm.netschool.mapper;

import java.util.List;
import java.util.Map;

import com.drm.netschool.entity.CourseTypeBean;

public interface CourseTypeMapper {
    List <CourseTypeBean>  courseTypeList();
	
	List<CourseTypeBean> courseTypeListBySearchMap(Map<String,Object> map);
}
