package com.drm.netschool.mapper;

import java.util.List;
import java.util.Map;

import com.drm.netschool.entity.CourseCommentBean;

/**
 * @author chenxuezheng
 * 课程评论Mapper
 * */
public interface CourseCommentMapper {
	List<CourseCommentBean> getCourseCommentList(Map<String,Object> searchInfo);
	  
	int getCourseCommentListCount(Map<String,Object> searchInfo);
}
