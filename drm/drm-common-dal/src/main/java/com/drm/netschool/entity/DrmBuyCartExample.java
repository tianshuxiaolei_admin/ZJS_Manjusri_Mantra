package com.drm.netschool.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DrmBuyCartExample {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table drm_buy_cart
     *
     * @mbggenerated Thu Jul 23 23:06:59 CST 2015
     */
    protected String orderByClause;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table drm_buy_cart
     *
     * @mbggenerated Thu Jul 23 23:06:59 CST 2015
     */
    protected boolean distinct;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table drm_buy_cart
     *
     * @mbggenerated Thu Jul 23 23:06:59 CST 2015
     */
    protected List<Criteria> oredCriteria;

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table drm_buy_cart
     *
     * @mbggenerated Thu Jul 23 23:06:59 CST 2015
     */
    public DrmBuyCartExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table drm_buy_cart
     *
     * @mbggenerated Thu Jul 23 23:06:59 CST 2015
     */
    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table drm_buy_cart
     *
     * @mbggenerated Thu Jul 23 23:06:59 CST 2015
     */
    public String getOrderByClause() {
        return orderByClause;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table drm_buy_cart
     *
     * @mbggenerated Thu Jul 23 23:06:59 CST 2015
     */
    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table drm_buy_cart
     *
     * @mbggenerated Thu Jul 23 23:06:59 CST 2015
     */
    public boolean isDistinct() {
        return distinct;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table drm_buy_cart
     *
     * @mbggenerated Thu Jul 23 23:06:59 CST 2015
     */
    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table drm_buy_cart
     *
     * @mbggenerated Thu Jul 23 23:06:59 CST 2015
     */
    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table drm_buy_cart
     *
     * @mbggenerated Thu Jul 23 23:06:59 CST 2015
     */
    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table drm_buy_cart
     *
     * @mbggenerated Thu Jul 23 23:06:59 CST 2015
     */
    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table drm_buy_cart
     *
     * @mbggenerated Thu Jul 23 23:06:59 CST 2015
     */
    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table drm_buy_cart
     *
     * @mbggenerated Thu Jul 23 23:06:59 CST 2015
     */
    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table drm_buy_cart
     *
     * @mbggenerated Thu Jul 23 23:06:59 CST 2015
     */
    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andCartIdIsNull() {
            addCriterion("cart_id is null");
            return (Criteria) this;
        }

        public Criteria andCartIdIsNotNull() {
            addCriterion("cart_id is not null");
            return (Criteria) this;
        }

        public Criteria andCartIdEqualTo(Long value) {
            addCriterion("cart_id =", value, "cartId");
            return (Criteria) this;
        }

        public Criteria andCartIdNotEqualTo(Long value) {
            addCriterion("cart_id <>", value, "cartId");
            return (Criteria) this;
        }

        public Criteria andCartIdGreaterThan(Long value) {
            addCriterion("cart_id >", value, "cartId");
            return (Criteria) this;
        }

        public Criteria andCartIdGreaterThanOrEqualTo(Long value) {
            addCriterion("cart_id >=", value, "cartId");
            return (Criteria) this;
        }

        public Criteria andCartIdLessThan(Long value) {
            addCriterion("cart_id <", value, "cartId");
            return (Criteria) this;
        }

        public Criteria andCartIdLessThanOrEqualTo(Long value) {
            addCriterion("cart_id <=", value, "cartId");
            return (Criteria) this;
        }

        public Criteria andCartIdIn(List<Long> values) {
            addCriterion("cart_id in", values, "cartId");
            return (Criteria) this;
        }

        public Criteria andCartIdNotIn(List<Long> values) {
            addCriterion("cart_id not in", values, "cartId");
            return (Criteria) this;
        }

        public Criteria andCartIdBetween(Long value1, Long value2) {
            addCriterion("cart_id between", value1, value2, "cartId");
            return (Criteria) this;
        }

        public Criteria andCartIdNotBetween(Long value1, Long value2) {
            addCriterion("cart_id not between", value1, value2, "cartId");
            return (Criteria) this;
        }

        public Criteria andCourseIdIsNull() {
            addCriterion("course_id is null");
            return (Criteria) this;
        }

        public Criteria andCourseIdIsNotNull() {
            addCriterion("course_id is not null");
            return (Criteria) this;
        }

        public Criteria andCourseIdEqualTo(Long value) {
            addCriterion("course_id =", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdNotEqualTo(Long value) {
            addCriterion("course_id <>", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdGreaterThan(Long value) {
            addCriterion("course_id >", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdGreaterThanOrEqualTo(Long value) {
            addCriterion("course_id >=", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdLessThan(Long value) {
            addCriterion("course_id <", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdLessThanOrEqualTo(Long value) {
            addCriterion("course_id <=", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdIn(List<Long> values) {
            addCriterion("course_id in", values, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdNotIn(List<Long> values) {
            addCriterion("course_id not in", values, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdBetween(Long value1, Long value2) {
            addCriterion("course_id between", value1, value2, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdNotBetween(Long value1, Long value2) {
            addCriterion("course_id not between", value1, value2, "courseId");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNull() {
            addCriterion("user_id is null");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNotNull() {
            addCriterion("user_id is not null");
            return (Criteria) this;
        }

        public Criteria andUserIdEqualTo(Long value) {
            addCriterion("user_id =", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotEqualTo(Long value) {
            addCriterion("user_id <>", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThan(Long value) {
            addCriterion("user_id >", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThanOrEqualTo(Long value) {
            addCriterion("user_id >=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThan(Long value) {
            addCriterion("user_id <", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThanOrEqualTo(Long value) {
            addCriterion("user_id <=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdIn(List<Long> values) {
            addCriterion("user_id in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotIn(List<Long> values) {
            addCriterion("user_id not in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdBetween(Long value1, Long value2) {
            addCriterion("user_id between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotBetween(Long value1, Long value2) {
            addCriterion("user_id not between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andCartStIsNull() {
            addCriterion("cart_st is null");
            return (Criteria) this;
        }

        public Criteria andCartStIsNotNull() {
            addCriterion("cart_st is not null");
            return (Criteria) this;
        }

        public Criteria andCartStEqualTo(Integer value) {
            addCriterion("cart_st =", value, "cartSt");
            return (Criteria) this;
        }

        public Criteria andCartStNotEqualTo(Integer value) {
            addCriterion("cart_st <>", value, "cartSt");
            return (Criteria) this;
        }

        public Criteria andCartStGreaterThan(Integer value) {
            addCriterion("cart_st >", value, "cartSt");
            return (Criteria) this;
        }

        public Criteria andCartStGreaterThanOrEqualTo(Integer value) {
            addCriterion("cart_st >=", value, "cartSt");
            return (Criteria) this;
        }

        public Criteria andCartStLessThan(Integer value) {
            addCriterion("cart_st <", value, "cartSt");
            return (Criteria) this;
        }

        public Criteria andCartStLessThanOrEqualTo(Integer value) {
            addCriterion("cart_st <=", value, "cartSt");
            return (Criteria) this;
        }

        public Criteria andCartStIn(List<Integer> values) {
            addCriterion("cart_st in", values, "cartSt");
            return (Criteria) this;
        }

        public Criteria andCartStNotIn(List<Integer> values) {
            addCriterion("cart_st not in", values, "cartSt");
            return (Criteria) this;
        }

        public Criteria andCartStBetween(Integer value1, Integer value2) {
            addCriterion("cart_st between", value1, value2, "cartSt");
            return (Criteria) this;
        }

        public Criteria andCartStNotBetween(Integer value1, Integer value2) {
            addCriterion("cart_st not between", value1, value2, "cartSt");
            return (Criteria) this;
        }

        public Criteria andPutTimeIsNull() {
            addCriterion("put_time is null");
            return (Criteria) this;
        }

        public Criteria andPutTimeIsNotNull() {
            addCriterion("put_time is not null");
            return (Criteria) this;
        }

        public Criteria andPutTimeEqualTo(Date value) {
            addCriterion("put_time =", value, "putTime");
            return (Criteria) this;
        }

        public Criteria andPutTimeNotEqualTo(Date value) {
            addCriterion("put_time <>", value, "putTime");
            return (Criteria) this;
        }

        public Criteria andPutTimeGreaterThan(Date value) {
            addCriterion("put_time >", value, "putTime");
            return (Criteria) this;
        }

        public Criteria andPutTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("put_time >=", value, "putTime");
            return (Criteria) this;
        }

        public Criteria andPutTimeLessThan(Date value) {
            addCriterion("put_time <", value, "putTime");
            return (Criteria) this;
        }

        public Criteria andPutTimeLessThanOrEqualTo(Date value) {
            addCriterion("put_time <=", value, "putTime");
            return (Criteria) this;
        }

        public Criteria andPutTimeIn(List<Date> values) {
            addCriterion("put_time in", values, "putTime");
            return (Criteria) this;
        }

        public Criteria andPutTimeNotIn(List<Date> values) {
            addCriterion("put_time not in", values, "putTime");
            return (Criteria) this;
        }

        public Criteria andPutTimeBetween(Date value1, Date value2) {
            addCriterion("put_time between", value1, value2, "putTime");
            return (Criteria) this;
        }

        public Criteria andPutTimeNotBetween(Date value1, Date value2) {
            addCriterion("put_time not between", value1, value2, "putTime");
            return (Criteria) this;
        }

        public Criteria andCancelDtIsNull() {
            addCriterion("cancel_dt is null");
            return (Criteria) this;
        }

        public Criteria andCancelDtIsNotNull() {
            addCriterion("cancel_dt is not null");
            return (Criteria) this;
        }

        public Criteria andCancelDtEqualTo(Date value) {
            addCriterion("cancel_dt =", value, "cancelDt");
            return (Criteria) this;
        }

        public Criteria andCancelDtNotEqualTo(Date value) {
            addCriterion("cancel_dt <>", value, "cancelDt");
            return (Criteria) this;
        }

        public Criteria andCancelDtGreaterThan(Date value) {
            addCriterion("cancel_dt >", value, "cancelDt");
            return (Criteria) this;
        }

        public Criteria andCancelDtGreaterThanOrEqualTo(Date value) {
            addCriterion("cancel_dt >=", value, "cancelDt");
            return (Criteria) this;
        }

        public Criteria andCancelDtLessThan(Date value) {
            addCriterion("cancel_dt <", value, "cancelDt");
            return (Criteria) this;
        }

        public Criteria andCancelDtLessThanOrEqualTo(Date value) {
            addCriterion("cancel_dt <=", value, "cancelDt");
            return (Criteria) this;
        }

        public Criteria andCancelDtIn(List<Date> values) {
            addCriterion("cancel_dt in", values, "cancelDt");
            return (Criteria) this;
        }

        public Criteria andCancelDtNotIn(List<Date> values) {
            addCriterion("cancel_dt not in", values, "cancelDt");
            return (Criteria) this;
        }

        public Criteria andCancelDtBetween(Date value1, Date value2) {
            addCriterion("cancel_dt between", value1, value2, "cancelDt");
            return (Criteria) this;
        }

        public Criteria andCancelDtNotBetween(Date value1, Date value2) {
            addCriterion("cancel_dt not between", value1, value2, "cancelDt");
            return (Criteria) this;
        }

        public Criteria andPayDtIsNull() {
            addCriterion("pay_dt is null");
            return (Criteria) this;
        }

        public Criteria andPayDtIsNotNull() {
            addCriterion("pay_dt is not null");
            return (Criteria) this;
        }

        public Criteria andPayDtEqualTo(Date value) {
            addCriterion("pay_dt =", value, "payDt");
            return (Criteria) this;
        }

        public Criteria andPayDtNotEqualTo(Date value) {
            addCriterion("pay_dt <>", value, "payDt");
            return (Criteria) this;
        }

        public Criteria andPayDtGreaterThan(Date value) {
            addCriterion("pay_dt >", value, "payDt");
            return (Criteria) this;
        }

        public Criteria andPayDtGreaterThanOrEqualTo(Date value) {
            addCriterion("pay_dt >=", value, "payDt");
            return (Criteria) this;
        }

        public Criteria andPayDtLessThan(Date value) {
            addCriterion("pay_dt <", value, "payDt");
            return (Criteria) this;
        }

        public Criteria andPayDtLessThanOrEqualTo(Date value) {
            addCriterion("pay_dt <=", value, "payDt");
            return (Criteria) this;
        }

        public Criteria andPayDtIn(List<Date> values) {
            addCriterion("pay_dt in", values, "payDt");
            return (Criteria) this;
        }

        public Criteria andPayDtNotIn(List<Date> values) {
            addCriterion("pay_dt not in", values, "payDt");
            return (Criteria) this;
        }

        public Criteria andPayDtBetween(Date value1, Date value2) {
            addCriterion("pay_dt between", value1, value2, "payDt");
            return (Criteria) this;
        }

        public Criteria andPayDtNotBetween(Date value1, Date value2) {
            addCriterion("pay_dt not between", value1, value2, "payDt");
            return (Criteria) this;
        }

        public Criteria andCartTpIsNull() {
            addCriterion("cart_tp is null");
            return (Criteria) this;
        }

        public Criteria andCartTpIsNotNull() {
            addCriterion("cart_tp is not null");
            return (Criteria) this;
        }

        public Criteria andCartTpEqualTo(Integer value) {
            addCriterion("cart_tp =", value, "cartTp");
            return (Criteria) this;
        }

        public Criteria andCartTpNotEqualTo(Integer value) {
            addCriterion("cart_tp <>", value, "cartTp");
            return (Criteria) this;
        }

        public Criteria andCartTpGreaterThan(Integer value) {
            addCriterion("cart_tp >", value, "cartTp");
            return (Criteria) this;
        }

        public Criteria andCartTpGreaterThanOrEqualTo(Integer value) {
            addCriterion("cart_tp >=", value, "cartTp");
            return (Criteria) this;
        }

        public Criteria andCartTpLessThan(Integer value) {
            addCriterion("cart_tp <", value, "cartTp");
            return (Criteria) this;
        }

        public Criteria andCartTpLessThanOrEqualTo(Integer value) {
            addCriterion("cart_tp <=", value, "cartTp");
            return (Criteria) this;
        }

        public Criteria andCartTpIn(List<Integer> values) {
            addCriterion("cart_tp in", values, "cartTp");
            return (Criteria) this;
        }

        public Criteria andCartTpNotIn(List<Integer> values) {
            addCriterion("cart_tp not in", values, "cartTp");
            return (Criteria) this;
        }

        public Criteria andCartTpBetween(Integer value1, Integer value2) {
            addCriterion("cart_tp between", value1, value2, "cartTp");
            return (Criteria) this;
        }

        public Criteria andCartTpNotBetween(Integer value1, Integer value2) {
            addCriterion("cart_tp not between", value1, value2, "cartTp");
            return (Criteria) this;
        }
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table drm_buy_cart
     *
     * @mbggenerated do_not_delete_during_merge Thu Jul 23 23:06:59 CST 2015
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table drm_buy_cart
     *
     * @mbggenerated Thu Jul 23 23:06:59 CST 2015
     */
    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}