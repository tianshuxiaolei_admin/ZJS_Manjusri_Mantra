package com.drm.netschool.entity.extend;

import java.util.Date;

/**
 * 课程详情的购买记录
 * @author Lenovo
 */
public class BuyCourseRecord extends UserInfo{

	
	private Date commitDt;//提交订单时间

	public Date getCommitDt() {
		return commitDt;
	}

	public void setCommitDt(Date commitDt) {
		this.commitDt = commitDt;
	}
	
	 
}
