package com.drm.netschool.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * @author chenxuezheng
 * 课程评论Bean
 * */
public class CourseCommentBean implements Serializable {
	private long id;
	private long courseId;// 课程ID
	private Date commontCdt;// 增加课程评论的时间
	private String commontTxt;// 课程评论的内容
	private Long commontUid;// 课程评论的用户ID，没有的话‘’表示匿名
	private Long commontPid;// 评论的评论 的父亲id
	private int commontSeq;// 评论的顺序
	private int commontDel;// 是否删除 0 不删除 1 删除
	private Date commontDeldt;// 删除的时间
	private int commentStar;// 星星等级
	private String userRnm;//实际名称
	private String userLnm;//登陆用户名
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getCourseId() {
		return courseId;
	}
	public void setCourseId(long courseId) {
		this.courseId = courseId;
	}
	public Date getCommontCdt() {
		return commontCdt;
	}
	public void setCommontCdt(Date commontCdt) {
		this.commontCdt = commontCdt;
	}
	public String getCommontTxt() {
		return commontTxt;
	}
	public void setCommontTxt(String commontTxt) {
		this.commontTxt = commontTxt;
	}
	public Long getCommontUid() {
		return commontUid;
	}
	public void setCommontUid(Long commontUid) {
		this.commontUid = commontUid;
	}
	public Long getCommontPid() {
		return commontPid;
	}
	public void setCommontPid(Long commontPid) {
		this.commontPid = commontPid;
	}
	public int getCommontSeq() {
		return commontSeq;
	}
	public void setCommontSeq(int commontSeq) {
		this.commontSeq = commontSeq;
	}
	public int getCommontDel() {
		return commontDel;
	}
	public void setCommontDel(int commontDel) {
		this.commontDel = commontDel;
	}
	public Date getCommontDeldt() {
		return commontDeldt;
	}
	public void setCommontDeldt(Date commontDeldt) {
		this.commontDeldt = commontDeldt;
	}
	public int getCommentStar() {
		return commentStar;
	}
	public void setCommentStar(int commentStar) {
		this.commentStar = commentStar;
	}
	public String getUserRnm() {
		return userRnm;
	}
	public void setUserRnm(String userRnm) {
		this.userRnm = userRnm;
	}
	public String getUserLnm() {
		return userLnm;
	}
	public void setUserLnm(String userLnm) {
		this.userLnm = userLnm;
	}
	
	
}
