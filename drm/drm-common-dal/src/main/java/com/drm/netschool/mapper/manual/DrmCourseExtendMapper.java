package com.drm.netschool.mapper.manual;

import java.util.List;
import java.util.Map;

import com.drm.netschool.entity.DrmCourse;
import com.drm.netschool.entity.extend.RecommendCourse;

public interface DrmCourseExtendMapper {
    
	public List<DrmCourse> getListByPage(Map<String, Object> param);
	
	public int getCount(Map<String, Object> param);
	
	public List<DrmCourse> getListByIds(Map<String, Object> param);
	
	public List<RecommendCourse> getRecommendCourses(Map<String, Object> param);
	
	public int getRecommendCoursesCount(Map<String, Object> param);
	 
	public List<RecommendCourse> getRecommendCourse(Map<String, Object> param);
}