package com.drm.netschool.entity.extend;

import java.util.Date;

import com.drm.common.util.NumberUtil;
import com.drm.common.util.PriceUtils;

public class RecommendCourse {

	private Long courseId;// 课程id
	private String courseName;// 课程名称
	private String coursePic;// 课程图片
	private Integer courseStudyNum;// 课程学习人数
	private String courseClass;//是否是公开课   1 公开课  2 非公开课
	private Integer courseOnlinetp;//课程的线上线下类型 1：线上课程 2：线下课程
	private Long orgPrice;
	private Long curPrice;
	private Integer courseGoodteacher;
	private Long courseTp;
	private Integer commentStar;	//评分
	private Integer courseSubject;	//1: 课程  2：专题
	private String courseDesc;
	private String content; //课程内容
	private Integer courseSize;	//课程数
	private Integer courseTime;	//学时
	private Long courseTeacherid;
	private String courseTeachernm;
	private Date coursePdt;
	private String keyword;	//模糊查询课程使用
	private Integer courseBoutique;	//精品推荐
	
	private Integer courseTimes;	//课件数
	
	private String orderBy;
	
	
	public String getStrCurPrice(){
		return PriceUtils.intToStr(NumberUtil.parseLong(curPrice).intValue());
	}
	
	public String getStrCourseId(){
		return courseId == null ? "0" : courseId.toString();
	}
	
	public Long getCourseId() {
		return courseId;
	}
	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}
	public String getCourseName() {
		return courseName;
	}
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
	public String getCoursePic() {
		return coursePic;
	}
	public void setCoursePic(String coursePic) {
		this.coursePic = coursePic;
	}
	public Integer getCourseStudyNum() {
		return courseStudyNum;
	}
	public void setCourseStudyNum(Integer courseStudyNum) {
		this.courseStudyNum = courseStudyNum;
	}
	public String getCourseClass() {
		return courseClass;
	}
	public void setCourseClass(String courseClass) {
		this.courseClass = courseClass;
	}
	public Integer getCourseOnlinetp() {
		return courseOnlinetp;
	}
	public void setCourseOnlinetp(Integer courseOnlinetp) {
		this.courseOnlinetp = courseOnlinetp;
	}
	public Long getOrgPrice() {
		return orgPrice;
	}
	public void setOrgPrice(Long orgPrice) {
		this.orgPrice = orgPrice;
	}
	public Long getCurPrice() {
		return curPrice;
	}
	public void setCurPrice(Long curPrice) {
		this.curPrice = curPrice;
	}
	public Integer getCourseGoodteacher() {
		return courseGoodteacher;
	}
	public void setCourseGoodteacher(Integer courseGoodteacher) {
		this.courseGoodteacher = courseGoodteacher;
	}
	public Long getCourseTp() {
		return courseTp;
	}
	public void setCourseTp(Long courseTp) {
		this.courseTp = courseTp;
	}
	public Integer getCommentStar() {
		return commentStar;
	}
	public void setCommentStar(Integer commentStar) {
		this.commentStar = commentStar;
	}
	public String getOrderBy() {
		return orderBy;
	}
	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}
	public Integer getCourseSubject() {
		return courseSubject;
	}
	public void setCourseSubject(Integer courseSubject) {
		this.courseSubject = courseSubject;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getCourseDesc() {
		return courseDesc;
	}
	public void setCourseDesc(String courseDesc) {
		this.courseDesc = courseDesc;
	}
	public Integer getCourseSize() {
		return courseSize;
	}
	public void setCourseSize(Integer courseSize) {
		this.courseSize = courseSize;
	}
	public Integer getCourseTime() {
		return courseTime;
	}
	public void setCourseTime(Integer courseTime) {
		this.courseTime = courseTime;
	}
	public Long getCourseTeacherid() {
		return courseTeacherid;
	}
	public void setCourseTeacherid(Long courseTeacherid) {
		this.courseTeacherid = courseTeacherid;
	}
	public String getCourseTeachernm() {
		return courseTeachernm;
	}
	public void setCourseTeachernm(String courseTeachernm) {
		this.courseTeachernm = courseTeachernm;
	}
	public Date getCoursePdt() {
		return coursePdt;
	}
	public void setCoursePdt(Date coursePdt) {
		this.coursePdt = coursePdt;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public Integer getCourseBoutique() {
		return courseBoutique;
	}

	public void setCourseBoutique(Integer courseBoutique) {
		this.courseBoutique = courseBoutique;
	}

	public Integer getCourseTimes() {
		return courseTimes;
	}

	public void setCourseTimes(Integer courseTimes) {
		this.courseTimes = courseTimes;
	}
}
