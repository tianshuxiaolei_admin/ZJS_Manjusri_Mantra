package com.drm.netschool.mapper;

import java.util.List;
import java.util.Map;

import com.drm.netschool.entity.CourseCategoryBean;
import com.drm.netschool.entity.CourseCtgExam;
import com.drm.netschool.entity.CourseCtgHomework;
import com.drm.netschool.entity.CourseInfoBean;
import com.drm.netschool.entity.TopicCourseBean;
import com.drm.netschool.entity.extend.BuyCourseRecord;
import com.drm.netschool.entity.extend.RecommendCourse;

public interface CourseInfoMapper {
	
	List<CourseInfoBean> getCourseInfoBeanList(Map<String, Object> searchInfo);

	/**
	 * 获得课程课件列表
	 * */
	List<CourseCategoryBean> getCourseCategoryBeanList(Map<String, Object> searchInfo);

	/**
	 * 获得课时作业列表
	 * */
	List<CourseCtgExam> getCourseExamBeanList(Map<String, Object> searchInfo);

	/**
	 * 获得课时考试列表
	 * 
	 * */
	List<CourseCtgHomework> getCourseHomeworkBeanList(Map<String, Object> searchInfo);

	/**
	 * 获取专题信息列表
	 * */
	List<TopicCourseBean> getTopicInfo(Map<String, Object> searchInfo);

	/********************************************* tsw ****************************************/

	/**
	 * 这个课程的购买记录
	 * 
	 * @param course
	 *            传递课程，不过有用的只是课程ID
	 * @param state
	 *            　传递　ｂｕｙ＿ｏｒｄｅｒ里面的状态，比如　不付款的也算
	 * @return
	 */
	public List<BuyCourseRecord> getBuyRecordsByCourseId(Map<String, Object> param);

	public int getBuyRecordsCntByCourseId(Map<String, Object> param);

	/**
	 * 
	 * Function: 功能说明： 一个课程中，总过课件的时长。。。。。。。。。 使用说明：
	 * 
	 * @author wangxiaolei DateTime 2015年8月27日 上午12:41:30 返回类型: int
	 * @param courseId
	 * @return
	 */
	public int getCateTimesByCourseId(Long courseId);

	/**
	 * 根据专题的 ID ，获取课程详情，下面的课程列表信息
	 * 
	 * @param course
	 *            专题，其中ID分成重要
	 * @return 专题下面的课程列表
	 */
	public List<RecommendCourse> listCourseByTopicId(Map<String, Object> param);
}
