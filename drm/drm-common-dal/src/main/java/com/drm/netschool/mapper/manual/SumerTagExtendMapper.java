package com.drm.netschool.mapper.manual;

import com.drm.netschool.entity.SumerTag;

import java.util.List;
import java.util.Map;


public interface SumerTagExtendMapper {
    
	public List<SumerTag> getListByPage(Map<String, Object> param);
	
	public int getCount(Map<String, Object> param);
	
	public List<SumerTag> getHostTagList(Map<String, Object> param);
}