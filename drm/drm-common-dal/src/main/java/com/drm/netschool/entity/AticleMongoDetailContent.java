package com.drm.netschool.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * 这个类是影射 保存 文字的时候 将详情保存在mongodb中的方法
 * 
 * @author Thinkpad
 * 
 */
@Document(collection = "drm_articles_detail")
public class AticleMongoDetailContent {

	public static final String ID = "_id";

	@Id
	private String id;
	private String detailContent;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDetailContent() {
		return detailContent;
	}

	public void setDetailContent(String detailContent) {
		this.detailContent = detailContent;
	}

}
