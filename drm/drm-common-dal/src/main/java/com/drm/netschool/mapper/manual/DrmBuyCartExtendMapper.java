package com.drm.netschool.mapper.manual;

import java.util.List;
import java.util.Map;

import com.drm.netschool.entity.DrmBuyCart;
import com.drm.netschool.entity.extend.CartInfo;

public interface DrmBuyCartExtendMapper {
	
	public List<DrmBuyCart> getListByPage(Map<String, Object> param);
	
	public int getCount(Map<String, Object> param);
	
	public int updateCartState(Map<String, Object> param);
	
	public List<CartInfo> getCartList(Map<String, Object> param);
}