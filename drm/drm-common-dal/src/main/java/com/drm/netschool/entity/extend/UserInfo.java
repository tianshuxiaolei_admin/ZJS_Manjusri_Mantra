package com.drm.netschool.entity.extend;

import java.util.Date;

public class UserInfo {
	
	private Long userId;
	private String userLnm;
	private String userRnm;
	private String userHimg;
	private String userSing;
	private String userMajor;
	private Integer browseCnt;
	private Integer rechargeSum;
	private Integer blogCnt;
	private Integer loninCnt;
	private Integer userIntegral;
	private Integer userTp;
	private String userDesc;
	private Date userCdt;
	private Integer fenCnt;	//粉丝数
	
	private String contend;
	
	private String orderBy;
	
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getUserLnm() {
		return userLnm;
	}
	public void setUserLnm(String userLnm) {
		this.userLnm = userLnm;
	}
	public String getUserRnm() {
		return userRnm;
	}
	public void setUserRnm(String userRnm) {
		this.userRnm = userRnm;
	}
	public String getUserHimg() {
		return userHimg;
	}
	public void setUserHimg(String userHimg) {
		this.userHimg = userHimg;
	}
	public String getUserSing() {
		return userSing;
	}
	public void setUserSing(String userSing) {
		this.userSing = userSing;
	}
	public String getUserMajor() {
		return userMajor;
	}
	public void setUserMajor(String userMajor) {
		this.userMajor = userMajor;
	}
	public Integer getBrowseCnt() {
		return browseCnt;
	}
	public void setBrowseCnt(Integer browseCnt) {
		this.browseCnt = browseCnt;
	}
	public Integer getRechargeSum() {
		return rechargeSum;
	}
	public void setRechargeSum(Integer rechargeSum) {
		this.rechargeSum = rechargeSum;
	}
	public Integer getBlogCnt() {
		return blogCnt;
	}
	public void setBlogCnt(Integer blogCnt) {
		this.blogCnt = blogCnt;
	}
	public Integer getLoninCnt() {
		return loninCnt;
	}
	public void setLoninCnt(Integer loninCnt) {
		this.loninCnt = loninCnt;
	}
	public Integer getUserIntegral() {
		return userIntegral;
	}
	public void setUserIntegral(Integer userIntegral) {
		this.userIntegral = userIntegral;
	}
	public Integer getUserTp() {
		return userTp;
	}
	public void setUserTp(Integer userTp) {
		this.userTp = userTp;
	}
	public String getUserDesc() {
		return userDesc;
	}
	public void setUserDesc(String userDesc) {
		this.userDesc = userDesc;
	}
	public Date getUserCdt() {
		return userCdt;
	}
	public void setUserCdt(Date userCdt) {
		this.userCdt = userCdt;
	}
	public Integer getFenCnt() {
		return fenCnt;
	}
	public void setFenCnt(Integer fenCnt) {
		this.fenCnt = fenCnt;
	}
	public String getOrderBy() {
		return orderBy;
	}
	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}
	public String getContend() {
		return contend;
	}
	public void setContend(String contend) {
		this.contend = contend;
	}
}
