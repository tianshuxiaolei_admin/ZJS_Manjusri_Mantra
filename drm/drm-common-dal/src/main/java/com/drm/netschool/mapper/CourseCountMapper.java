package com.drm.netschool.mapper;

import java.util.List;
import java.util.Map;

import com.drm.netschool.entity.CourseCountBean;

public interface CourseCountMapper {
	 public List<CourseCountBean> getCourseCountInfo(Map<String,Object> searchInfo);
}
