package com.drm.netschool.mapper;

import com.drm.netschool.entity.DrmArticles;
import com.drm.netschool.entity.DrmArticlesExample;
import java.util.List;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

public interface DrmArticlesMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table drm_articles
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    int countByExample(DrmArticlesExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table drm_articles
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    int deleteByExample(DrmArticlesExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table drm_articles
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    @Delete({
        "delete from drm_articles",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int deleteByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table drm_articles
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    @Insert({
        "insert into drm_articles (id, article_title, ",
        "notice_subtitle, article_tp, ",
        "article_keys, article_hotyn, ",
        "article_desc, article_image, ",
        "article_content, article_show_student, ",
        "article_attch_yn, article_cdt, ",
        "article_udt, brower_cnt, ",
        "article_headyn, article_cid, ",
        "article_uid, show_imgicon, ",
        "article_delyn, article_publishyn, ",
        "article_publishdt)",
        "values (#{id,jdbcType=BIGINT}, #{articleTitle,jdbcType=VARCHAR}, ",
        "#{noticeSubtitle,jdbcType=VARCHAR}, #{articleTp,jdbcType=INTEGER}, ",
        "#{articleKeys,jdbcType=VARCHAR}, #{articleHotyn,jdbcType=INTEGER}, ",
        "#{articleDesc,jdbcType=VARCHAR}, #{articleImage,jdbcType=VARCHAR}, ",
        "#{articleContent,jdbcType=VARCHAR}, #{articleShowStudent,jdbcType=INTEGER}, ",
        "#{articleAttchYn,jdbcType=VARCHAR}, #{articleCdt,jdbcType=TIMESTAMP}, ",
        "#{articleUdt,jdbcType=TIMESTAMP}, #{browerCnt,jdbcType=INTEGER}, ",
        "#{articleHeadyn,jdbcType=INTEGER}, #{articleCid,jdbcType=BIGINT}, ",
        "#{articleUid,jdbcType=BIGINT}, #{showImgicon,jdbcType=INTEGER}, ",
        "#{articleDelyn,jdbcType=INTEGER}, #{articlePublishyn,jdbcType=INTEGER}, ",
        "#{articlePublishdt,jdbcType=TIMESTAMP})"
    })
    int insert(DrmArticles record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table drm_articles
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    int insertSelective(DrmArticles record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table drm_articles
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    List<DrmArticles> selectByExample(DrmArticlesExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table drm_articles
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    @Select({
        "select",
        "id, article_title, notice_subtitle, article_tp, article_keys, article_hotyn, ",
        "article_desc, article_image, article_content, article_show_student, article_attch_yn, ",
        "article_cdt, article_udt, brower_cnt, article_headyn, article_cid, article_uid, ",
        "show_imgicon, article_delyn, article_publishyn, article_publishdt",
        "from drm_articles",
        "where id = #{id,jdbcType=BIGINT}"
    })
    @ResultMap("BaseResultMap")
    DrmArticles selectByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table drm_articles
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    int updateByExampleSelective(@Param("record") DrmArticles record, @Param("example") DrmArticlesExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table drm_articles
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    int updateByExample(@Param("record") DrmArticles record, @Param("example") DrmArticlesExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table drm_articles
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    int updateByPrimaryKeySelective(DrmArticles record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table drm_articles
     *
     * @mbggenerated Thu Oct 15 14:52:33 CST 2015
     */
    @Update({
        "update drm_articles",
        "set article_title = #{articleTitle,jdbcType=VARCHAR},",
          "notice_subtitle = #{noticeSubtitle,jdbcType=VARCHAR},",
          "article_tp = #{articleTp,jdbcType=INTEGER},",
          "article_keys = #{articleKeys,jdbcType=VARCHAR},",
          "article_hotyn = #{articleHotyn,jdbcType=INTEGER},",
          "article_desc = #{articleDesc,jdbcType=VARCHAR},",
          "article_image = #{articleImage,jdbcType=VARCHAR},",
          "article_content = #{articleContent,jdbcType=VARCHAR},",
          "article_show_student = #{articleShowStudent,jdbcType=INTEGER},",
          "article_attch_yn = #{articleAttchYn,jdbcType=VARCHAR},",
          "article_cdt = #{articleCdt,jdbcType=TIMESTAMP},",
          "article_udt = #{articleUdt,jdbcType=TIMESTAMP},",
          "brower_cnt = #{browerCnt,jdbcType=INTEGER},",
          "article_headyn = #{articleHeadyn,jdbcType=INTEGER},",
          "article_cid = #{articleCid,jdbcType=BIGINT},",
          "article_uid = #{articleUid,jdbcType=BIGINT},",
          "show_imgicon = #{showImgicon,jdbcType=INTEGER},",
          "article_delyn = #{articleDelyn,jdbcType=INTEGER},",
          "article_publishyn = #{articlePublishyn,jdbcType=INTEGER},",
          "article_publishdt = #{articlePublishdt,jdbcType=TIMESTAMP}",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int updateByPrimaryKey(DrmArticles record);
}