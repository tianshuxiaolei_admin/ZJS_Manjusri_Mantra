package com.drm.netschool.entity.extend;

import java.util.Date;

public class ResourceInfo {
	
    private Long id;

    private String name;

    private String fileId;
    
    private Date createTime;
    
    private Date updateTime;

	private String fileType;

    private Long resourceId;

	private Long fileSize;
    
    private String topic;
    
    private Integer viewCnt;
    
    private Integer downCnt;
    
    private Integer commentCnt;
    
    private Integer innergroup;
    
    private Integer needIntegral;
    
    private Long authorId;
    
    private String userLnm;
    
    private String userRnm;
    
    private String userHimg;
    
    private Date downloadTime;
    
	private String orderBy;
	
	private Integer weekDowncnt;
	
	private int tagId;
    
    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFileId() {
		return fileId;
	}

	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public Long getResourceId() {
		return resourceId;
	}

	public void setResourceId(Long resourceId) {
		this.resourceId = resourceId;
	}

	public Long getFileSize() {
		return fileSize;
	}

	public void setFileSize(Long fileSize) {
		this.fileSize = fileSize;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public Integer getViewCnt() {
		return viewCnt;
	}

	public void setViewCnt(Integer viewCnt) {
		this.viewCnt = viewCnt;
	}

	public Integer getDownCnt() {
		return downCnt;
	}

	public void setDownCnt(Integer downCnt) {
		this.downCnt = downCnt;
	}

	public Integer getCommentCnt() {
		return commentCnt;
	}

	public void setCommentCnt(Integer commentCnt) {
		this.commentCnt = commentCnt;
	}

	public Integer getInnergroup() {
		return innergroup;
	}

	public void setInnergroup(Integer innergroup) {
		this.innergroup = innergroup;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}
	
	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Integer getNeedIntegral() {
		return needIntegral;
	}

	public void setNeedIntegral(Integer needIntegral) {
		this.needIntegral = needIntegral;
	}

	public Long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	public String getUserLnm() {
		return userLnm;
	}

	public void setUserLnm(String userLnm) {
		this.userLnm = userLnm;
	}

	public String getUserRnm() {
		return userRnm;
	}

	public void setUserRnm(String userRnm) {
		this.userRnm = userRnm;
	}
	
	public String getUserHimg() {
		return userHimg;
	}

	public void setUserHimg(String userHimg) {
		this.userHimg = userHimg;
	}

	public Date getDownloadTime() {
		return downloadTime;
	}

	public void setDownloadTime(Date downloadTime) {
		this.downloadTime = downloadTime;
	}

	public int getTagId() {
		return tagId;
	}

	public void setTagId(int tagId) {
		this.tagId = tagId;
	}

	public Integer getWeekDowncnt() {
		return weekDowncnt;
	}

	public void setWeekDowncnt(Integer weekDowncnt) {
		this.weekDowncnt = weekDowncnt;
	}
}
