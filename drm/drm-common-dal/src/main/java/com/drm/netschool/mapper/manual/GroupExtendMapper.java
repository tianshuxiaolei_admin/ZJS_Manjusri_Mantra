package com.drm.netschool.mapper.manual;

import java.util.List;
import java.util.Map;

import com.drm.netschool.entity.extend.GroupInfo;

public interface GroupExtendMapper {
	
	public int getGroupInfoCount(Map<String, Object> param);

	public List<GroupInfo> getGroupInfoList(Map<String, Object> param);
}
