package com.drm.netschool.mapper.manual;

import java.util.List;
import java.util.Map;

import com.drm.netschool.entity.DrmCodes;

public interface DrmCodesExtendMapper {
    
	public List<DrmCodes> getListByPage(Map<String, Object> param);
	
	public int getCount(Map<String, Object> param);
}