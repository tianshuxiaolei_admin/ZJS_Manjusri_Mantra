package com.drm.netschool.entity;

public class DrmCourseStatus {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column drm_course_status.id
     *
     * @mbggenerated Thu Aug 13 00:51:57 CST 2015
     */
    private Long id;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column drm_course_status.course_id
     *
     * @mbggenerated Thu Aug 13 00:51:57 CST 2015
     */
    private Long courseId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column drm_course_status.course_boutique
     *
     * @mbggenerated Thu Aug 13 00:51:57 CST 2015
     */
    private Integer courseBoutique;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column drm_course_status.course_checkyn
     *
     * @mbggenerated Thu Aug 13 00:51:57 CST 2015
     */
    private Integer courseCheckyn;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column drm_course_status.course_publishyn
     *
     * @mbggenerated Thu Aug 13 00:51:57 CST 2015
     */
    private Integer coursePublishyn;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column drm_course_status.course_goodteacher
     *
     * @mbggenerated Thu Aug 13 00:51:57 CST 2015
     */
    private Integer courseGoodteacher;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column drm_course_status.course_lookset_islogin
     *
     * @mbggenerated Thu Aug 13 00:51:57 CST 2015
     */
    private Integer courseLooksetIslogin;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column drm_course_status.course_lookset_islogin_time
     *
     * @mbggenerated Thu Aug 13 00:51:57 CST 2015
     */
    private Integer courseLooksetIsloginTime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column drm_course_status.course_lookset_nologin
     *
     * @mbggenerated Thu Aug 13 00:51:57 CST 2015
     */
    private Integer courseLooksetNologin;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column drm_course_status.course_lookset_nologin_time
     *
     * @mbggenerated Thu Aug 13 00:51:57 CST 2015
     */
    private Integer courseLooksetNologinTime;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column drm_course_status.id
     *
     * @return the value of drm_course_status.id
     *
     * @mbggenerated Thu Aug 13 00:51:57 CST 2015
     */
    public Long getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column drm_course_status.id
     *
     * @param id the value for drm_course_status.id
     *
     * @mbggenerated Thu Aug 13 00:51:57 CST 2015
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column drm_course_status.course_id
     *
     * @return the value of drm_course_status.course_id
     *
     * @mbggenerated Thu Aug 13 00:51:57 CST 2015
     */
    public Long getCourseId() {
        return courseId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column drm_course_status.course_id
     *
     * @param courseId the value for drm_course_status.course_id
     *
     * @mbggenerated Thu Aug 13 00:51:57 CST 2015
     */
    public void setCourseId(Long courseId) {
        this.courseId = courseId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column drm_course_status.course_boutique
     *
     * @return the value of drm_course_status.course_boutique
     *
     * @mbggenerated Thu Aug 13 00:51:57 CST 2015
     */
    public Integer getCourseBoutique() {
        return courseBoutique;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column drm_course_status.course_boutique
     *
     * @param courseBoutique the value for drm_course_status.course_boutique
     *
     * @mbggenerated Thu Aug 13 00:51:57 CST 2015
     */
    public void setCourseBoutique(Integer courseBoutique) {
        this.courseBoutique = courseBoutique;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column drm_course_status.course_checkyn
     *
     * @return the value of drm_course_status.course_checkyn
     *
     * @mbggenerated Thu Aug 13 00:51:57 CST 2015
     */
    public Integer getCourseCheckyn() {
        return courseCheckyn;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column drm_course_status.course_checkyn
     *
     * @param courseCheckyn the value for drm_course_status.course_checkyn
     *
     * @mbggenerated Thu Aug 13 00:51:57 CST 2015
     */
    public void setCourseCheckyn(Integer courseCheckyn) {
        this.courseCheckyn = courseCheckyn;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column drm_course_status.course_publishyn
     *
     * @return the value of drm_course_status.course_publishyn
     *
     * @mbggenerated Thu Aug 13 00:51:57 CST 2015
     */
    public Integer getCoursePublishyn() {
        return coursePublishyn;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column drm_course_status.course_publishyn
     *
     * @param coursePublishyn the value for drm_course_status.course_publishyn
     *
     * @mbggenerated Thu Aug 13 00:51:57 CST 2015
     */
    public void setCoursePublishyn(Integer coursePublishyn) {
        this.coursePublishyn = coursePublishyn;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column drm_course_status.course_goodteacher
     *
     * @return the value of drm_course_status.course_goodteacher
     *
     * @mbggenerated Thu Aug 13 00:51:57 CST 2015
     */
    public Integer getCourseGoodteacher() {
        return courseGoodteacher;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column drm_course_status.course_goodteacher
     *
     * @param courseGoodteacher the value for drm_course_status.course_goodteacher
     *
     * @mbggenerated Thu Aug 13 00:51:57 CST 2015
     */
    public void setCourseGoodteacher(Integer courseGoodteacher) {
        this.courseGoodteacher = courseGoodteacher;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column drm_course_status.course_lookset_islogin
     *
     * @return the value of drm_course_status.course_lookset_islogin
     *
     * @mbggenerated Thu Aug 13 00:51:57 CST 2015
     */
    public Integer getCourseLooksetIslogin() {
        return courseLooksetIslogin;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column drm_course_status.course_lookset_islogin
     *
     * @param courseLooksetIslogin the value for drm_course_status.course_lookset_islogin
     *
     * @mbggenerated Thu Aug 13 00:51:57 CST 2015
     */
    public void setCourseLooksetIslogin(Integer courseLooksetIslogin) {
        this.courseLooksetIslogin = courseLooksetIslogin;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column drm_course_status.course_lookset_islogin_time
     *
     * @return the value of drm_course_status.course_lookset_islogin_time
     *
     * @mbggenerated Thu Aug 13 00:51:57 CST 2015
     */
    public Integer getCourseLooksetIsloginTime() {
        return courseLooksetIsloginTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column drm_course_status.course_lookset_islogin_time
     *
     * @param courseLooksetIsloginTime the value for drm_course_status.course_lookset_islogin_time
     *
     * @mbggenerated Thu Aug 13 00:51:57 CST 2015
     */
    public void setCourseLooksetIsloginTime(Integer courseLooksetIsloginTime) {
        this.courseLooksetIsloginTime = courseLooksetIsloginTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column drm_course_status.course_lookset_nologin
     *
     * @return the value of drm_course_status.course_lookset_nologin
     *
     * @mbggenerated Thu Aug 13 00:51:57 CST 2015
     */
    public Integer getCourseLooksetNologin() {
        return courseLooksetNologin;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column drm_course_status.course_lookset_nologin
     *
     * @param courseLooksetNologin the value for drm_course_status.course_lookset_nologin
     *
     * @mbggenerated Thu Aug 13 00:51:57 CST 2015
     */
    public void setCourseLooksetNologin(Integer courseLooksetNologin) {
        this.courseLooksetNologin = courseLooksetNologin;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column drm_course_status.course_lookset_nologin_time
     *
     * @return the value of drm_course_status.course_lookset_nologin_time
     *
     * @mbggenerated Thu Aug 13 00:51:57 CST 2015
     */
    public Integer getCourseLooksetNologinTime() {
        return courseLooksetNologinTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column drm_course_status.course_lookset_nologin_time
     *
     * @param courseLooksetNologinTime the value for drm_course_status.course_lookset_nologin_time
     *
     * @mbggenerated Thu Aug 13 00:51:57 CST 2015
     */
    public void setCourseLooksetNologinTime(Integer courseLooksetNologinTime) {
        this.courseLooksetNologinTime = courseLooksetNologinTime;
    }
}