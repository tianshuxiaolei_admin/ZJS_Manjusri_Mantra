package com.drm.netschool.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SumerGroupBaseExample {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table sumer_group_base
     *
     * @mbggenerated Sat Aug 01 12:55:27 CST 2015
     */
    protected String orderByClause;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table sumer_group_base
     *
     * @mbggenerated Sat Aug 01 12:55:27 CST 2015
     */
    protected boolean distinct;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table sumer_group_base
     *
     * @mbggenerated Sat Aug 01 12:55:27 CST 2015
     */
    protected List<Criteria> oredCriteria;

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sumer_group_base
     *
     * @mbggenerated Sat Aug 01 12:55:27 CST 2015
     */
    public SumerGroupBaseExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sumer_group_base
     *
     * @mbggenerated Sat Aug 01 12:55:27 CST 2015
     */
    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sumer_group_base
     *
     * @mbggenerated Sat Aug 01 12:55:27 CST 2015
     */
    public String getOrderByClause() {
        return orderByClause;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sumer_group_base
     *
     * @mbggenerated Sat Aug 01 12:55:27 CST 2015
     */
    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sumer_group_base
     *
     * @mbggenerated Sat Aug 01 12:55:27 CST 2015
     */
    public boolean isDistinct() {
        return distinct;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sumer_group_base
     *
     * @mbggenerated Sat Aug 01 12:55:27 CST 2015
     */
    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sumer_group_base
     *
     * @mbggenerated Sat Aug 01 12:55:27 CST 2015
     */
    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sumer_group_base
     *
     * @mbggenerated Sat Aug 01 12:55:27 CST 2015
     */
    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sumer_group_base
     *
     * @mbggenerated Sat Aug 01 12:55:27 CST 2015
     */
    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sumer_group_base
     *
     * @mbggenerated Sat Aug 01 12:55:27 CST 2015
     */
    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sumer_group_base
     *
     * @mbggenerated Sat Aug 01 12:55:27 CST 2015
     */
    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table sumer_group_base
     *
     * @mbggenerated Sat Aug 01 12:55:27 CST 2015
     */
    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andGroupIdIsNull() {
            addCriterion("group_id is null");
            return (Criteria) this;
        }

        public Criteria andGroupIdIsNotNull() {
            addCriterion("group_id is not null");
            return (Criteria) this;
        }

        public Criteria andGroupIdEqualTo(Long value) {
            addCriterion("group_id =", value, "groupId");
            return (Criteria) this;
        }

        public Criteria andGroupIdNotEqualTo(Long value) {
            addCriterion("group_id <>", value, "groupId");
            return (Criteria) this;
        }

        public Criteria andGroupIdGreaterThan(Long value) {
            addCriterion("group_id >", value, "groupId");
            return (Criteria) this;
        }

        public Criteria andGroupIdGreaterThanOrEqualTo(Long value) {
            addCriterion("group_id >=", value, "groupId");
            return (Criteria) this;
        }

        public Criteria andGroupIdLessThan(Long value) {
            addCriterion("group_id <", value, "groupId");
            return (Criteria) this;
        }

        public Criteria andGroupIdLessThanOrEqualTo(Long value) {
            addCriterion("group_id <=", value, "groupId");
            return (Criteria) this;
        }

        public Criteria andGroupIdIn(List<Long> values) {
            addCriterion("group_id in", values, "groupId");
            return (Criteria) this;
        }

        public Criteria andGroupIdNotIn(List<Long> values) {
            addCriterion("group_id not in", values, "groupId");
            return (Criteria) this;
        }

        public Criteria andGroupIdBetween(Long value1, Long value2) {
            addCriterion("group_id between", value1, value2, "groupId");
            return (Criteria) this;
        }

        public Criteria andGroupIdNotBetween(Long value1, Long value2) {
            addCriterion("group_id not between", value1, value2, "groupId");
            return (Criteria) this;
        }

        public Criteria andGroupNmIsNull() {
            addCriterion("group_nm is null");
            return (Criteria) this;
        }

        public Criteria andGroupNmIsNotNull() {
            addCriterion("group_nm is not null");
            return (Criteria) this;
        }

        public Criteria andGroupNmEqualTo(String value) {
            addCriterion("group_nm =", value, "groupNm");
            return (Criteria) this;
        }

        public Criteria andGroupNmNotEqualTo(String value) {
            addCriterion("group_nm <>", value, "groupNm");
            return (Criteria) this;
        }

        public Criteria andGroupNmGreaterThan(String value) {
            addCriterion("group_nm >", value, "groupNm");
            return (Criteria) this;
        }

        public Criteria andGroupNmGreaterThanOrEqualTo(String value) {
            addCriterion("group_nm >=", value, "groupNm");
            return (Criteria) this;
        }

        public Criteria andGroupNmLessThan(String value) {
            addCriterion("group_nm <", value, "groupNm");
            return (Criteria) this;
        }

        public Criteria andGroupNmLessThanOrEqualTo(String value) {
            addCriterion("group_nm <=", value, "groupNm");
            return (Criteria) this;
        }

        public Criteria andGroupNmLike(String value) {
            addCriterion("group_nm like", value, "groupNm");
            return (Criteria) this;
        }

        public Criteria andGroupNmNotLike(String value) {
            addCriterion("group_nm not like", value, "groupNm");
            return (Criteria) this;
        }

        public Criteria andGroupNmIn(List<String> values) {
            addCriterion("group_nm in", values, "groupNm");
            return (Criteria) this;
        }

        public Criteria andGroupNmNotIn(List<String> values) {
            addCriterion("group_nm not in", values, "groupNm");
            return (Criteria) this;
        }

        public Criteria andGroupNmBetween(String value1, String value2) {
            addCriterion("group_nm between", value1, value2, "groupNm");
            return (Criteria) this;
        }

        public Criteria andGroupNmNotBetween(String value1, String value2) {
            addCriterion("group_nm not between", value1, value2, "groupNm");
            return (Criteria) this;
        }

        public Criteria andGroupTxtIsNull() {
            addCriterion("group_txt is null");
            return (Criteria) this;
        }

        public Criteria andGroupTxtIsNotNull() {
            addCriterion("group_txt is not null");
            return (Criteria) this;
        }

        public Criteria andGroupTxtEqualTo(String value) {
            addCriterion("group_txt =", value, "groupTxt");
            return (Criteria) this;
        }

        public Criteria andGroupTxtNotEqualTo(String value) {
            addCriterion("group_txt <>", value, "groupTxt");
            return (Criteria) this;
        }

        public Criteria andGroupTxtGreaterThan(String value) {
            addCriterion("group_txt >", value, "groupTxt");
            return (Criteria) this;
        }

        public Criteria andGroupTxtGreaterThanOrEqualTo(String value) {
            addCriterion("group_txt >=", value, "groupTxt");
            return (Criteria) this;
        }

        public Criteria andGroupTxtLessThan(String value) {
            addCriterion("group_txt <", value, "groupTxt");
            return (Criteria) this;
        }

        public Criteria andGroupTxtLessThanOrEqualTo(String value) {
            addCriterion("group_txt <=", value, "groupTxt");
            return (Criteria) this;
        }

        public Criteria andGroupTxtLike(String value) {
            addCriterion("group_txt like", value, "groupTxt");
            return (Criteria) this;
        }

        public Criteria andGroupTxtNotLike(String value) {
            addCriterion("group_txt not like", value, "groupTxt");
            return (Criteria) this;
        }

        public Criteria andGroupTxtIn(List<String> values) {
            addCriterion("group_txt in", values, "groupTxt");
            return (Criteria) this;
        }

        public Criteria andGroupTxtNotIn(List<String> values) {
            addCriterion("group_txt not in", values, "groupTxt");
            return (Criteria) this;
        }

        public Criteria andGroupTxtBetween(String value1, String value2) {
            addCriterion("group_txt between", value1, value2, "groupTxt");
            return (Criteria) this;
        }

        public Criteria andGroupTxtNotBetween(String value1, String value2) {
            addCriterion("group_txt not between", value1, value2, "groupTxt");
            return (Criteria) this;
        }

        public Criteria andGroupImgIsNull() {
            addCriterion("group_img is null");
            return (Criteria) this;
        }

        public Criteria andGroupImgIsNotNull() {
            addCriterion("group_img is not null");
            return (Criteria) this;
        }

        public Criteria andGroupImgEqualTo(String value) {
            addCriterion("group_img =", value, "groupImg");
            return (Criteria) this;
        }

        public Criteria andGroupImgNotEqualTo(String value) {
            addCriterion("group_img <>", value, "groupImg");
            return (Criteria) this;
        }

        public Criteria andGroupImgGreaterThan(String value) {
            addCriterion("group_img >", value, "groupImg");
            return (Criteria) this;
        }

        public Criteria andGroupImgGreaterThanOrEqualTo(String value) {
            addCriterion("group_img >=", value, "groupImg");
            return (Criteria) this;
        }

        public Criteria andGroupImgLessThan(String value) {
            addCriterion("group_img <", value, "groupImg");
            return (Criteria) this;
        }

        public Criteria andGroupImgLessThanOrEqualTo(String value) {
            addCriterion("group_img <=", value, "groupImg");
            return (Criteria) this;
        }

        public Criteria andGroupImgLike(String value) {
            addCriterion("group_img like", value, "groupImg");
            return (Criteria) this;
        }

        public Criteria andGroupImgNotLike(String value) {
            addCriterion("group_img not like", value, "groupImg");
            return (Criteria) this;
        }

        public Criteria andGroupImgIn(List<String> values) {
            addCriterion("group_img in", values, "groupImg");
            return (Criteria) this;
        }

        public Criteria andGroupImgNotIn(List<String> values) {
            addCriterion("group_img not in", values, "groupImg");
            return (Criteria) this;
        }

        public Criteria andGroupImgBetween(String value1, String value2) {
            addCriterion("group_img between", value1, value2, "groupImg");
            return (Criteria) this;
        }

        public Criteria andGroupImgNotBetween(String value1, String value2) {
            addCriterion("group_img not between", value1, value2, "groupImg");
            return (Criteria) this;
        }

        public Criteria andCreateIdIsNull() {
            addCriterion("create_id is null");
            return (Criteria) this;
        }

        public Criteria andCreateIdIsNotNull() {
            addCriterion("create_id is not null");
            return (Criteria) this;
        }

        public Criteria andCreateIdEqualTo(Long value) {
            addCriterion("create_id =", value, "createId");
            return (Criteria) this;
        }

        public Criteria andCreateIdNotEqualTo(Long value) {
            addCriterion("create_id <>", value, "createId");
            return (Criteria) this;
        }

        public Criteria andCreateIdGreaterThan(Long value) {
            addCriterion("create_id >", value, "createId");
            return (Criteria) this;
        }

        public Criteria andCreateIdGreaterThanOrEqualTo(Long value) {
            addCriterion("create_id >=", value, "createId");
            return (Criteria) this;
        }

        public Criteria andCreateIdLessThan(Long value) {
            addCriterion("create_id <", value, "createId");
            return (Criteria) this;
        }

        public Criteria andCreateIdLessThanOrEqualTo(Long value) {
            addCriterion("create_id <=", value, "createId");
            return (Criteria) this;
        }

        public Criteria andCreateIdIn(List<Long> values) {
            addCriterion("create_id in", values, "createId");
            return (Criteria) this;
        }

        public Criteria andCreateIdNotIn(List<Long> values) {
            addCriterion("create_id not in", values, "createId");
            return (Criteria) this;
        }

        public Criteria andCreateIdBetween(Long value1, Long value2) {
            addCriterion("create_id between", value1, value2, "createId");
            return (Criteria) this;
        }

        public Criteria andCreateIdNotBetween(Long value1, Long value2) {
            addCriterion("create_id not between", value1, value2, "createId");
            return (Criteria) this;
        }

        public Criteria andCreateDtIsNull() {
            addCriterion("create_dt is null");
            return (Criteria) this;
        }

        public Criteria andCreateDtIsNotNull() {
            addCriterion("create_dt is not null");
            return (Criteria) this;
        }

        public Criteria andCreateDtEqualTo(Date value) {
            addCriterion("create_dt =", value, "createDt");
            return (Criteria) this;
        }

        public Criteria andCreateDtNotEqualTo(Date value) {
            addCriterion("create_dt <>", value, "createDt");
            return (Criteria) this;
        }

        public Criteria andCreateDtGreaterThan(Date value) {
            addCriterion("create_dt >", value, "createDt");
            return (Criteria) this;
        }

        public Criteria andCreateDtGreaterThanOrEqualTo(Date value) {
            addCriterion("create_dt >=", value, "createDt");
            return (Criteria) this;
        }

        public Criteria andCreateDtLessThan(Date value) {
            addCriterion("create_dt <", value, "createDt");
            return (Criteria) this;
        }

        public Criteria andCreateDtLessThanOrEqualTo(Date value) {
            addCriterion("create_dt <=", value, "createDt");
            return (Criteria) this;
        }

        public Criteria andCreateDtIn(List<Date> values) {
            addCriterion("create_dt in", values, "createDt");
            return (Criteria) this;
        }

        public Criteria andCreateDtNotIn(List<Date> values) {
            addCriterion("create_dt not in", values, "createDt");
            return (Criteria) this;
        }

        public Criteria andCreateDtBetween(Date value1, Date value2) {
            addCriterion("create_dt between", value1, value2, "createDt");
            return (Criteria) this;
        }

        public Criteria andCreateDtNotBetween(Date value1, Date value2) {
            addCriterion("create_dt not between", value1, value2, "createDt");
            return (Criteria) this;
        }

        public Criteria andDelYnIsNull() {
            addCriterion("del_yn is null");
            return (Criteria) this;
        }

        public Criteria andDelYnIsNotNull() {
            addCriterion("del_yn is not null");
            return (Criteria) this;
        }

        public Criteria andDelYnEqualTo(Integer value) {
            addCriterion("del_yn =", value, "delYn");
            return (Criteria) this;
        }

        public Criteria andDelYnNotEqualTo(Integer value) {
            addCriterion("del_yn <>", value, "delYn");
            return (Criteria) this;
        }

        public Criteria andDelYnGreaterThan(Integer value) {
            addCriterion("del_yn >", value, "delYn");
            return (Criteria) this;
        }

        public Criteria andDelYnGreaterThanOrEqualTo(Integer value) {
            addCriterion("del_yn >=", value, "delYn");
            return (Criteria) this;
        }

        public Criteria andDelYnLessThan(Integer value) {
            addCriterion("del_yn <", value, "delYn");
            return (Criteria) this;
        }

        public Criteria andDelYnLessThanOrEqualTo(Integer value) {
            addCriterion("del_yn <=", value, "delYn");
            return (Criteria) this;
        }

        public Criteria andDelYnIn(List<Integer> values) {
            addCriterion("del_yn in", values, "delYn");
            return (Criteria) this;
        }

        public Criteria andDelYnNotIn(List<Integer> values) {
            addCriterion("del_yn not in", values, "delYn");
            return (Criteria) this;
        }

        public Criteria andDelYnBetween(Integer value1, Integer value2) {
            addCriterion("del_yn between", value1, value2, "delYn");
            return (Criteria) this;
        }

        public Criteria andDelYnNotBetween(Integer value1, Integer value2) {
            addCriterion("del_yn not between", value1, value2, "delYn");
            return (Criteria) this;
        }

        public Criteria andClassTpIsNull() {
            addCriterion("class_tp is null");
            return (Criteria) this;
        }

        public Criteria andClassTpIsNotNull() {
            addCriterion("class_tp is not null");
            return (Criteria) this;
        }

        public Criteria andClassTpEqualTo(String value) {
            addCriterion("class_tp =", value, "classTp");
            return (Criteria) this;
        }

        public Criteria andClassTpNotEqualTo(String value) {
            addCriterion("class_tp <>", value, "classTp");
            return (Criteria) this;
        }

        public Criteria andClassTpGreaterThan(String value) {
            addCriterion("class_tp >", value, "classTp");
            return (Criteria) this;
        }

        public Criteria andClassTpGreaterThanOrEqualTo(String value) {
            addCriterion("class_tp >=", value, "classTp");
            return (Criteria) this;
        }

        public Criteria andClassTpLessThan(String value) {
            addCriterion("class_tp <", value, "classTp");
            return (Criteria) this;
        }

        public Criteria andClassTpLessThanOrEqualTo(String value) {
            addCriterion("class_tp <=", value, "classTp");
            return (Criteria) this;
        }

        public Criteria andClassTpLike(String value) {
            addCriterion("class_tp like", value, "classTp");
            return (Criteria) this;
        }

        public Criteria andClassTpNotLike(String value) {
            addCriterion("class_tp not like", value, "classTp");
            return (Criteria) this;
        }

        public Criteria andClassTpIn(List<String> values) {
            addCriterion("class_tp in", values, "classTp");
            return (Criteria) this;
        }

        public Criteria andClassTpNotIn(List<String> values) {
            addCriterion("class_tp not in", values, "classTp");
            return (Criteria) this;
        }

        public Criteria andClassTpBetween(String value1, String value2) {
            addCriterion("class_tp between", value1, value2, "classTp");
            return (Criteria) this;
        }

        public Criteria andClassTpNotBetween(String value1, String value2) {
            addCriterion("class_tp not between", value1, value2, "classTp");
            return (Criteria) this;
        }

        public Criteria andLocationNmIsNull() {
            addCriterion("location_nm is null");
            return (Criteria) this;
        }

        public Criteria andLocationNmIsNotNull() {
            addCriterion("location_nm is not null");
            return (Criteria) this;
        }

        public Criteria andLocationNmEqualTo(String value) {
            addCriterion("location_nm =", value, "locationNm");
            return (Criteria) this;
        }

        public Criteria andLocationNmNotEqualTo(String value) {
            addCriterion("location_nm <>", value, "locationNm");
            return (Criteria) this;
        }

        public Criteria andLocationNmGreaterThan(String value) {
            addCriterion("location_nm >", value, "locationNm");
            return (Criteria) this;
        }

        public Criteria andLocationNmGreaterThanOrEqualTo(String value) {
            addCriterion("location_nm >=", value, "locationNm");
            return (Criteria) this;
        }

        public Criteria andLocationNmLessThan(String value) {
            addCriterion("location_nm <", value, "locationNm");
            return (Criteria) this;
        }

        public Criteria andLocationNmLessThanOrEqualTo(String value) {
            addCriterion("location_nm <=", value, "locationNm");
            return (Criteria) this;
        }

        public Criteria andLocationNmLike(String value) {
            addCriterion("location_nm like", value, "locationNm");
            return (Criteria) this;
        }

        public Criteria andLocationNmNotLike(String value) {
            addCriterion("location_nm not like", value, "locationNm");
            return (Criteria) this;
        }

        public Criteria andLocationNmIn(List<String> values) {
            addCriterion("location_nm in", values, "locationNm");
            return (Criteria) this;
        }

        public Criteria andLocationNmNotIn(List<String> values) {
            addCriterion("location_nm not in", values, "locationNm");
            return (Criteria) this;
        }

        public Criteria andLocationNmBetween(String value1, String value2) {
            addCriterion("location_nm between", value1, value2, "locationNm");
            return (Criteria) this;
        }

        public Criteria andLocationNmNotBetween(String value1, String value2) {
            addCriterion("location_nm not between", value1, value2, "locationNm");
            return (Criteria) this;
        }

        public Criteria andGroupNoticeIsNull() {
            addCriterion("group_notice is null");
            return (Criteria) this;
        }

        public Criteria andGroupNoticeIsNotNull() {
            addCriterion("group_notice is not null");
            return (Criteria) this;
        }

        public Criteria andGroupNoticeEqualTo(String value) {
            addCriterion("group_notice =", value, "groupNotice");
            return (Criteria) this;
        }

        public Criteria andGroupNoticeNotEqualTo(String value) {
            addCriterion("group_notice <>", value, "groupNotice");
            return (Criteria) this;
        }

        public Criteria andGroupNoticeGreaterThan(String value) {
            addCriterion("group_notice >", value, "groupNotice");
            return (Criteria) this;
        }

        public Criteria andGroupNoticeGreaterThanOrEqualTo(String value) {
            addCriterion("group_notice >=", value, "groupNotice");
            return (Criteria) this;
        }

        public Criteria andGroupNoticeLessThan(String value) {
            addCriterion("group_notice <", value, "groupNotice");
            return (Criteria) this;
        }

        public Criteria andGroupNoticeLessThanOrEqualTo(String value) {
            addCriterion("group_notice <=", value, "groupNotice");
            return (Criteria) this;
        }

        public Criteria andGroupNoticeLike(String value) {
            addCriterion("group_notice like", value, "groupNotice");
            return (Criteria) this;
        }

        public Criteria andGroupNoticeNotLike(String value) {
            addCriterion("group_notice not like", value, "groupNotice");
            return (Criteria) this;
        }

        public Criteria andGroupNoticeIn(List<String> values) {
            addCriterion("group_notice in", values, "groupNotice");
            return (Criteria) this;
        }

        public Criteria andGroupNoticeNotIn(List<String> values) {
            addCriterion("group_notice not in", values, "groupNotice");
            return (Criteria) this;
        }

        public Criteria andGroupNoticeBetween(String value1, String value2) {
            addCriterion("group_notice between", value1, value2, "groupNotice");
            return (Criteria) this;
        }

        public Criteria andGroupNoticeNotBetween(String value1, String value2) {
            addCriterion("group_notice not between", value1, value2, "groupNotice");
            return (Criteria) this;
        }
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table sumer_group_base
     *
     * @mbggenerated do_not_delete_during_merge Sat Aug 01 12:55:27 CST 2015
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table sumer_group_base
     *
     * @mbggenerated Sat Aug 01 12:55:27 CST 2015
     */
    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}