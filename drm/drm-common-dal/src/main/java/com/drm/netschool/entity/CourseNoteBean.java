package com.drm.netschool.entity;

import java.io.Serializable;
import java.util.Date;
/**
 * @author chenxuezheng
 * 课程笔记Bean
 * */
public class CourseNoteBean implements Serializable {
	private long id;// 笔记编号
	private long courseId;// 课程编号
	private String noteNm;// 笔记名称
	private String noteTxt;// 笔记内容
	private String userName;//用户昵称
	private String userImg;//用户头像
    private long userId;//用户id
    private Date updateDt; //更新时间
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getCourseId() {
		return courseId;
	}
	public void setCourseId(long courseId) {
		this.courseId = courseId;
	}
	public String getNoteNm() {
		return noteNm;
	}
	public void setNoteNm(String noteNm) {
		this.noteNm = noteNm;
	}
	public String getNoteTxt() {
		return noteTxt;
	}
	public void setNoteTxt(String noteTxt) {
		this.noteTxt = noteTxt;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserImg() {
		return userImg;
	}
	public void setUserImg(String userImg) {
		this.userImg = userImg;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public Date getUpdateDt() {
		return updateDt;
	}
	public void setUpdateDt(Date updateDt) {
		this.updateDt = updateDt;
	}
    
    
}
