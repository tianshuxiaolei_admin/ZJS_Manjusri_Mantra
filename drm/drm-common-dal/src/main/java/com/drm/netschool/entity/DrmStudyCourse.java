package com.drm.netschool.entity;

import java.util.Date;

public class DrmStudyCourse {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column drm_study_course.id
     *
     * @mbggenerated Mon Sep 07 01:49:26 CST 2015
     */
    private Long id;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column drm_study_course.course_id
     *
     * @mbggenerated Mon Sep 07 01:49:26 CST 2015
     */
    private Long courseId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column drm_study_course.user_id
     *
     * @mbggenerated Mon Sep 07 01:49:26 CST 2015
     */
    private Long userId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column drm_study_course.study_cnt
     *
     * @mbggenerated Mon Sep 07 01:49:26 CST 2015
     */
    private Integer studyCnt;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column drm_study_course.study_time
     *
     * @mbggenerated Mon Sep 07 01:49:26 CST 2015
     */
    private Integer studyTime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column drm_study_course.can_study
     *
     * @mbggenerated Mon Sep 07 01:49:26 CST 2015
     */
    private Integer canStudy;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column drm_study_course.progress
     *
     * @mbggenerated Mon Sep 07 01:49:26 CST 2015
     */
    private Integer progress;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column drm_study_course.laststudy_dt
     *
     * @mbggenerated Mon Sep 07 01:49:26 CST 2015
     */
    private Date laststudyDt;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column drm_study_course.create_time
     *
     * @mbggenerated Mon Sep 07 01:49:26 CST 2015
     */
    private Date createTime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column drm_study_course.remark
     *
     * @mbggenerated Mon Sep 07 01:49:26 CST 2015
     */
    private String remark;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column drm_study_course.id
     *
     * @return the value of drm_study_course.id
     *
     * @mbggenerated Mon Sep 07 01:49:26 CST 2015
     */
    public Long getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column drm_study_course.id
     *
     * @param id the value for drm_study_course.id
     *
     * @mbggenerated Mon Sep 07 01:49:26 CST 2015
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column drm_study_course.course_id
     *
     * @return the value of drm_study_course.course_id
     *
     * @mbggenerated Mon Sep 07 01:49:26 CST 2015
     */
    public Long getCourseId() {
        return courseId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column drm_study_course.course_id
     *
     * @param courseId the value for drm_study_course.course_id
     *
     * @mbggenerated Mon Sep 07 01:49:26 CST 2015
     */
    public void setCourseId(Long courseId) {
        this.courseId = courseId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column drm_study_course.user_id
     *
     * @return the value of drm_study_course.user_id
     *
     * @mbggenerated Mon Sep 07 01:49:26 CST 2015
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column drm_study_course.user_id
     *
     * @param userId the value for drm_study_course.user_id
     *
     * @mbggenerated Mon Sep 07 01:49:26 CST 2015
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column drm_study_course.study_cnt
     *
     * @return the value of drm_study_course.study_cnt
     *
     * @mbggenerated Mon Sep 07 01:49:26 CST 2015
     */
    public Integer getStudyCnt() {
        return studyCnt;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column drm_study_course.study_cnt
     *
     * @param studyCnt the value for drm_study_course.study_cnt
     *
     * @mbggenerated Mon Sep 07 01:49:26 CST 2015
     */
    public void setStudyCnt(Integer studyCnt) {
        this.studyCnt = studyCnt;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column drm_study_course.study_time
     *
     * @return the value of drm_study_course.study_time
     *
     * @mbggenerated Mon Sep 07 01:49:26 CST 2015
     */
    public Integer getStudyTime() {
        return studyTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column drm_study_course.study_time
     *
     * @param studyTime the value for drm_study_course.study_time
     *
     * @mbggenerated Mon Sep 07 01:49:26 CST 2015
     */
    public void setStudyTime(Integer studyTime) {
        this.studyTime = studyTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column drm_study_course.can_study
     *
     * @return the value of drm_study_course.can_study
     *
     * @mbggenerated Mon Sep 07 01:49:26 CST 2015
     */
    public Integer getCanStudy() {
        return canStudy;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column drm_study_course.can_study
     *
     * @param canStudy the value for drm_study_course.can_study
     *
     * @mbggenerated Mon Sep 07 01:49:26 CST 2015
     */
    public void setCanStudy(Integer canStudy) {
        this.canStudy = canStudy;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column drm_study_course.progress
     *
     * @return the value of drm_study_course.progress
     *
     * @mbggenerated Mon Sep 07 01:49:26 CST 2015
     */
    public Integer getProgress() {
        return progress;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column drm_study_course.progress
     *
     * @param progress the value for drm_study_course.progress
     *
     * @mbggenerated Mon Sep 07 01:49:26 CST 2015
     */
    public void setProgress(Integer progress) {
        this.progress = progress;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column drm_study_course.laststudy_dt
     *
     * @return the value of drm_study_course.laststudy_dt
     *
     * @mbggenerated Mon Sep 07 01:49:26 CST 2015
     */
    public Date getLaststudyDt() {
        return laststudyDt;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column drm_study_course.laststudy_dt
     *
     * @param laststudyDt the value for drm_study_course.laststudy_dt
     *
     * @mbggenerated Mon Sep 07 01:49:26 CST 2015
     */
    public void setLaststudyDt(Date laststudyDt) {
        this.laststudyDt = laststudyDt;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column drm_study_course.create_time
     *
     * @return the value of drm_study_course.create_time
     *
     * @mbggenerated Mon Sep 07 01:49:26 CST 2015
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column drm_study_course.create_time
     *
     * @param createTime the value for drm_study_course.create_time
     *
     * @mbggenerated Mon Sep 07 01:49:26 CST 2015
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column drm_study_course.remark
     *
     * @return the value of drm_study_course.remark
     *
     * @mbggenerated Mon Sep 07 01:49:26 CST 2015
     */
    public String getRemark() {
        return remark;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column drm_study_course.remark
     *
     * @param remark the value for drm_study_course.remark
     *
     * @mbggenerated Mon Sep 07 01:49:26 CST 2015
     */
    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }
}