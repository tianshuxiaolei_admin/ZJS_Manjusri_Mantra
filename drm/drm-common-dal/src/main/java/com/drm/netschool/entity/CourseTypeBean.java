package com.drm.netschool.entity;

import java.io.Serializable;
/**
 * @author chenxuezheng
 * 课程类型Bean
 * */
public class CourseTypeBean implements Serializable {
	private long id;
	private int courseClass;
	private String courseTp;
	private int seq;
	private int courseCnt;
	private int useYn;
	private Integer offCourseCnt;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public int getCourseClass() {
		return courseClass;
	}
	public void setCourseClass(int courseClass) {
		this.courseClass = courseClass;
	}
	public String getCourseTp() {
		return courseTp;
	}
	public void setCourseTp(String courseTp) {
		this.courseTp = courseTp;
	}
	public int getSeq() {
		return seq;
	}
	public void setSeq(int seq) {
		this.seq = seq;
	}
	public int getCourseCnt() {
		return courseCnt;
	}
	public void setCourseCnt(int courseCnt) {
		this.courseCnt = courseCnt;
	}
	public int getUseYn() {
		return useYn;
	}
	public void setUseYn(int useYn) {
		this.useYn = useYn;
	}
	public Integer getOffCourseCnt() {
		return offCourseCnt;
	}
	public void setOffCourseCnt(Integer offCourseCnt) {
		this.offCourseCnt = offCourseCnt;
	}
	
	
}
