package com.drm.netschool.entity;

import java.util.Date;

public class DrmBuyCart extends DrmBuyCartKey {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column drm_buy_cart.user_id
     *
     * @mbggenerated Thu Jul 23 23:06:59 CST 2015
     */
    private Long userId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column drm_buy_cart.cart_st
     *
     * @mbggenerated Thu Jul 23 23:06:59 CST 2015
     */
    private Integer cartSt;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column drm_buy_cart.put_time
     *
     * @mbggenerated Thu Jul 23 23:06:59 CST 2015
     */
    private Date putTime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column drm_buy_cart.cancel_dt
     *
     * @mbggenerated Thu Jul 23 23:06:59 CST 2015
     */
    private Date cancelDt;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column drm_buy_cart.pay_dt
     *
     * @mbggenerated Thu Jul 23 23:06:59 CST 2015
     */
    private Date payDt;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column drm_buy_cart.cart_tp
     *
     * @mbggenerated Thu Jul 23 23:06:59 CST 2015
     */
    private Integer cartTp;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column drm_buy_cart.user_id
     *
     * @return the value of drm_buy_cart.user_id
     *
     * @mbggenerated Thu Jul 23 23:06:59 CST 2015
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column drm_buy_cart.user_id
     *
     * @param userId the value for drm_buy_cart.user_id
     *
     * @mbggenerated Thu Jul 23 23:06:59 CST 2015
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column drm_buy_cart.cart_st
     *
     * @return the value of drm_buy_cart.cart_st
     *
     * @mbggenerated Thu Jul 23 23:06:59 CST 2015
     */
    public Integer getCartSt() {
        return cartSt;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column drm_buy_cart.cart_st
     *
     * @param cartSt the value for drm_buy_cart.cart_st
     *
     * @mbggenerated Thu Jul 23 23:06:59 CST 2015
     */
    public void setCartSt(Integer cartSt) {
        this.cartSt = cartSt;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column drm_buy_cart.put_time
     *
     * @return the value of drm_buy_cart.put_time
     *
     * @mbggenerated Thu Jul 23 23:06:59 CST 2015
     */
    public Date getPutTime() {
        return putTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column drm_buy_cart.put_time
     *
     * @param putTime the value for drm_buy_cart.put_time
     *
     * @mbggenerated Thu Jul 23 23:06:59 CST 2015
     */
    public void setPutTime(Date putTime) {
        this.putTime = putTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column drm_buy_cart.cancel_dt
     *
     * @return the value of drm_buy_cart.cancel_dt
     *
     * @mbggenerated Thu Jul 23 23:06:59 CST 2015
     */
    public Date getCancelDt() {
        return cancelDt;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column drm_buy_cart.cancel_dt
     *
     * @param cancelDt the value for drm_buy_cart.cancel_dt
     *
     * @mbggenerated Thu Jul 23 23:06:59 CST 2015
     */
    public void setCancelDt(Date cancelDt) {
        this.cancelDt = cancelDt;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column drm_buy_cart.pay_dt
     *
     * @return the value of drm_buy_cart.pay_dt
     *
     * @mbggenerated Thu Jul 23 23:06:59 CST 2015
     */
    public Date getPayDt() {
        return payDt;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column drm_buy_cart.pay_dt
     *
     * @param payDt the value for drm_buy_cart.pay_dt
     *
     * @mbggenerated Thu Jul 23 23:06:59 CST 2015
     */
    public void setPayDt(Date payDt) {
        this.payDt = payDt;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column drm_buy_cart.cart_tp
     *
     * @return the value of drm_buy_cart.cart_tp
     *
     * @mbggenerated Thu Jul 23 23:06:59 CST 2015
     */
    public Integer getCartTp() {
        return cartTp;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column drm_buy_cart.cart_tp
     *
     * @param cartTp the value for drm_buy_cart.cart_tp
     *
     * @mbggenerated Thu Jul 23 23:06:59 CST 2015
     */
    public void setCartTp(Integer cartTp) {
        this.cartTp = cartTp;
    }
}