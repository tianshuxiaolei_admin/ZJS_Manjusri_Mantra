package com.drm.netschool.entity.extend;

import java.util.Date;

public class CartInfo{
	
	private Long cartId;

	private Long userId;
	
	private Long courseId;

    private Integer cartSt;

    private Date putTime;

    private Date cancelDt;

    private Date payDt;

    private Integer cartTp;
    
    private String courseNm;
    
    private Long orgPrice;
    
    private Long curPrice;
    
    private Long courseTeacherid;

    private String courseTeachernm;
    
    private String coursePimg;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getCourseId() {
		return courseId;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}

	public Integer getCartSt() {
		return cartSt;
	}

	public void setCartSt(Integer cartSt) {
		this.cartSt = cartSt;
	}

	public Date getPutTime() {
		return putTime;
	}

	public void setPutTime(Date putTime) {
		this.putTime = putTime;
	}

	public Date getCancelDt() {
		return cancelDt;
	}

	public void setCancelDt(Date cancelDt) {
		this.cancelDt = cancelDt;
	}

	public Date getPayDt() {
		return payDt;
	}

	public void setPayDt(Date payDt) {
		this.payDt = payDt;
	}

	public Integer getCartTp() {
		return cartTp;
	}

	public void setCartTp(Integer cartTp) {
		this.cartTp = cartTp;
	}

	public String getCourseNm() {
		return courseNm;
	}

	public void setCourseNm(String courseNm) {
		this.courseNm = courseNm;
	}

	public Long getOrgPrice() {
		return orgPrice;
	}

	public void setOrgPrice(Long orgPrice) {
		this.orgPrice = orgPrice;
	}

	public Long getCurPrice() {
		return curPrice;
	}

	public void setCurPrice(Long curPrice) {
		this.curPrice = curPrice;
	}

	public Long getCourseTeacherid() {
		return courseTeacherid;
	}

	public void setCourseTeacherid(Long courseTeacherid) {
		this.courseTeacherid = courseTeacherid;
	}

	public String getCourseTeachernm() {
		return courseTeachernm;
	}

	public void setCourseTeachernm(String courseTeachernm) {
		this.courseTeachernm = courseTeachernm;
	}

	public String getCoursePimg() {
		return coursePimg;
	}

	public void setCoursePimg(String coursePimg) {
		this.coursePimg = coursePimg;
	}

	public Long getCartId() {
		return cartId;
	}

	public void setCartId(Long cartId) {
		this.cartId = cartId;
	}
}
