package com.drm.netschool.mapper.manual;

import java.util.List;
import java.util.Map;

import com.drm.netschool.entity.SumerSharedResource;
import com.drm.netschool.entity.extend.ResourceInfo;

public interface SumerSharedResourceExtendMapper {

	public List<SumerSharedResource> getListByPage(Map<String, Object> param);
	
	public int getCount(Map<String, Object> param);
	
	public List<ResourceInfo> getResourceInfoList(Map<String, Object> param);
	
	public int getResourceInfoCount(Map<String, Object> param);
	
	public ResourceInfo getResourceInfo(Map<String, Object> param);
	
	public List<ResourceInfo> getDownUserList(Map<String, Object> param);
	
	public int addDownCount(Map<String, Object> param);
	
	public int setDownWeekCount(Map<String, Object> param);
	
	public int addDownWeekCount(Map<String, Object> param);
	
	public int addShareDownrecord(Map<String, Object> param);
	
	
}
