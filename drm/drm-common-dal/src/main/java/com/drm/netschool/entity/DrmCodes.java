package com.drm.netschool.entity;

import java.util.Date;

public class DrmCodes {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column drm_codes.id
     *
     * @mbggenerated Sun Aug 09 22:37:53 CST 2015
     */
    private Integer id;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column drm_codes.cd
     *
     * @mbggenerated Sun Aug 09 22:37:53 CST 2015
     */
    private String cd;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column drm_codes.nm
     *
     * @mbggenerated Sun Aug 09 22:37:53 CST 2015
     */
    private String nm;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column drm_codes.code_gp
     *
     * @mbggenerated Sun Aug 09 22:37:53 CST 2015
     */
    private String codeGp;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column drm_codes.use_yn
     *
     * @mbggenerated Sun Aug 09 22:37:53 CST 2015
     */
    private Integer useYn;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column drm_codes.create_time
     *
     * @mbggenerated Sun Aug 09 22:37:53 CST 2015
     */
    private Date createTime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column drm_codes.desc
     *
     * @mbggenerated Sun Aug 09 22:37:53 CST 2015
     */
    private String desc;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column drm_codes.id
     *
     * @return the value of drm_codes.id
     *
     * @mbggenerated Sun Aug 09 22:37:53 CST 2015
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column drm_codes.id
     *
     * @param id the value for drm_codes.id
     *
     * @mbggenerated Sun Aug 09 22:37:53 CST 2015
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column drm_codes.cd
     *
     * @return the value of drm_codes.cd
     *
     * @mbggenerated Sun Aug 09 22:37:53 CST 2015
     */
    public String getCd() {
        return cd;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column drm_codes.cd
     *
     * @param cd the value for drm_codes.cd
     *
     * @mbggenerated Sun Aug 09 22:37:53 CST 2015
     */
    public void setCd(String cd) {
        this.cd = cd == null ? null : cd.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column drm_codes.nm
     *
     * @return the value of drm_codes.nm
     *
     * @mbggenerated Sun Aug 09 22:37:53 CST 2015
     */
    public String getNm() {
        return nm;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column drm_codes.nm
     *
     * @param nm the value for drm_codes.nm
     *
     * @mbggenerated Sun Aug 09 22:37:53 CST 2015
     */
    public void setNm(String nm) {
        this.nm = nm == null ? null : nm.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column drm_codes.code_gp
     *
     * @return the value of drm_codes.code_gp
     *
     * @mbggenerated Sun Aug 09 22:37:53 CST 2015
     */
    public String getCodeGp() {
        return codeGp;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column drm_codes.code_gp
     *
     * @param codeGp the value for drm_codes.code_gp
     *
     * @mbggenerated Sun Aug 09 22:37:53 CST 2015
     */
    public void setCodeGp(String codeGp) {
        this.codeGp = codeGp == null ? null : codeGp.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column drm_codes.use_yn
     *
     * @return the value of drm_codes.use_yn
     *
     * @mbggenerated Sun Aug 09 22:37:53 CST 2015
     */
    public Integer getUseYn() {
        return useYn;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column drm_codes.use_yn
     *
     * @param useYn the value for drm_codes.use_yn
     *
     * @mbggenerated Sun Aug 09 22:37:53 CST 2015
     */
    public void setUseYn(Integer useYn) {
        this.useYn = useYn;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column drm_codes.create_time
     *
     * @return the value of drm_codes.create_time
     *
     * @mbggenerated Sun Aug 09 22:37:53 CST 2015
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column drm_codes.create_time
     *
     * @param createTime the value for drm_codes.create_time
     *
     * @mbggenerated Sun Aug 09 22:37:53 CST 2015
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column drm_codes.desc
     *
     * @return the value of drm_codes.desc
     *
     * @mbggenerated Sun Aug 09 22:37:53 CST 2015
     */
    public String getDesc() {
        return desc;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column drm_codes.desc
     *
     * @param desc the value for drm_codes.desc
     *
     * @mbggenerated Sun Aug 09 22:37:53 CST 2015
     */
    public void setDesc(String desc) {
        this.desc = desc == null ? null : desc.trim();
    }
}