package com.drm.netschool.mapper.manual;

import java.util.List;
import java.util.Map;

import com.drm.netschool.entity.DrmUser;
import com.drm.netschool.entity.extend.UserInfo;

public interface DrmUserExtendMapper {
	
	public List<DrmUser> getListByPage(Map<String, Object> param);
	
	public int getCount(Map<String, Object> param);
    
	public List<UserInfo> getUserInfoList(Map<String, Object> param);
	
	public int getUserInfoCount(Map<String, Object> param);
	
	public int getSortUserInfoCount(Map<String, Object> param);
	
	public List<UserInfo> getSortUserInfoList(Map<String, Object> param);
	
	public List<UserInfo> getRecommendUserInfoList();
}