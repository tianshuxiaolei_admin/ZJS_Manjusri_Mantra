package com.drm.netschool.entity.extend;


/**
 * *************************************************************************
 * @文件名称: AddStudyCourseDTO.java
 *
 * @包路径  : com.drm.task.dto 
 *				 
 * @版权所有:   TSW   科技有限公司 (C) 2014
 *
 * @类描述:  分配课程或者支付后成功，添加课程
 * 
 * @创建人:   wangxiaolei  
 *
 * @创建时间: 2015年8月22日 - 上午9:16:43 
 *
 * @修改记录:
   -----------------------------------------------------------------------------------------------
             时间						|		修改人		|		修改的方法		|		修改描述                                                                
   -----------------------------------------------------------------------------------------------
							|					|					|                                       
   ----------------------------------------------------------------------------------------------- 	
 
 **************************************************************************
 */
public class AddStudyCourseDTO {

	/**
	 * 课程ID
	 */
	private Long  courseId;
	/**
	 * 分配给谁
	 */
	private Long  userId;
	/**
	 * 所有的描述，比如 谁 分配给谁  了一个课程，  时间是几点， 扣除多少，，等等这些说明
	 */
	private String desc;

	public Long getCourseId() {
		return courseId;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	
	
}
