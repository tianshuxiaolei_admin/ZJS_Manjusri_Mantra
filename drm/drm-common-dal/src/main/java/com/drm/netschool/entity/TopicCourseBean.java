package com.drm.netschool.entity;

import java.io.Serializable;
import java.util.Date;

public class TopicCourseBean implements Serializable {
	private Long courseId;
	private int studyCnt;
	private int studyTime;
	private int canStudy;
	private String userName;
	private String courseName;
	private String coursePimg;
	private int courseTimes;
	private Date coursePdt;
	private int courseLimitTp;
	private int courseLimit;
	private String courseSmart;
	private String courseDesc;
	private String courseSubject;
	private Date courseCdt;
	private String courseTeachernm;
	private int courseCredit;
	private int progress;
	private String userHimg;
	
	

	public String getUserHimg() {
		return userHimg;
	}

	public void setUserHimg(String userHimg) {
		this.userHimg = userHimg;
	}

	public Long getCourseId() {
		return courseId;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}

	public int getStudyCnt() {
		return studyCnt;
	}

	public void setStudyCnt(int studyCnt) {
		this.studyCnt = studyCnt;
	}

	public int getStudyTime() {
		return studyTime;
	}

	public void setStudyTime(int studyTime) {
		this.studyTime = studyTime;
	}

	public int getCanStudy() {
		return canStudy;
	}

	public void setCanStudy(int canStudy) {
		this.canStudy = canStudy;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getCoursePimg() {
		return coursePimg;
	}

	public void setCoursePimg(String coursePimg) {
		this.coursePimg = coursePimg;
	}

	public int getCourseTimes() {
		return courseTimes;
	}

	public void setCourseTimes(int courseTimes) {
		this.courseTimes = courseTimes;
	}

	public Date getCoursePdt() {
		return coursePdt;
	}

	public void setCoursePdt(Date coursePdt) {
		this.coursePdt = coursePdt;
	}

	public int getCourseLimitTp() {
		return courseLimitTp;
	}

	public void setCourseLimitTp(int courseLimitTp) {
		this.courseLimitTp = courseLimitTp;
	}

	public int getCourseLimit() {
		return courseLimit;
	}

	public void setCourseLimit(int courseLimit) {
		this.courseLimit = courseLimit;
	}

	public String getCourseSmart() {
		return courseSmart;
	}

	public void setCourseSmart(String courseSmart) {
		this.courseSmart = courseSmart;
	}

	public String getCourseDesc() {
		return courseDesc;
	}

	public void setCourseDesc(String courseDesc) {
		this.courseDesc = courseDesc;
	}

	public String getCourseSubject() {
		return courseSubject;
	}

	public void setCourseSubject(String courseSubject) {
		this.courseSubject = courseSubject;
	}

	public Date getCourseCdt() {
		return courseCdt;
	}

	public void setCourseCdt(Date courseCdt) {
		this.courseCdt = courseCdt;
	}

	public String getCourseTeachernm() {
		return courseTeachernm;
	}

	public void setCourseTeachernm(String courseTeachernm) {
		this.courseTeachernm = courseTeachernm;
	}

	public int getCourseCredit() {
		return courseCredit;
	}

	public void setCourseCredit(int courseCredit) {
		this.courseCredit = courseCredit;
	}

	public int getProgress() {
		return progress;
	}

	public void setProgress(int progress) {
		this.progress = progress;
	}
}
