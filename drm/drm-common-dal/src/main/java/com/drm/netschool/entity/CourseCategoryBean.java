package com.drm.netschool.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * @author chenxuezheng
 * 课程分类Bean
 * */
public class CourseCategoryBean implements Serializable {
	private int seq;
	private Long courseId;
	private Date createDate;
	private Long id;
	private int categoryTp;
	private String iconClass;
	private String categorySourceid;
	private String  categoryName;
	//每个课件的时常
	private int		courseTimes;
	 
      
      
	
	public int getCourseTimes() {
		return courseTimes;
	}
	public void setCourseTimes(int courseTimes) {
		this.courseTimes = courseTimes;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public int getSeq() {
		return seq;
	}
	public void setSeq(int seq) {
		this.seq = seq;
	}
	public Long getCourseId() {
		return courseId;
	}
	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public int getCategoryTp() {
		return categoryTp;
	}
	public void setCategoryTp(int categoryTp) {
		this.categoryTp = categoryTp;
	}
	public String getIconClass() {
		return iconClass;
	}
	public void setIconClass(String iconClass) {
		this.iconClass = iconClass;
	}
	public String getCategorySourceid() {
		return categorySourceid;
	}
	public void setCategorySourceid(String categorySourceid) {
		this.categorySourceid = categorySourceid;
	}

	
}
