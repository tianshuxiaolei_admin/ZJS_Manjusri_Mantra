package com.drm.netschool.mongo;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Repository;

import com.drm.common.util.StringUtil;
import com.drm.netschool.entity.AticleMongoDetailContent;
 
/**
 * *************************************************************************
 * @文件名称: MongoBaseDao.java
 *
 * @包路径  : com.drm.netschool.mongo 
 *				 
 * @版权所有:   TSW   科技有限公司 (C) 2014
 *
 * @类描述:   根据ID获取详情
 * 
 * @创建人:   wangxiaolei  
 *
 * @创建时间: 2015年8月26日 - 下午11:20:53 
 *
 * @修改记录:
   -----------------------------------------------------------------------------------------------
             时间						|		修改人		|		修改的方法		|		修改描述                                                                
   -----------------------------------------------------------------------------------------------
							|					|					|                                       
   ----------------------------------------------------------------------------------------------- 	
 
 **************************************************************************
 */
@Repository
public   class MongoDao  {
	private final Log log = LogFactory.getLog(MongoDao.class);

	@Autowired
	private MongoOperations mongoTemplate;

	public MongoDao(MongoOperations mongoTemplate) {
		this.mongoTemplate = mongoTemplate;
	}

	public AticleMongoDetailContent findById(String id) {
		log.info("load id =" + id);
		return mongoTemplate.findById(id, AticleMongoDetailContent.class);
	}

	public String getContentById(String id) {
		AticleMongoDetailContent content = findById(id);
		if (content != null && content.getDetailContent() != null) {
			return content.getDetailContent();
		}
		return content == null || StringUtil.empty(content.getDetailContent()) ? "" : content.getDetailContent();
	}
}
