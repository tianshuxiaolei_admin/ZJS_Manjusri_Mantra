package com.drm.netschool.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DrmSystemSettingExample {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table drm_system_setting
     *
     * @mbggenerated Sat Aug 29 16:23:09 CST 2015
     */
    protected String orderByClause;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table drm_system_setting
     *
     * @mbggenerated Sat Aug 29 16:23:09 CST 2015
     */
    protected boolean distinct;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table drm_system_setting
     *
     * @mbggenerated Sat Aug 29 16:23:09 CST 2015
     */
    protected List<Criteria> oredCriteria;

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table drm_system_setting
     *
     * @mbggenerated Sat Aug 29 16:23:09 CST 2015
     */
    public DrmSystemSettingExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table drm_system_setting
     *
     * @mbggenerated Sat Aug 29 16:23:09 CST 2015
     */
    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table drm_system_setting
     *
     * @mbggenerated Sat Aug 29 16:23:09 CST 2015
     */
    public String getOrderByClause() {
        return orderByClause;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table drm_system_setting
     *
     * @mbggenerated Sat Aug 29 16:23:09 CST 2015
     */
    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table drm_system_setting
     *
     * @mbggenerated Sat Aug 29 16:23:09 CST 2015
     */
    public boolean isDistinct() {
        return distinct;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table drm_system_setting
     *
     * @mbggenerated Sat Aug 29 16:23:09 CST 2015
     */
    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table drm_system_setting
     *
     * @mbggenerated Sat Aug 29 16:23:09 CST 2015
     */
    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table drm_system_setting
     *
     * @mbggenerated Sat Aug 29 16:23:09 CST 2015
     */
    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table drm_system_setting
     *
     * @mbggenerated Sat Aug 29 16:23:09 CST 2015
     */
    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table drm_system_setting
     *
     * @mbggenerated Sat Aug 29 16:23:09 CST 2015
     */
    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table drm_system_setting
     *
     * @mbggenerated Sat Aug 29 16:23:09 CST 2015
     */
    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table drm_system_setting
     *
     * @mbggenerated Sat Aug 29 16:23:09 CST 2015
     */
    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andWebsiteNameIsNull() {
            addCriterion("website_name is null");
            return (Criteria) this;
        }

        public Criteria andWebsiteNameIsNotNull() {
            addCriterion("website_name is not null");
            return (Criteria) this;
        }

        public Criteria andWebsiteNameEqualTo(String value) {
            addCriterion("website_name =", value, "websiteName");
            return (Criteria) this;
        }

        public Criteria andWebsiteNameNotEqualTo(String value) {
            addCriterion("website_name <>", value, "websiteName");
            return (Criteria) this;
        }

        public Criteria andWebsiteNameGreaterThan(String value) {
            addCriterion("website_name >", value, "websiteName");
            return (Criteria) this;
        }

        public Criteria andWebsiteNameGreaterThanOrEqualTo(String value) {
            addCriterion("website_name >=", value, "websiteName");
            return (Criteria) this;
        }

        public Criteria andWebsiteNameLessThan(String value) {
            addCriterion("website_name <", value, "websiteName");
            return (Criteria) this;
        }

        public Criteria andWebsiteNameLessThanOrEqualTo(String value) {
            addCriterion("website_name <=", value, "websiteName");
            return (Criteria) this;
        }

        public Criteria andWebsiteNameLike(String value) {
            addCriterion("website_name like", value, "websiteName");
            return (Criteria) this;
        }

        public Criteria andWebsiteNameNotLike(String value) {
            addCriterion("website_name not like", value, "websiteName");
            return (Criteria) this;
        }

        public Criteria andWebsiteNameIn(List<String> values) {
            addCriterion("website_name in", values, "websiteName");
            return (Criteria) this;
        }

        public Criteria andWebsiteNameNotIn(List<String> values) {
            addCriterion("website_name not in", values, "websiteName");
            return (Criteria) this;
        }

        public Criteria andWebsiteNameBetween(String value1, String value2) {
            addCriterion("website_name between", value1, value2, "websiteName");
            return (Criteria) this;
        }

        public Criteria andWebsiteNameNotBetween(String value1, String value2) {
            addCriterion("website_name not between", value1, value2, "websiteName");
            return (Criteria) this;
        }

        public Criteria andWebsiteIcoIsNull() {
            addCriterion("website_ico is null");
            return (Criteria) this;
        }

        public Criteria andWebsiteIcoIsNotNull() {
            addCriterion("website_ico is not null");
            return (Criteria) this;
        }

        public Criteria andWebsiteIcoEqualTo(String value) {
            addCriterion("website_ico =", value, "websiteIco");
            return (Criteria) this;
        }

        public Criteria andWebsiteIcoNotEqualTo(String value) {
            addCriterion("website_ico <>", value, "websiteIco");
            return (Criteria) this;
        }

        public Criteria andWebsiteIcoGreaterThan(String value) {
            addCriterion("website_ico >", value, "websiteIco");
            return (Criteria) this;
        }

        public Criteria andWebsiteIcoGreaterThanOrEqualTo(String value) {
            addCriterion("website_ico >=", value, "websiteIco");
            return (Criteria) this;
        }

        public Criteria andWebsiteIcoLessThan(String value) {
            addCriterion("website_ico <", value, "websiteIco");
            return (Criteria) this;
        }

        public Criteria andWebsiteIcoLessThanOrEqualTo(String value) {
            addCriterion("website_ico <=", value, "websiteIco");
            return (Criteria) this;
        }

        public Criteria andWebsiteIcoLike(String value) {
            addCriterion("website_ico like", value, "websiteIco");
            return (Criteria) this;
        }

        public Criteria andWebsiteIcoNotLike(String value) {
            addCriterion("website_ico not like", value, "websiteIco");
            return (Criteria) this;
        }

        public Criteria andWebsiteIcoIn(List<String> values) {
            addCriterion("website_ico in", values, "websiteIco");
            return (Criteria) this;
        }

        public Criteria andWebsiteIcoNotIn(List<String> values) {
            addCriterion("website_ico not in", values, "websiteIco");
            return (Criteria) this;
        }

        public Criteria andWebsiteIcoBetween(String value1, String value2) {
            addCriterion("website_ico between", value1, value2, "websiteIco");
            return (Criteria) this;
        }

        public Criteria andWebsiteIcoNotBetween(String value1, String value2) {
            addCriterion("website_ico not between", value1, value2, "websiteIco");
            return (Criteria) this;
        }

        public Criteria andWebsiteMetaIsNull() {
            addCriterion("website_meta is null");
            return (Criteria) this;
        }

        public Criteria andWebsiteMetaIsNotNull() {
            addCriterion("website_meta is not null");
            return (Criteria) this;
        }

        public Criteria andWebsiteMetaEqualTo(String value) {
            addCriterion("website_meta =", value, "websiteMeta");
            return (Criteria) this;
        }

        public Criteria andWebsiteMetaNotEqualTo(String value) {
            addCriterion("website_meta <>", value, "websiteMeta");
            return (Criteria) this;
        }

        public Criteria andWebsiteMetaGreaterThan(String value) {
            addCriterion("website_meta >", value, "websiteMeta");
            return (Criteria) this;
        }

        public Criteria andWebsiteMetaGreaterThanOrEqualTo(String value) {
            addCriterion("website_meta >=", value, "websiteMeta");
            return (Criteria) this;
        }

        public Criteria andWebsiteMetaLessThan(String value) {
            addCriterion("website_meta <", value, "websiteMeta");
            return (Criteria) this;
        }

        public Criteria andWebsiteMetaLessThanOrEqualTo(String value) {
            addCriterion("website_meta <=", value, "websiteMeta");
            return (Criteria) this;
        }

        public Criteria andWebsiteMetaLike(String value) {
            addCriterion("website_meta like", value, "websiteMeta");
            return (Criteria) this;
        }

        public Criteria andWebsiteMetaNotLike(String value) {
            addCriterion("website_meta not like", value, "websiteMeta");
            return (Criteria) this;
        }

        public Criteria andWebsiteMetaIn(List<String> values) {
            addCriterion("website_meta in", values, "websiteMeta");
            return (Criteria) this;
        }

        public Criteria andWebsiteMetaNotIn(List<String> values) {
            addCriterion("website_meta not in", values, "websiteMeta");
            return (Criteria) this;
        }

        public Criteria andWebsiteMetaBetween(String value1, String value2) {
            addCriterion("website_meta between", value1, value2, "websiteMeta");
            return (Criteria) this;
        }

        public Criteria andWebsiteMetaNotBetween(String value1, String value2) {
            addCriterion("website_meta not between", value1, value2, "websiteMeta");
            return (Criteria) this;
        }

        public Criteria andWesiteMetadescIsNull() {
            addCriterion("wesite_metadesc is null");
            return (Criteria) this;
        }

        public Criteria andWesiteMetadescIsNotNull() {
            addCriterion("wesite_metadesc is not null");
            return (Criteria) this;
        }

        public Criteria andWesiteMetadescEqualTo(String value) {
            addCriterion("wesite_metadesc =", value, "wesiteMetadesc");
            return (Criteria) this;
        }

        public Criteria andWesiteMetadescNotEqualTo(String value) {
            addCriterion("wesite_metadesc <>", value, "wesiteMetadesc");
            return (Criteria) this;
        }

        public Criteria andWesiteMetadescGreaterThan(String value) {
            addCriterion("wesite_metadesc >", value, "wesiteMetadesc");
            return (Criteria) this;
        }

        public Criteria andWesiteMetadescGreaterThanOrEqualTo(String value) {
            addCriterion("wesite_metadesc >=", value, "wesiteMetadesc");
            return (Criteria) this;
        }

        public Criteria andWesiteMetadescLessThan(String value) {
            addCriterion("wesite_metadesc <", value, "wesiteMetadesc");
            return (Criteria) this;
        }

        public Criteria andWesiteMetadescLessThanOrEqualTo(String value) {
            addCriterion("wesite_metadesc <=", value, "wesiteMetadesc");
            return (Criteria) this;
        }

        public Criteria andWesiteMetadescLike(String value) {
            addCriterion("wesite_metadesc like", value, "wesiteMetadesc");
            return (Criteria) this;
        }

        public Criteria andWesiteMetadescNotLike(String value) {
            addCriterion("wesite_metadesc not like", value, "wesiteMetadesc");
            return (Criteria) this;
        }

        public Criteria andWesiteMetadescIn(List<String> values) {
            addCriterion("wesite_metadesc in", values, "wesiteMetadesc");
            return (Criteria) this;
        }

        public Criteria andWesiteMetadescNotIn(List<String> values) {
            addCriterion("wesite_metadesc not in", values, "wesiteMetadesc");
            return (Criteria) this;
        }

        public Criteria andWesiteMetadescBetween(String value1, String value2) {
            addCriterion("wesite_metadesc between", value1, value2, "wesiteMetadesc");
            return (Criteria) this;
        }

        public Criteria andWesiteMetadescNotBetween(String value1, String value2) {
            addCriterion("wesite_metadesc not between", value1, value2, "wesiteMetadesc");
            return (Criteria) this;
        }

        public Criteria andUpdateDtIsNull() {
            addCriterion("update_dt is null");
            return (Criteria) this;
        }

        public Criteria andUpdateDtIsNotNull() {
            addCriterion("update_dt is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateDtEqualTo(Date value) {
            addCriterion("update_dt =", value, "updateDt");
            return (Criteria) this;
        }

        public Criteria andUpdateDtNotEqualTo(Date value) {
            addCriterion("update_dt <>", value, "updateDt");
            return (Criteria) this;
        }

        public Criteria andUpdateDtGreaterThan(Date value) {
            addCriterion("update_dt >", value, "updateDt");
            return (Criteria) this;
        }

        public Criteria andUpdateDtGreaterThanOrEqualTo(Date value) {
            addCriterion("update_dt >=", value, "updateDt");
            return (Criteria) this;
        }

        public Criteria andUpdateDtLessThan(Date value) {
            addCriterion("update_dt <", value, "updateDt");
            return (Criteria) this;
        }

        public Criteria andUpdateDtLessThanOrEqualTo(Date value) {
            addCriterion("update_dt <=", value, "updateDt");
            return (Criteria) this;
        }

        public Criteria andUpdateDtIn(List<Date> values) {
            addCriterion("update_dt in", values, "updateDt");
            return (Criteria) this;
        }

        public Criteria andUpdateDtNotIn(List<Date> values) {
            addCriterion("update_dt not in", values, "updateDt");
            return (Criteria) this;
        }

        public Criteria andUpdateDtBetween(Date value1, Date value2) {
            addCriterion("update_dt between", value1, value2, "updateDt");
            return (Criteria) this;
        }

        public Criteria andUpdateDtNotBetween(Date value1, Date value2) {
            addCriterion("update_dt not between", value1, value2, "updateDt");
            return (Criteria) this;
        }

        public Criteria andUpdateIdIsNull() {
            addCriterion("update_id is null");
            return (Criteria) this;
        }

        public Criteria andUpdateIdIsNotNull() {
            addCriterion("update_id is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateIdEqualTo(Long value) {
            addCriterion("update_id =", value, "updateId");
            return (Criteria) this;
        }

        public Criteria andUpdateIdNotEqualTo(Long value) {
            addCriterion("update_id <>", value, "updateId");
            return (Criteria) this;
        }

        public Criteria andUpdateIdGreaterThan(Long value) {
            addCriterion("update_id >", value, "updateId");
            return (Criteria) this;
        }

        public Criteria andUpdateIdGreaterThanOrEqualTo(Long value) {
            addCriterion("update_id >=", value, "updateId");
            return (Criteria) this;
        }

        public Criteria andUpdateIdLessThan(Long value) {
            addCriterion("update_id <", value, "updateId");
            return (Criteria) this;
        }

        public Criteria andUpdateIdLessThanOrEqualTo(Long value) {
            addCriterion("update_id <=", value, "updateId");
            return (Criteria) this;
        }

        public Criteria andUpdateIdIn(List<Long> values) {
            addCriterion("update_id in", values, "updateId");
            return (Criteria) this;
        }

        public Criteria andUpdateIdNotIn(List<Long> values) {
            addCriterion("update_id not in", values, "updateId");
            return (Criteria) this;
        }

        public Criteria andUpdateIdBetween(Long value1, Long value2) {
            addCriterion("update_id between", value1, value2, "updateId");
            return (Criteria) this;
        }

        public Criteria andUpdateIdNotBetween(Long value1, Long value2) {
            addCriterion("update_id not between", value1, value2, "updateId");
            return (Criteria) this;
        }

        public Criteria andWebsiteUrlIsNull() {
            addCriterion("website_url is null");
            return (Criteria) this;
        }

        public Criteria andWebsiteUrlIsNotNull() {
            addCriterion("website_url is not null");
            return (Criteria) this;
        }

        public Criteria andWebsiteUrlEqualTo(String value) {
            addCriterion("website_url =", value, "websiteUrl");
            return (Criteria) this;
        }

        public Criteria andWebsiteUrlNotEqualTo(String value) {
            addCriterion("website_url <>", value, "websiteUrl");
            return (Criteria) this;
        }

        public Criteria andWebsiteUrlGreaterThan(String value) {
            addCriterion("website_url >", value, "websiteUrl");
            return (Criteria) this;
        }

        public Criteria andWebsiteUrlGreaterThanOrEqualTo(String value) {
            addCriterion("website_url >=", value, "websiteUrl");
            return (Criteria) this;
        }

        public Criteria andWebsiteUrlLessThan(String value) {
            addCriterion("website_url <", value, "websiteUrl");
            return (Criteria) this;
        }

        public Criteria andWebsiteUrlLessThanOrEqualTo(String value) {
            addCriterion("website_url <=", value, "websiteUrl");
            return (Criteria) this;
        }

        public Criteria andWebsiteUrlLike(String value) {
            addCriterion("website_url like", value, "websiteUrl");
            return (Criteria) this;
        }

        public Criteria andWebsiteUrlNotLike(String value) {
            addCriterion("website_url not like", value, "websiteUrl");
            return (Criteria) this;
        }

        public Criteria andWebsiteUrlIn(List<String> values) {
            addCriterion("website_url in", values, "websiteUrl");
            return (Criteria) this;
        }

        public Criteria andWebsiteUrlNotIn(List<String> values) {
            addCriterion("website_url not in", values, "websiteUrl");
            return (Criteria) this;
        }

        public Criteria andWebsiteUrlBetween(String value1, String value2) {
            addCriterion("website_url between", value1, value2, "websiteUrl");
            return (Criteria) this;
        }

        public Criteria andWebsiteUrlNotBetween(String value1, String value2) {
            addCriterion("website_url not between", value1, value2, "websiteUrl");
            return (Criteria) this;
        }

        public Criteria andWebsiteLogoIsNull() {
            addCriterion("website_logo is null");
            return (Criteria) this;
        }

        public Criteria andWebsiteLogoIsNotNull() {
            addCriterion("website_logo is not null");
            return (Criteria) this;
        }

        public Criteria andWebsiteLogoEqualTo(String value) {
            addCriterion("website_logo =", value, "websiteLogo");
            return (Criteria) this;
        }

        public Criteria andWebsiteLogoNotEqualTo(String value) {
            addCriterion("website_logo <>", value, "websiteLogo");
            return (Criteria) this;
        }

        public Criteria andWebsiteLogoGreaterThan(String value) {
            addCriterion("website_logo >", value, "websiteLogo");
            return (Criteria) this;
        }

        public Criteria andWebsiteLogoGreaterThanOrEqualTo(String value) {
            addCriterion("website_logo >=", value, "websiteLogo");
            return (Criteria) this;
        }

        public Criteria andWebsiteLogoLessThan(String value) {
            addCriterion("website_logo <", value, "websiteLogo");
            return (Criteria) this;
        }

        public Criteria andWebsiteLogoLessThanOrEqualTo(String value) {
            addCriterion("website_logo <=", value, "websiteLogo");
            return (Criteria) this;
        }

        public Criteria andWebsiteLogoLike(String value) {
            addCriterion("website_logo like", value, "websiteLogo");
            return (Criteria) this;
        }

        public Criteria andWebsiteLogoNotLike(String value) {
            addCriterion("website_logo not like", value, "websiteLogo");
            return (Criteria) this;
        }

        public Criteria andWebsiteLogoIn(List<String> values) {
            addCriterion("website_logo in", values, "websiteLogo");
            return (Criteria) this;
        }

        public Criteria andWebsiteLogoNotIn(List<String> values) {
            addCriterion("website_logo not in", values, "websiteLogo");
            return (Criteria) this;
        }

        public Criteria andWebsiteLogoBetween(String value1, String value2) {
            addCriterion("website_logo between", value1, value2, "websiteLogo");
            return (Criteria) this;
        }

        public Criteria andWebsiteLogoNotBetween(String value1, String value2) {
            addCriterion("website_logo not between", value1, value2, "websiteLogo");
            return (Criteria) this;
        }

        public Criteria andWebsiteTitleIsNull() {
            addCriterion("website_title is null");
            return (Criteria) this;
        }

        public Criteria andWebsiteTitleIsNotNull() {
            addCriterion("website_title is not null");
            return (Criteria) this;
        }

        public Criteria andWebsiteTitleEqualTo(String value) {
            addCriterion("website_title =", value, "websiteTitle");
            return (Criteria) this;
        }

        public Criteria andWebsiteTitleNotEqualTo(String value) {
            addCriterion("website_title <>", value, "websiteTitle");
            return (Criteria) this;
        }

        public Criteria andWebsiteTitleGreaterThan(String value) {
            addCriterion("website_title >", value, "websiteTitle");
            return (Criteria) this;
        }

        public Criteria andWebsiteTitleGreaterThanOrEqualTo(String value) {
            addCriterion("website_title >=", value, "websiteTitle");
            return (Criteria) this;
        }

        public Criteria andWebsiteTitleLessThan(String value) {
            addCriterion("website_title <", value, "websiteTitle");
            return (Criteria) this;
        }

        public Criteria andWebsiteTitleLessThanOrEqualTo(String value) {
            addCriterion("website_title <=", value, "websiteTitle");
            return (Criteria) this;
        }

        public Criteria andWebsiteTitleLike(String value) {
            addCriterion("website_title like", value, "websiteTitle");
            return (Criteria) this;
        }

        public Criteria andWebsiteTitleNotLike(String value) {
            addCriterion("website_title not like", value, "websiteTitle");
            return (Criteria) this;
        }

        public Criteria andWebsiteTitleIn(List<String> values) {
            addCriterion("website_title in", values, "websiteTitle");
            return (Criteria) this;
        }

        public Criteria andWebsiteTitleNotIn(List<String> values) {
            addCriterion("website_title not in", values, "websiteTitle");
            return (Criteria) this;
        }

        public Criteria andWebsiteTitleBetween(String value1, String value2) {
            addCriterion("website_title between", value1, value2, "websiteTitle");
            return (Criteria) this;
        }

        public Criteria andWebsiteTitleNotBetween(String value1, String value2) {
            addCriterion("website_title not between", value1, value2, "websiteTitle");
            return (Criteria) this;
        }
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table drm_system_setting
     *
     * @mbggenerated do_not_delete_during_merge Sat Aug 29 16:23:09 CST 2015
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table drm_system_setting
     *
     * @mbggenerated Sat Aug 29 16:23:09 CST 2015
     */
    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}