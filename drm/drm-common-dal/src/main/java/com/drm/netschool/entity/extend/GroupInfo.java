package com.drm.netschool.entity.extend;

import java.util.Date;

public class GroupInfo {
	
	private long userId;
	private long groupId;
	private String groupNm;
	private String groupTxt;
	private String groupImg;
	private long createId;
	private String userRnm;
	private Date createDt;
	private int merberCnt;
	private int topicCnt;
	private int activityCnt;
	
	private String orderBy;
	
	public long getGroupId() {
		return groupId;
	}
	public void setGroupId(long groupId) {
		this.groupId = groupId;
	}
	public String getGroupNm() {
		return groupNm;
	}
	public void setGroupNm(String groupNm) {
		this.groupNm = groupNm;
	}
	public String getGroupTxt() {
		return groupTxt;
	}
	public void setGroupTxt(String groupTxt) {
		this.groupTxt = groupTxt;
	}
	public String getGroupImg() {
		return groupImg;
	}
	public void setGroupImg(String groupImg) {
		this.groupImg = groupImg;
	}
	public long getCreateId() {
		return createId;
	}
	public void setCreateId(long createId) {
		this.createId = createId;
	}
	public String getUserRnm() {
		return userRnm;
	}
	public void setUserRnm(String userRnm) {
		this.userRnm = userRnm;
	}
	public Date getCreateDt() {
		return createDt;
	}
	public void setCreateDt(Date createDt) {
		this.createDt = createDt;
	}
	public int getMerberCnt() {
		return merberCnt;
	}
	public void setMerberCnt(int merberCnt) {
		this.merberCnt = merberCnt;
	}
	public int getTopicCnt() {
		return topicCnt;
	}
	public void setTopicCnt(int topicCnt) {
		this.topicCnt = topicCnt;
	}
	public int getActivityCnt() {
		return activityCnt;
	}
	public void setActivityCnt(int activityCnt) {
		this.activityCnt = activityCnt;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public String getOrderBy() {
		return orderBy;
	}
	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}
}
