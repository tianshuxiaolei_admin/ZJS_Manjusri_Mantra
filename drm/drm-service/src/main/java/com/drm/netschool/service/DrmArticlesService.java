package com.drm.netschool.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.drm.common.util.DateUtil;
import com.drm.netschool.entity.DrmArticles;
import com.drm.netschool.mapper.DrmArticlesMapper;
import com.drm.netschool.mapper.manual.DrmArticlesExtendMapper;

@Service
public class DrmArticlesService {
	
	private Logger LOGGER = LoggerFactory.getLogger(DrmArticlesService.class);
	
	@Autowired
	private DrmArticlesMapper articlesMapper;
	
	@Autowired
	private DrmArticlesExtendMapper extendMapper;
	
	public DrmArticles getById(long id){
		return articlesMapper.selectByPrimaryKey(id);
	}
	
	public void update(DrmArticles entity){
		entity.setArticleUdt(DateUtil.current());
		articlesMapper.updateByPrimaryKey(entity);
	}
	
	public int getCount(DrmArticles entity){
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("param", entity);
		return extendMapper.getCount(param);
	}
	
	public List<DrmArticles> getListByPage(DrmArticles entity, Integer page, Integer pageSize){
		int start = (page - 1) * pageSize;
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("param", entity);
		param.put("offset", start);
		param.put("limit", pageSize);
		return extendMapper.getListByPage(param);
	}
}
