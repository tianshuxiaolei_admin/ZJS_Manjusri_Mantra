package com.drm.netschool.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.drm.common.util.DateUtil;
import com.drm.common.util.EncryptTool;
import com.drm.common.util.JsonMapper;
import com.drm.common.util.MD5Util;
import com.drm.common.util.StringUtil;
import com.drm.netschool.entity.DrmUser;
import com.drm.netschool.redis.RedisUtil;
import com.drm.netschool.service.model.SessionBean;
import com.drm.netschool.service.model.User;

@Service
public class DrmUserCacheService {

	private Logger LOGGER = LoggerFactory.getLogger(DrmUserCacheService.class);

	@Autowired
	private RedisUtil redisUtil;

	public String setUserCache(DrmUser user) {
		SessionBean bean = new SessionBean();
		// 设置登陆时间
		bean.setLoginTime(DateUtil.current().getTime());
		bean.setUser(drmUser2User(user));
		String key = "";
		try { key = MD5Util.digest(EncryptTool.sha1HEX(user.getUserId() + "")); } catch (Exception e) { e.printStackTrace(); }

		String strSession = JsonMapper.toJson(bean);
		redisUtil.hashSet(SessionBean.USER_SESSION_KEY_PERFIX + key, SessionBean.USER_SESSION_SUBKEY_SESSION, strSession, bean.getSaveTempTimes());
		LOGGER.info(strSession + " userid=" + user.getUserId() + "exiperse=" + bean.getSaveTempTimes() + "s!");
		return key;
	}

	public DrmUser getUserCache(String key) {
		if(StringUtil.empty(key))
			return null;
		
		String json = redisUtil.hashGet(SessionBean.USER_SESSION_KEY_PERFIX + key, SessionBean.USER_SESSION_SUBKEY_SESSION);
		if (StringUtil.empty(json))
			return null;
		SessionBean bean = JsonMapper.toObject(json, SessionBean.class);
		if (bean == null)
			return null;
		return user2DrmUser(bean.getUser());
	}
	
	public void delUserCache(String key){
		if(!StringUtil.empty(key))
			redisUtil.delKey(key);
	}
	
	private DrmUser user2DrmUser(User user){
		DrmUser entity = new DrmUser();
		entity.setUserId(user.getUserId());
		entity.setUserLnm(user.getLoginName());
		entity.setUserRnm(user.getUserName());
		entity.setUserSalt(user.getPwdsalt());
		entity.setUserIdcard(user.getIdentifierId());
		entity.setUserTel(user.getTelNumber());
		entity.setUserEmail(user.getEmailAddr());
		entity.setUserDesc(user.getUser_desc());
		entity.setUserCdt(user.getCreateTime());
//		entity.setUserUid(userUid);
		entity.setUserUdt(user.getUser_udt());
		entity.setUserCid(user.getUser_cid());
		entity.setUserHimg(user.getUser_himg());
		entity.setUserUyn(user.getUser_uyn());
		entity.setUserSource(user.getUser_source());
		entity.setUserQq(user.getUser_qq());
		entity.setUserWeibo(user.getUser_weibo());
		entity.setUserSourceid(user.getUser_sourceid());
		entity.setUserEdu(user.getUser_edu());
		entity.setUserSchool(user.getUser_school());
		entity.setUserMajor(user.getUser_major());
		entity.setUserMale(user.getGender());
		entity.setUserBthd(user.getBirthDate());
		entity.setUserProv(user.getProvinceCode());
		entity.setUserProvNm(user.getProvinceName());
		entity.setUserCity(user.getCityCode());
		entity.setUserCityNm(user.getCityName());
		entity.setUserAvtive(user.getUserAvtive());
		entity.setUserSing(user.getPersonal_sig());
		entity.setUserLock(user.getUser_lock());
		entity.setUserMobile(user.getUser_mobile());
		entity.setUserLockTime(user.getUser_lock_time());
		entity.setUserTp(user.getUserType());
		return entity;
	}
	
	private User drmUser2User(DrmUser entity){
		User user = new User();
		user.setUserId(entity.getUserId());
		user.setLoginName(entity.getUserLnm());
		user.setUserName(entity.getUserRnm());
		user.setPwdsalt(entity.getUserSalt());
		user.setIdentifierId(entity.getUserIdcard());
		user.setTelNumber(entity.getUserTel());
		user.setEmailAddr(entity.getUserEmail());
		user.setUser_desc(entity.getUserDesc());
		user.setCreateTime(entity.getUserCdt());
//		entity.setUserUid(userUid);
		user.setUser_udt(entity.getUserUdt());
		user.setUser_cid(entity.getUserCid());
		user.setUser_himg(entity.getUserHimg());
		user.setUser_uyn(entity.getUserUyn());
		user.setUser_source(entity.getUserSource());
		user.setUser_qq(entity.getUserQq());
		user.setUser_weibo(entity.getUserWeibo());
		user.setUser_sourceid(entity.getUserSourceid());
		user.setUser_edu(entity.getUserEdu());
		user.setUser_school(entity.getUserSchool());
		user.setUser_major(entity.getUserMajor());
		user.setGender(entity.getUserMale() == null ? 1 : entity.getUserMale());
		user.setBirthDate(entity.getUserBthd());
		user.setProvinceCode(entity.getUserProv());
		user.setProvinceName(entity.getUserProvNm());
		user.setCityCode(entity.getUserCity());
		user.setCityName(entity.getUserCityNm());
		user.setUserAvtive(entity.getUserAvtive());
		user.setPersonal_sig(entity.getUserSing());
		user.setUser_lock(entity.getUserLock());
		user.setUser_mobile(entity.getUserMobile());
		user.setUser_lock_time(entity.getUserLockTime());
		user.setUserType(entity.getUserTp());
		return user;
	}
}
