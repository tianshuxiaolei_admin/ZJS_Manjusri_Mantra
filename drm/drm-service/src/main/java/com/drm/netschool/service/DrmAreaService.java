package com.drm.netschool.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.drm.netschool.entity.DrmArea;
import com.drm.netschool.entity.DrmAreaExample;
import com.drm.netschool.mapper.DrmAreaMapper;

@Service
public class DrmAreaService {
	
	@Autowired
	private DrmAreaMapper areaMapper;
	
	public DrmArea getById(String id){
		return areaMapper.selectByPrimaryKey(id);
	}
	
	public List<DrmArea> getProvinceList(){
		DrmAreaExample example = new DrmAreaExample();
		example.createCriteria().andLevelEqualTo(1);
		return areaMapper.selectByExample(example);
	}
	
	public List<DrmArea> getCityList(String pId){
		DrmAreaExample example = new DrmAreaExample();
		example.createCriteria().andParentCodeEqualTo(pId).andLevelEqualTo(2);
		return areaMapper.selectByExample(example);
	}
	
	public List<DrmArea> getCityListByPcode(String pCode){
		DrmAreaExample example = new DrmAreaExample();
		example.createCriteria().andParentCodeEqualTo(pCode).andLevelEqualTo(2);
		return  areaMapper.selectByExample(example);
	}
}
