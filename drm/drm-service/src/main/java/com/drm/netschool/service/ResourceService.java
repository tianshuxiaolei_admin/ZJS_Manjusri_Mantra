package com.drm.netschool.service;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.drm.common.util.DateUtil;
import com.drm.common.util.IdCreater;
import com.drm.common.util.MD5Util;
import com.drm.netschool.entity.DrmArticles;
import com.drm.netschool.entity.SumerShareDownrecord;
import com.drm.netschool.entity.SumerSharedResource;
import com.drm.netschool.entity.extend.ResourceInfo;
import com.drm.netschool.mapper.SumerShareDownrecordMapper;
import com.drm.netschool.mapper.SumerSharedResourceMapper;
import com.drm.netschool.mapper.manual.SumerSharedResourceExtendMapper;
import com.drm.netschool.redis.RedisUtil;

@Service
public class ResourceService {

	private Logger LOGGER = LoggerFactory.getLogger(ResourceService.class);
	
	@Autowired
	private SumerSharedResourceMapper sumerSharedResourceMapper;
	
	@Autowired
	private  SumerShareDownrecordMapper  sumerShareDownrecordMapper;
	
	@Autowired
	private RedisUtil redisUtil;
	
	@Autowired
	private SumerSharedResourceExtendMapper extendMapper;
	
	public SumerSharedResource getById(long id){
		return sumerSharedResourceMapper.selectByPrimaryKey(id);
	}
	
	public int getCount(DrmArticles entity){
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("param", entity);
		return extendMapper.getCount(param);
	}
	
	public List<SumerSharedResource> getListByPage(SumerSharedResource entity, Integer page, Integer pageSize){
		int start = (page - 1) * pageSize;
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("param", entity);
		param.put("offset", start);
		param.put("limit", pageSize);
		return extendMapper.getListByPage(param);
	}
	
	public List<ResourceInfo> getResourceInfoList(ResourceInfo entity, Integer page, Integer pageSize){
		int start = (page - 1) * pageSize;
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("param", entity);
		param.put("offset", start);
		param.put("limit", pageSize);
		return extendMapper.getResourceInfoList(param);
	}
	
	public int getResourceInfoCount(ResourceInfo entity){
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("param", entity);
		return extendMapper.getResourceInfoCount(param);
	}
	
	public ResourceInfo getResourceInfo(Long id){
		ResourceInfo entity = new ResourceInfo();
		entity.setId(id);
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("param", entity);
		return extendMapper.getResourceInfo(param);
	}
	
	public List<ResourceInfo> getDownUserList(ResourceInfo entity, Integer page, Integer pageSize){
		int start = (page - 1) * pageSize;
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("param", entity);
		param.put("offset", start);
		param.put("limit", pageSize);
		return extendMapper.getDownUserList(param);
	}
	
	/**
	 * 
	 *  Function:
	 *  功能说明：整体下载记录
	 *	 使用说明：
	 *  @author  wangxiaolei  DateTime 2015年10月19日 下午11:23:41
	 *	返回类型: int    
	 *  @param info
	 *  @return
	 */
	public int addDownLoadCount( ResourceInfo info){
		ResourceInfo entity = new ResourceInfo();
		Map<String, Object> param = new HashMap<String, Object>();
		entity.setDownCnt(1);
		entity.setResourceId( info.getResourceId());
		param.put("param", entity);
		return extendMapper.addDownCount( param );
	}
	
	/**
	 * 
	 *  Function:
	 *  功能说明： 周下载记录
	 *	 使用说明：
	 *  @author  wangxiaolei  DateTime 2015年10月19日 下午11:23:22
	 *	返回类型: int    
	 *  @param info
	 *  @return
	 */
	public int setDownLoadWeekCount( ResourceInfo info){
		String key_perfix =  "resource:download:week:remark";
		Calendar calendar = Calendar.getInstance();
		int currentWeek = calendar.getWeekYear();
		ResourceInfo entity = new ResourceInfo();
		String currentCache = this.redisUtil.getString(key_perfix+info.getResourceId());
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("param", entity);
		entity.setDownCnt(1);
		entity.setResourceId(  info.getResourceId());
		
		if(StringUtils.isEmpty( currentCache ) || !currentCache.equals( String.valueOf( currentWeek ) ) ){
			//新的就设置
			String result = this.redisUtil.setString( key_perfix+info.getResourceId() , String.valueOf( currentWeek ) );
			LOGGER.info(key_perfix+info.getResourceId() + "  redis result := "+ result );
			return extendMapper.setDownWeekCount(param);
		}else{
			//存在并且相等就更新
			return extendMapper.addDownWeekCount(param);
		}
	}
	
	/**
	 * 
	 *  Function:
	 *  功能说明：保存下载记录
	 *	 使用说明：
	 *  @author  wangxiaolei  DateTime 2015年10月19日 下午11:52:42
	 *	返回类型: int    
	 *  @param info
	 *  @return
	 */
	public int insertDownLoadRecord( ResourceInfo info){
	 
		Map<String, Object> param = new HashMap<String, Object>();
		int len = info.getUserLnm().length();
		info.setUserLnm( "***"+info.getUserLnm().substring( len<2?0:1, len>5?3:len>1?3:1  )+"**" );
		param.put("param", info);
		return extendMapper.addShareDownrecord( param);
	}
	
	
	/**
	 * 
	 *  Function:
	 *  功能说明：生成下载的key，  验证是否下载有效，是否是我网站产生的
	 *	 使用说明：
	 *  @author  wangxiaolei  DateTime 2015年10月19日 下午11:58:43
	 *	返回类型: String    
	 *  @return
	 */
	public String  takedownloadKey(){
		String value = (IdCreater.getId()+DateUtil.getCurrentDateYMDHMS());
		String key = MD5Util.digest (value);
		String result = this.redisUtil.setString(  (key) , value, 30);
		LOGGER.info(key + " =key  ,    vaule =:   "+ value +" , result:="+result );
		return key;
	}
	
	
	
}
