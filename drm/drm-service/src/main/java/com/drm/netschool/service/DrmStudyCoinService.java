package com.drm.netschool.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.drm.netschool.entity.DrmStudyCoin;
import com.drm.netschool.entity.DrmStudyCoinExample;
import com.drm.netschool.mapper.DrmStudyCoinMapper;

@Service
public class DrmStudyCoinService {
	
	@Autowired
	private DrmStudyCoinMapper studyCoinMapper; 

	public DrmStudyCoin getById(Integer id){
		return studyCoinMapper.selectByPrimaryKey(id);
	}
	
	public DrmStudyCoin getDrmStudyCoin(){
		DrmStudyCoinExample example = new DrmStudyCoinExample();
		example.createCriteria().andUseYnEqualTo(1);
		List<DrmStudyCoin> list = studyCoinMapper.selectByExample(example);
		return list == null || list.isEmpty() ? null : list.get(0);
	}
}
