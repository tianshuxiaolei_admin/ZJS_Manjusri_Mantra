package com.drm.netschool.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.drm.common.util.DateUtil;
import com.drm.common.util.IdCreater;
import com.drm.netschool.entity.DrmStudyCourse;
import com.drm.netschool.mapper.DrmStudyCourseMapper;

@Service
public class DrmStudyCourseService {
	
	@Autowired
	private DrmStudyCourseMapper studyCourseMapper;
	
	public DrmStudyCourse getById(Long id){
		return studyCourseMapper.selectByPrimaryKey(id);
	}
	
	public int insert(Long userId, Long courseId){
		DrmStudyCourse entity = new DrmStudyCourse();
		entity.setUserId(userId);
		entity.setCourseId(courseId);
		entity.setCanStudy(1);
		entity.setCreateTime(DateUtil.current());
		Long id = IdCreater.getId();
		entity.setId(id);
		entity.setStudyTime(0);
		entity.setStudyCnt(0);
		return studyCourseMapper.insertSelective(entity);
	}
}
