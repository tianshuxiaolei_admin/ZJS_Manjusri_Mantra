package com.drm.netschool.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.drm.common.util.JsonMapper;
import com.drm.common.util.StringUtil;
import com.drm.netschool.entity.DrmSystemSetting;
import com.drm.netschool.entity.DrmSystemSettingExample;
import com.drm.netschool.mapper.DrmSystemSettingMapper;
import com.drm.netschool.redis.RedisUtil;

@Service
public class DrmSystemSettingService {

	private static final String CACHE_PREFIX = "drm_system_setting_20150830";

	@Autowired
	private DrmSystemSettingMapper systemSettingMapper;

	@Autowired
	private RedisUtil redisUtil;

	public DrmSystemSetting getById(Integer id) {
		return systemSettingMapper.selectByPrimaryKey(id);
	}

	public DrmSystemSetting getDrmSystemSetting() {
		DrmSystemSetting systemSetting = null;
		try {
			String json = redisUtil.getString(CACHE_PREFIX);
			if (StringUtil.empty(json)) {
				DrmSystemSettingExample example = new DrmSystemSettingExample();
				List<DrmSystemSetting> list = systemSettingMapper.selectByExample(example);
				if (list != null && list.size() > 0) {
					systemSetting = list.get(0);
					redisUtil.setString(CACHE_PREFIX, JsonMapper.toJson(systemSetting), 60 * 60);
				}
			} else {
				systemSetting = JsonMapper.toObject(json, DrmSystemSetting.class);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return systemSetting;
	}
}
