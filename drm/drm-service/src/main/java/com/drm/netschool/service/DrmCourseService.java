package com.drm.netschool.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.drm.netschool.entity.CourseCategoryBean;
import com.drm.netschool.entity.DrmCourse;
import com.drm.netschool.entity.DrmCourseOffline;
import com.drm.netschool.entity.DrmCourseOfflineExample;
import com.drm.netschool.entity.DrmCourseStatus;
import com.drm.netschool.entity.DrmCourseStatusExample;
import com.drm.netschool.entity.extend.BuyCourseRecord;
import com.drm.netschool.entity.extend.RecommendCourse;
import com.drm.netschool.mapper.CourseInfoMapper;
import com.drm.netschool.mapper.DrmCourseMapper;
import com.drm.netschool.mapper.DrmCourseOfflineMapper;
import com.drm.netschool.mapper.DrmCourseStatusMapper;
import com.drm.netschool.mapper.manual.DrmCourseExtendMapper;
import com.drm.netschool.mapper.manual.DrmUserExtendMapper;
import com.drm.netschool.mongo.MongoDao;

 /**
  *  集成所有的课程业务，当前service都是查询没有使用事物
  * @author Lenovo
  *
  */
@Service
public class DrmCourseService {
	
	private Logger LOGGER = LoggerFactory.getLogger(DrmCourseService.class);
	
	@Autowired
	private DrmCourseMapper drmCourseMapper;
	
	@Autowired
	private DrmCourseExtendMapper courseExtendMapper;
	
	@Autowired
	private CourseInfoMapper  courseInfoMapper;
	
	@Autowired
	@Qualifier("mongoDao")
	private MongoDao mongoDao;
	
	@Autowired  
	private  DrmCourseOfflineMapper	drmCourseOfflineMapper;
	
	@Autowired  
	private  DrmUserExtendMapper   drmUserExtendMapper;
	
	@Autowired
	private DrmCourseCountService drmCourseCountService;
	
	@Autowired
	private DrmCourseStatusMapper drmCourseStatusMapper;
	
	
	public DrmCourse getById(long id){
		return drmCourseMapper.selectByPrimaryKey(id);
	}
	/**
	 * TODO  说明
	 * @param entity
	 * @return
	 */
	public int getCount(DrmCourse entity){
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("param", entity);
		return courseExtendMapper.getCount(param);
	}
	
	/**
	 * TODO  说明
	 * @param entity
	 * @param page
	 * @param pageSize
	 * @return
	 */
	public List<DrmCourse> getListByPage(DrmCourse entity, Integer page, Integer pageSize){
		int start = (page - 1) * pageSize;
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("param", entity);
		param.put("offset", start);
		param.put("limit", pageSize);
		return courseExtendMapper.getListByPage(param);
	}
	
	/**
	 * TODO  说明
	 * @param ids
	 * @return
	 */
	public List<DrmCourse> getListByIds(List<Long> ids){
		if(ids == null || ids.size() <= 0)
			return null;
		
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("ids", ids);
		return courseExtendMapper.getListByIds(param);
	}
	
	public RecommendCourse getRecommendCourse(Long courseId) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("courseId", courseId);
		List<RecommendCourse> list = courseExtendMapper.getRecommendCourse(param);
		return list == null || list.isEmpty() ? null : list.get(0);
	}
	/**
	 * TODO 说明
	 * @param entity
	 * @param page
	 * @param pageSize
	 * @return
	 */
	public List<RecommendCourse> getRecommendCourses(RecommendCourse entity, Integer page, Integer pageSize){
		int start = (page - 1) * pageSize;
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("param", entity);
		param.put("offset", start);
		param.put("limit", pageSize);
		return courseExtendMapper.getRecommendCourses(param);
	}
	
	/**
	 * TODO	说明
	 * @param entity
	 * @return
	 */
	public int getRecommendCoursesCount(RecommendCourse entity){
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("param", entity);
		return courseExtendMapper.getRecommendCoursesCount(param);
	}
	
	public RecommendCourse getCourseDetail(Long courseId){
		LOGGER.info("getCourseDetail:::" + courseId);
		RecommendCourse course = getRecommendCourse(courseId);
		if(course == null)
			return null;
		
		course.setContent(mongoDao.getContentById(course.getCourseDesc()));
//		course.setCourseTime(cateTimeByCourseID(courseId));	//课程学时
		
		return course;
	}
	
	/*
	 * 主题显示列表的       每个课程信息   可以用作课程数量和现实
	 */
	public List<RecommendCourse>  subjectCourselist(Long courseId){
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("courseId", courseId);
		return courseInfoMapper.listCourseByTopicId(param) ;
	}
	
	/*
	 * 普通课程现实的额   课件信息          可以用作课件数量，和现实
	 * 
	 *  ...courseId  为了给专题算总过可是数量
	 */
	public List<CourseCategoryBean>  courseCatelist(Long ...courseId ){
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("courseIds", courseId);
		return courseInfoMapper.getCourseCategoryBeanList(param);
	}
	 
	/**
	 * 线下课程详情，需要的内容
	 */
	public  DrmCourseOffline  baseOffCourseById(Long courseId){
		DrmCourseOfflineExample exmple = new DrmCourseOfflineExample();
		exmple.createCriteria().andCourseIdEqualTo(courseId );
		List<DrmCourseOffline> course = this.drmCourseOfflineMapper.selectByExample(exmple) ;
		if(course!=null && course.size()>0){
			return course.get(0);
		}
		return null;
	}
	
	/*
	 * 根据 课程ID ，显示课程的   课件  总时长  ，  单位为分钟   
	 *   <= 0     表示未知时长    ,就显示位置市场，  
	 */
	private int cateTimeByCourseID(Long courseId ){
		return courseInfoMapper.getCateTimesByCourseId(courseId)  ;
	}
	
	/*
	 * 获取购买记录，   这个购买记录可以提供前台的分页查询功能，   初始化传递  offset =0  limit=10 
	 * 前台点积分也的时候再算
	 */
	public List<BuyCourseRecord> getBuyOrders(Long courseId, Integer page, Integer pageSize) {
		int start = (page - 1) * pageSize;
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("courseId", courseId);
		param.put("offset", start);
		param.put("limit", pageSize);
		return this.courseInfoMapper.getBuyRecordsByCourseId(param);
	}
	/*
	 * 获取总数
	 */
	public int getBuyOrdersCnt(DrmCourse course){
		Map param =new HashMap();
		param.put("courseId", course.getCourseId());
		return this.courseInfoMapper.getBuyRecordsCntByCourseId(param);
	}
	
	public DrmCourseStatus getCourseStatusByCourseId(Long courseId) {
		if (courseId == null)
			return null;

		DrmCourseStatusExample example = new DrmCourseStatusExample();
		example.createCriteria().andCourseIdEqualTo(courseId);
		List<DrmCourseStatus> list = drmCourseStatusMapper.selectByExample(example);
		return (list == null || list.isEmpty()) ? null : list.get(0);
	}
}
