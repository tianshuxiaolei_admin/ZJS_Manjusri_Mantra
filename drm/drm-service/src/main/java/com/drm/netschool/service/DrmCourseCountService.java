package com.drm.netschool.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.drm.netschool.entity.DrmCourseCount;
import com.drm.netschool.entity.DrmCourseCountExample;
import com.drm.netschool.mapper.DrmCourseCountMapper;


@Service
public class DrmCourseCountService {

	private Logger LOGGER = LoggerFactory.getLogger(DrmCourseCountService.class);
	
	@Autowired
	private DrmCourseCountMapper courseCountMapper;
	
	public DrmCourseCount getByCourseId(Long courseId){
		if(courseId == null || courseId == 0){
			return null;
		}
		DrmCourseCountExample example = new DrmCourseCountExample();
		example.createCriteria().andCourseIdEqualTo(courseId);
		List<DrmCourseCount> list = courseCountMapper.selectByExample(example);
		if(list == null || list.isEmpty()){
			return null;
		}
		return list.get(0);
	}
}
