package com.drm.netschool.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.drm.common.util.DateUtil;
import com.drm.netschool.entity.DrmUserUlog;
import com.drm.netschool.mapper.DrmUserUlogMapper;

@Service
public class DrmUserUlogService {
	
private Logger LOGGER = LoggerFactory.getLogger(DrmUserUlogService.class);
	
	@Autowired
	private DrmUserUlogMapper userUlogMapper;
	
	public DrmUserUlog getByUserId(Long userId){
		return userUlogMapper.selectByPrimaryKey(userId);
	}
	
	public int add(Long userId){
		DrmUserUlog entity = new DrmUserUlog();
		entity.setUserId(userId);
		entity.setArticleCnt(0);
		entity.setBlogCnt(0);
		entity.setBrowseCnt(0);
		entity.setBuyCnt(0);
		entity.setLoninCnt(0);
		entity.setQuizCnt(0);
		entity.setRechargeSum(0L);
		entity.setShareCnt(0);
		entity.setStudyNcnt(0);
		return userUlogMapper.insert(entity);
	}
	
	public int updateLoginTime(Long userId){
		DrmUserUlog entity = getByUserId(userId);
		if(entity == null){
			return 0;
		}
		entity.setLoginLog(DateUtil.current());
		return userUlogMapper.updateByPrimaryKey(entity);
	}
}
