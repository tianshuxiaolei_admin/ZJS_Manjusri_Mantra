package com.drm.netschool.service.model;

/**
 * 保存session的实体
 * 
 * @author 王晓磊
 * 
 */
public class SessionBean {

	/**
	 * 前缀
	 */
	public static final String USER_SESSION_KEY_PERFIX = "user:hash:info:";
	/**
	 * session 的 子字段
	 */
	public static final String USER_SESSION_SUBKEY_SESSION = "session";
	/**
	 * 用户等级的子字段
	 */
	public static final String USER_SESSION_SUBKEY_GREAD = "gread";
	/**
	 * 用户积分的子字段
	 */
	public static final String USER_SESSION_SUBKEY_SORCE = "sorce";

	// session 保存的时间 失效 ，单位 秒
	private int saveTempTimes = 24 * 60 * 60;

	// 登陆的时间
	private long loginTime;
	// 用户信息
	private User user;

	public long getLoginTime() {
		return loginTime;
	}

	public int getSaveTempTimes() {
		return saveTempTimes;
	}

	public void setSaveTempTimes(int saveTempTimes) {
		this.saveTempTimes = saveTempTimes;
	}

	public void setLoginTime(long loginTime) {
		this.loginTime = loginTime;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}
