package com.drm.netschool.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.drm.netschool.entity.SumerTag;
import com.drm.netschool.mapper.SumerTagMapper;
import com.drm.netschool.mapper.manual.SumerTagExtendMapper;

@Service
public class TagService {
	
	@Autowired
	private SumerTagExtendMapper extendMapper;
	
	@Autowired
	private SumerTagMapper tagMapper;
	
	public SumerTag getById(long id){
		return tagMapper.selectByPrimaryKey(id);
	}
	
	public int getCount(SumerTag entity){
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("param", entity);
		return extendMapper.getCount(param);
	}
	
	public List<SumerTag> getListByPage(SumerTag entity, Integer page, Integer pageSize){
		int start = (page - 1) * (pageSize == null ? 0 : pageSize);
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("param", entity);
		param.put("offset", start);
		param.put("limit", pageSize);
		return extendMapper.getListByPage(param);
	}
	
	public List<SumerTag> getHostTagList(SumerTag entity, Integer page, Integer pageSize){
		int start = (page - 1) * pageSize;
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("param", entity);
		param.put("offset", start);
		param.put("limit", pageSize);
		return extendMapper.getHostTagList(param);
	}
}
