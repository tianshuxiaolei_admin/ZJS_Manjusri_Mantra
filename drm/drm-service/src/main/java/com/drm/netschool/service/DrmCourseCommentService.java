package com.drm.netschool.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.drm.netschool.entity.DrmCourseComment;
import com.drm.netschool.entity.DrmCourseCommentExample;
import com.drm.netschool.mapper.DrmCourseCommentMapper;

@Service
public class DrmCourseCommentService {

	@Autowired
	private DrmCourseCommentMapper courseCommentMapper;

	public DrmCourseComment getById(Long id) {
		return courseCommentMapper.selectByPrimaryKey(id);
	}

	public DrmCourseComment getByCourseId(Long courseId) {
		DrmCourseCommentExample example = new DrmCourseCommentExample();
		example.createCriteria().andCourseIdEqualTo(courseId);
		List<DrmCourseComment> list = courseCommentMapper.selectByExample(example);
		return list == null || list.isEmpty() ? null : list.get(0);
	}
}
