package com.drm.netschool.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.drm.netschool.entity.DrmBuyCart;
import com.drm.netschool.entity.DrmCodes;
import com.drm.netschool.mapper.DrmCodesMapper;
import com.drm.netschool.mapper.manual.DrmCodesExtendMapper;

@Service
public class DrmCodesService {

	private Logger LOGGER = LoggerFactory.getLogger(DrmCodesService.class);
	
	@Autowired
	private DrmCodesMapper codesMapper;
	
	@Autowired
	private DrmCodesExtendMapper extendMapper;
	
	public DrmCodes getById(Integer id){
		return codesMapper.selectByPrimaryKey(id);
	}
	
	public int getCount(DrmBuyCart entity){
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("param", entity);
		return extendMapper.getCount(param);
	}
	
	public List<DrmCodes> getListByPage(DrmCodes entity, Integer page, Integer pageSize){
		int start = (page - 1) * (pageSize == null ? 0 : pageSize);
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("param", entity);
		param.put("offset", start);
		param.put("limit", pageSize);
		return extendMapper.getListByPage(param);
	}
}
