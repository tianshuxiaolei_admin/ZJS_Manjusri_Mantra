package com.drm.netschool.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.drm.netschool.entity.DrmCourseType;
import com.drm.netschool.mapper.DrmCourseTypeMapper;
import com.drm.netschool.mapper.manual.DrmCourseTypeExtendMapper;

@Service
public class DrmCourseTypeService {

	private Logger LOGGER = LoggerFactory.getLogger(DrmCourseTypeService.class);
	
	@Autowired
	private DrmCourseTypeMapper courseTypeMapper;
	
	@Autowired
	private DrmCourseTypeExtendMapper courseTypeExtendMapper;
	
	public DrmCourseType getById(Long id){
		return courseTypeMapper.selectByPrimaryKey(id);
	}
	
	public int getCount(DrmCourseType entity){
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("param", entity);
		return courseTypeExtendMapper.getCount(param);
	}
	
	public List<DrmCourseType> getListByPage(DrmCourseType entity, Integer page, Integer pageSize){
		int start = (page - 1) * (pageSize == null ? 0 : pageSize);
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("param", entity);
		param.put("offset", start);
		param.put("limit", pageSize);
		return courseTypeExtendMapper.getListByPage(param);
	}
}
