package com.drm.netschool.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.drm.netschool.entity.DrmSalesPromotion;
import com.drm.netschool.mapper.DrmSalesPromotionMapper;
import com.drm.netschool.mapper.manual.DrmSalesPromotionExtendMapper;

@Service
public class DrmSalesPromotionService {
	
	private Logger LOGGER = LoggerFactory.getLogger(DrmSalesPromotionService.class);
	
	@Autowired
	private DrmSalesPromotionMapper promotionMapper;
	
	@Autowired
	private DrmSalesPromotionExtendMapper extendMapper;
	
	public DrmSalesPromotion getById(int id){
		return promotionMapper.selectByPrimaryKey(id);
	}
	
	public DrmSalesPromotion getPromotion(int useYn){
		DrmSalesPromotion promotion = new DrmSalesPromotion();
		promotion.setUseYn(useYn);
		
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("param", promotion);
		param.put("status", 1);
		List<DrmSalesPromotion> list = extendMapper.getListByPage(param);
		if(list != null && list.size() > 0)
			return list.get(0);
		
		return null;
	}
	
	public int getCount(DrmSalesPromotion entity){
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("param", entity);
		return extendMapper.getCount(param);
	}
	
	public List<DrmSalesPromotion> getListByPage(DrmSalesPromotion entity, Integer page, Integer pageSize){
		int start = (page - 1) * pageSize;
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("param", entity);
		param.put("offset", start);
		param.put("limit", pageSize);
		return extendMapper.getListByPage(param);
	}
}
