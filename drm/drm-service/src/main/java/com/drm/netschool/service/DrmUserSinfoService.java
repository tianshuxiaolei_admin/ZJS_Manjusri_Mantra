package com.drm.netschool.service;

import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.drm.common.util.DateUtil;
import com.drm.netschool.entity.DrmUserSinfo;
import com.drm.netschool.entity.DrmUserSinfoLog;
import com.drm.netschool.mapper.DrmUserSinfoLogMapper;
import com.drm.netschool.mapper.DrmUserSinfoMapper;

@Service
public class DrmUserSinfoService {

	private Logger LOGGER = LoggerFactory.getLogger(DrmUserSinfoService.class);
	
	@Autowired
	private DrmUserSinfoMapper userInfoMapper;
	
	@Autowired
	private DrmUserSinfoLogMapper userInfoLogMapper;
	
	public DrmUserSinfo getByUserId(long userId){
		return userInfoMapper.selectByPrimaryKey(userId);
	}
	
	public void add(DrmUserSinfo entity){
		userInfoMapper.insert(entity);
		DrmUserSinfoLog record = new DrmUserSinfoLog();
		try {
			BeanUtils.copyProperties(record, entity);
			userInfoLogMapper.insertSelective(record);
			LOGGER.info(entity + ",添加DrmUserSinfolog!=:"+record);
		} catch ( Exception e) {
			e.printStackTrace();
		} 
	}
	
	public void add(long userId){
		DrmUserSinfo entity = new DrmUserSinfo();
		entity.setUserId(userId);
		entity.setLastupdateDt(DateUtil.current());
		entity.setUserGread("vip1");
		entity.setUserCredit(0);
		entity.setUserIntegral(0);
		entity.setUserLearncredit(0);
		add(entity);
		LOGGER.info(userId + ",添加DrmUserSinfo!");
	}
	
	public void update(DrmUserSinfo entity){
		entity.setLastupdateDt(DateUtil.current());
		userInfoMapper.updateByPrimaryKey(entity);
	}
}
