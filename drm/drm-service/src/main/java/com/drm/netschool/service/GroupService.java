package com.drm.netschool.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.drm.netschool.entity.extend.GroupInfo;
import com.drm.netschool.mapper.manual.GroupExtendMapper;

@Service
public class GroupService {
	
	private Logger LOGGER = LoggerFactory.getLogger(GroupService.class);
	
	@Autowired
	private GroupExtendMapper extendMapper;
	
	public int getGroupInfoCount(GroupInfo entity){
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("param", entity);
		return extendMapper.getGroupInfoCount(param);
	}
	public List<GroupInfo> getGroupInfoList(GroupInfo entity, Integer page, Integer pageSize){
		int start = (page - 1) * pageSize;
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("param", entity);
		param.put("offset", start);
		param.put("limit", pageSize);
		return extendMapper.getGroupInfoList(param);
	}
	
}
