package com.drm.netschool.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.drm.common.util.DateUtil;
import com.drm.netschool.entity.DrmCoursefaceApply;
import com.drm.netschool.entity.DrmCoursefaceApplyExample;
import com.drm.netschool.mapper.DrmCoursefaceApplyMapper;

@Service
public class DrmCoursefaceApplyService {
	
	@Autowired
	private DrmCoursefaceApplyMapper coursefaceApplyMapper;
	
	public void insert(DrmCoursefaceApply entity){
		entity.setApplyDt(DateUtil.current());
		coursefaceApplyMapper.insert(entity);
	}
	
	public DrmCoursefaceApply getByUserIdAndCourseId(Long userId, Long courseId){
		if(userId == null || userId <= 0 || courseId == null || courseId <= 0)
			return null;
		
		DrmCoursefaceApplyExample example = new DrmCoursefaceApplyExample();
		example.createCriteria().andUserIdEqualTo(userId).andCourseIdEqualTo(courseId);
		List<DrmCoursefaceApply> list = coursefaceApplyMapper.selectByExample(example);
		return (list == null || list.isEmpty()) ? null : list.get(0);
	}
}
