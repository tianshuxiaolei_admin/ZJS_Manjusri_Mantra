package com.drm.netschool.service.model;

import java.util.Date;

/**
 * 王晓磊
 */
public class User {
	private Long userId = 0L; // id
	private String loginName = ""; // 用户名
	private String userName = ""; // 真实姓名
	private String identifierId = ""; // 身份证号
	private String telNumber = ""; // 电话号码
	private String user_mobile = ""; // 手机号码
	private String emailAddr = ""; // 电子邮箱
	private int userType = 5; // 用户类型 类型1,系统管理员；2，管理员；3，教师；4，代理商；5，注册用户；

	private String pwd = ""; // 用户密码
	private String pwdsalt = ""; // 密码种子
	private String personal_sig = ""; // 个性签名
	private int gender = 2; // 性别 1，男；2，女
	private Date birthDate = null; // 出生年月日

	private String cityCode = ""; // 城市代码
	private String cityName = ""; // 城市名称
	private String provinceCode = ""; // 省份代码
	private String provinceName = ""; // 省份名称

	private Date createTime = null; // 创建时间
	private Date user_udt = null; // 更新时间

	private int userAvtive = 0; // 是否激活 0 未激活 ，1 激活

	private String user_desc = ""; // 用户描述
	private String user_cid = ""; // 创建人的id
	private String user_himg = ""; // 用户的头部图像
	private int user_uyn = 1; // 是否使用， 0表示不使用，1表示使用
	private int user_source = 0; // 用户来源，0，自己注册； 1，网校；2，代理商；3，分校
	private String user_qq = ""; // 用户的qq
	private String user_weibo = ""; // 用户的微博
	private long user_sourceid = 0; // 用户来源的细分
	private String user_edu = ""; // 学历，博士、硕士、本科、大专、高中、中专、其他
	private String user_school = ""; // 毕业院校
	private String user_major = ""; // 专业
	private int user_lock = 0; // 0 未锁定，1锁定， 锁定后暂时不能进入，需要等待一段时间
	private Date user_lock_time = null; // 被锁定的时间，可以根据这个时间，+
										// 设定的多久可以锁定失效来判断是否可以登录
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getIdentifierId() {
		return identifierId;
	}

	public void setIdentifierId(String identifierId) {
		this.identifierId = identifierId;
	}

	public String getTelNumber() {
		return telNumber;
	}

	public void setTelNumber(String telNumber) {
		this.telNumber = telNumber;
	}

	public String getUser_mobile() {
		return user_mobile;
	}

	public void setUser_mobile(String user_mobile) {
		this.user_mobile = user_mobile;
	}

	public String getEmailAddr() {
		return emailAddr;
	}

	public void setEmailAddr(String emailAddr) {
		this.emailAddr = emailAddr;
	}

	public int getUserType() {
		return userType;
	}

	public void setUserType(int userType) {
		this.userType = userType;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getPwdsalt() {
		return pwdsalt;
	}

	public void setPwdsalt(String pwdsalt) {
		this.pwdsalt = pwdsalt;
	}

	public String getPersonal_sig() {
		return personal_sig;
	}

	public void setPersonal_sig(String personal_sig) {
		this.personal_sig = personal_sig;
	}

	public int getGender() {
		return gender;
	}

	public void setGender(int gender) {
		this.gender = gender;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getProvinceCode() {
		return provinceCode;
	}

	public void setProvinceCode(String provinceCode) {
		this.provinceCode = provinceCode;
	}

	public String getProvinceName() {
		return provinceName;
	}

	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUser_udt() {
		return user_udt;
	}

	public void setUser_udt(Date user_udt) {
		this.user_udt = user_udt;
	}

	public int getUserAvtive() {
		return userAvtive;
	}

	public void setUserAvtive(int userAvtive) {
		this.userAvtive = userAvtive;
	}

	public String getUser_desc() {
		return user_desc;
	}

	public void setUser_desc(String user_desc) {
		this.user_desc = user_desc;
	}

	public String getUser_cid() {
		return user_cid;
	}

	public void setUser_cid(String user_cid) {
		this.user_cid = user_cid;
	}

	public String getUser_himg() {
		return user_himg;
	}

	public void setUser_himg(String user_himg) {
		this.user_himg = user_himg;
	}

	public int getUser_uyn() {
		return user_uyn;
	}

	public void setUser_uyn(int user_uyn) {
		this.user_uyn = user_uyn;
	}

	public int getUser_source() {
		return user_source;
	}

	public void setUser_source(int user_source) {
		this.user_source = user_source;
	}

	public String getUser_qq() {
		return user_qq;
	}

	public void setUser_qq(String user_qq) {
		this.user_qq = user_qq;
	}

	public String getUser_weibo() {
		return user_weibo;
	}

	public void setUser_weibo(String user_weibo) {
		this.user_weibo = user_weibo;
	}

	public long getUser_sourceid() {
		return user_sourceid;
	}

	public void setUser_sourceid(long user_sourceid) {
		this.user_sourceid = user_sourceid;
	}

	public String getUser_edu() {
		return user_edu;
	}

	public void setUser_edu(String user_edu) {
		this.user_edu = user_edu;
	}

	public String getUser_school() {
		return user_school;
	}

	public void setUser_school(String user_school) {
		this.user_school = user_school;
	}

	public String getUser_major() {
		return user_major;
	}

	public void setUser_major(String user_major) {
		this.user_major = user_major;
	}

	public int getUser_lock() {
		return user_lock;
	}

	public void setUser_lock(int user_lock) {
		this.user_lock = user_lock;
	}

	public Date getUser_lock_time() {
		return user_lock_time;
	}

	public void setUser_lock_time(Date user_lock_time) {
		this.user_lock_time = user_lock_time;
	}

	@Override
	public String toString() {
		return "User [userId=" + userId + ", loginName=" + loginName + ", userName=" + userName + ", identifierId=" + identifierId
				+ ", telNumber=" + telNumber + ", user_mobile=" + user_mobile + ", emailAddr=" + emailAddr + ", userType=" + userType
				+ ", pwd=" + pwd + ", pwdsalt=" + pwdsalt + ", personal_sig=" + personal_sig + ", gender=" + gender + ", birthDate="
				+ birthDate + ", cityCode=" + cityCode + ", cityName=" + cityName + ", provinceCode=" + provinceCode + ", provinceName="
				+ provinceName + ", createTime=" + createTime + ", user_udt=" + user_udt + ", userAvtive=" + userAvtive + ", user_desc="
				+ user_desc + ", user_cid=" + user_cid + ", user_himg=" + user_himg + ", user_uyn=" + user_uyn + ", user_source="
				+ user_source + ", user_qq=" + user_qq + ", user_weibo=" + user_weibo + ", user_sourceid=" + user_sourceid + ", user_edu="
				+ user_edu + ", user_school=" + user_school + ", user_major=" + user_major + ", user_lock=" + user_lock
				+ ", user_lock_time=" + user_lock_time + "]";
	}
}
