package com.drm.netschool.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.drm.common.util.DateUtil;
import com.drm.common.util.IdCreater;
import com.drm.common.util.StringUtil;
import com.drm.netschool.entity.DrmBuyCart;
import com.drm.netschool.entity.DrmBuyCartExample;
import com.drm.netschool.entity.DrmBuyCartKey;
import com.drm.netschool.entity.extend.CartInfo;
import com.drm.netschool.mapper.DrmBuyCartMapper;
import com.drm.netschool.mapper.manual.DrmBuyCartExtendMapper;

@Service
public class DrmBuyCartService {
	
	private Logger LOGGER = LoggerFactory.getLogger(DrmBuyCartService.class);
	
	@Autowired
	private DrmBuyCartExtendMapper extendMapper;
	
	@Autowired
	private DrmBuyCartMapper cartMapper;
	
	public DrmBuyCart getById(long cartId, long courseId){
		DrmBuyCartKey key = new DrmBuyCartKey();
		key.setCartId(cartId);
		key.setCourseId(courseId);
		return cartMapper.selectByPrimaryKey(key);
	}
	
	public int getCount(DrmBuyCart entity){
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("param", entity);
		return extendMapper.getCount(param);
	}
	
	public List<DrmBuyCart> getListByPage(DrmBuyCart entity, Integer page, Integer pageSize){
		int start = (page - 1) * pageSize;
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("param", entity);
		param.put("offset", start);
		param.put("limit", pageSize);
		return extendMapper.getListByPage(param);
	}
	
	public int delById(long cartId, long courseId){
		DrmBuyCart cart = getById(cartId, courseId);
		if(cart == null)
			return 0;
		
		cart.setCartSt(2);
		cart.setCancelDt(DateUtil.current());
		DrmBuyCartExample example = new DrmBuyCartExample();
		example.createCriteria().andCartIdEqualTo(cartId).andCourseIdEqualTo(courseId);
		return cartMapper.updateByExample(cart, example);
	}
	
	public Long add(long userId, long courseId, int type) {
		if(type == 0){
			DrmBuyCartExample example = new DrmBuyCartExample();
			example.createCriteria().andUserIdEqualTo(userId).andCourseIdEqualTo(courseId).andCartTpEqualTo(type).andCartStEqualTo(1);
			List<DrmBuyCart> l = cartMapper.selectByExample(example);
			if(l != null && l.size() > 0)
				return 0L;
		}
		LOGGER.info(userId + ",添加购物车:::" + courseId);
		DrmBuyCart cart = new DrmBuyCart();
		Long cartId = IdCreater.getId();
		cart.setCancelDt(null);
		cart.setCartId(cartId);
		cart.setCartSt(1);
		cart.setCourseId(courseId);
		cart.setPutTime(DateUtil.current());
		cart.setUserId(userId);
		cart.setCartTp(type);
		cartMapper.insertSelective(cart);
		return cartId;
	}
	
	/**
	 * 支付成功更新购物车状态
	 * @param userId
	 * @param courseIds
	 * @return
	 */
	public int updateCart(long userId, List<Long> courseIds){
		LOGGER.info(userId + "updateCart:::" + StringUtil.join(courseIds, ","));
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("state", 3);
		param.put("payTime", DateUtil.current());
		return extendMapper.updateCartState(param);
	}
	
	/**
	 * 查询购物车列表
	 * @param cartInfo
	 * @return
	 */
	public List<CartInfo> getCartList(CartInfo cartInfo){
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("param", cartInfo);
		return extendMapper.getCartList(param);
	}
	
	/**
	 * 查询购物车列表
	 * @param cartInfo
	 * @param cartIds
	 * @return
	 */
	public List<CartInfo> getCartList(CartInfo cartInfo, List<Long> cartIds){
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("param", cartInfo);
		param.put("cartIds", cartIds);
		return extendMapper.getCartList(param);
	}
}
