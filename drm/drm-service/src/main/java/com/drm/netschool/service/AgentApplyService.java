package com.drm.netschool.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.drm.common.util.DateUtil;
import com.drm.netschool.entity.DrmAgentApply;
import com.drm.netschool.mapper.DrmAgentApplyMapper;


@Service
public class AgentApplyService {

	@Autowired
	private DrmAgentApplyMapper agentApplyMapper;
	
	public DrmAgentApply getById(Integer id){
		return agentApplyMapper.selectByPrimaryKey(id);
	}
	
	public void add(DrmAgentApply entity){
		entity.setApplyDt(DateUtil.current());
		agentApplyMapper.insert(entity);
	}
}
